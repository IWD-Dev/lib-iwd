precision highp float;

// mats
uniform mat4 u_modelViewMatrix;
uniform mat3 u_normalMatrix;
uniform mat4 u_viewInverseMatrix;
uniform mat4 u_modelRotationMatrix;

// color
uniform vec4 u_ambient;
uniform vec4 u_diffuse;
uniform vec4 u_specular;
uniform float u_shininess;

// water rendering
uniform float u_wave_scale;
uniform float u_velocity_scale;
uniform float u_cycle_time;
uniform float u_phase_time;
uniform float u_lambert_beer;

uniform sampler2D u_noise_sampler;
uniform sampler2D u_wave_1_normal_sampler;
uniform sampler2D u_wave_2_normal_sampler;

// gltf (1.0) has no cubemaps, 6 faces instead
uniform sampler2D u_cube_front_sampler;
uniform sampler2D u_cube_back_sampler;
uniform sampler2D u_cube_left_sampler;
uniform sampler2D u_cube_right_sampler;
uniform sampler2D u_cube_up_sampler;
uniform sampler2D u_cube_down_sampler;

// alternative views
uniform bool u_hazard_as_color;
uniform float u_person_height;
uniform float u_person_weight;

varying vec2 v_position;
varying vec3 v_position_eye;
varying vec2 v_velocity;
varying float v_depth;

const float M_PI = 3.1415926535897932384626433832795;

// float rescale(float val, float old_min, float old_max, float new_min, float new_max){
//   return (((new_max - new_min) * (val - old_min)) / (old_max - old_min)) + new_min;
// }

float depth_to_alpha(float depth){
  return depth == 0.0 ? 0.0 : clamp(1.0 - exp(-depth * u_lambert_beer), 0.0, 0.95);
}

vec3 depth_to_color(float depth){
  return mix(u_diffuse.rgb, u_diffuse.rgb * 0.5, smoothstep(depth, 0.0, 1.0));
}

// https://www.gamedev.net/forums/topic/687535-implementing-a-cube-map-lookup-function/
vec2 sample_cube(vec3 v, inout float face_index){
  vec3 v_abs = abs(v);
	float ma;
	vec2 uv;

if(v_abs.z >= v_abs.x && v_abs.z >= v_abs.y)
	{
		face_index = v.z > 0.0 ? 5.0 : 4.0; // change
		ma = 0.5 / v_abs.z;
		uv = vec2(v.z < 0.0 ? -v.x : v.x, -v.y);
    
	}
	else if(v_abs.y >= v_abs.x)
	{
		face_index = v.y < 0.0 ? 3.0 : 2.0;
		ma = 0.5 / v_abs.y;
		uv = vec2(v.y < 0.0 ? v.z : -v.z, v.x); // change
	}
	else
	{
		face_index = v.x < 0.0 ? 1.0 : 0.0;
		ma = 0.5 / v_abs.x;
		uv = vec2(v.x < 0.0 ? v.z : -v.z, -v.y);
	}
	return uv * ma + 0.5;
}

float pn_flow(float height, float velocity){
  return height * velocity;
}

float pn_person(float height, float weight){
  // ToDo: Link Paper
  // could precalc this on cpu and just pass this single value instead of 2
  float exp_v = exp(0.001906 * weight * height + 1.09);
  return 0.0929 * (exp_v * exp_v);
}


void main(void) {
  // haazrd rendering
  if(u_hazard_as_color){

    float vmag = length(v_velocity);
    float depth = v_depth;

    float pnf = pn_flow(depth, vmag);
    float pnp = pn_person(u_person_height, u_person_weight);
    pnf += 0.2; // additional debris danger (paper says at least 0.5 though)

    float safety_edge = 0.8 * pnp;
    float hz = smoothstep(safety_edge, pnp, pnf);
    vec3 col = vec3(1.0, 1.0, 0.75);

    if(u_person_height - 0.25 < depth)
      col = vec3(0.16,0.51,0.73);
    if(pnp < pnf)
      col = mix(vec3(1.0,1.0,0.75), vec3(0.84,0.09,0.1), hz);

    gl_FragColor = vec4(col, 1.0);
  }

  // water rendering
  else 
  { 
    vec2 cv = v_velocity;
	  cv = -cv; // ToDo: might supply uniform to allow mirroring of x,y components seperately by user

    // normal calculation
    float noise = texture2D(u_noise_sampler, v_position * (u_wave_scale * 0.5)).r * 0.2;

    float half_cycle = 0.5 * u_cycle_time;
    float flowmap_offset_0 = mod(u_phase_time + noise, u_cycle_time);
    float flowmap_offset_1 = mod(u_phase_time + noise + half_cycle, u_cycle_time);

    vec3 wave1_normal = texture2D(u_wave_1_normal_sampler,
                   v_position * u_wave_scale + cv * u_wave_scale * u_velocity_scale * flowmap_offset_0).rgb;
    vec3 wave2_normal = texture2D(u_wave_2_normal_sampler,
                   v_position * u_wave_scale + cv * u_wave_scale * u_velocity_scale * flowmap_offset_1).rgb;

    float mix_factor = abs(half_cycle - flowmap_offset_0) / half_cycle;

    vec3 t_n = normalize(mix(wave1_normal, wave2_normal, mix_factor) * 2.0 - 1.0).xzy; // z-up normalmap
    //t_n = normalize(t_n * length(cv)); // flatten water surface by velocity
    vec3 r_n = normalize(czm_view3D * u_modelRotationMatrix * vec4(t_n, 0.0)).xyz;
    // end normal calculation

    vec3 view_dir = -normalize(v_position_eye);
    vec3 l = normalize(czm_sunDirectionEC); // may not need to normalize
    vec3 h = normalize(l + view_dir);
    vec3 r = normalize(reflect(view_dir, r_n));

    // transform eye-space reflection back into world-space
    r = (u_viewInverseMatrix * vec4(r, 0.0)).xyz;
    r.y = -r.y;
    r = normalize((u_modelRotationMatrix * vec4(r, 0.0))).xyz; // may not need to normalize
    
    // choose cubemap reflection face and sample color
    float refl_face = -1.0;
    vec3 refl_color = vec3(0.0);
    vec2 refl_uv = sample_cube(r, refl_face);
    
    if(refl_face == 0.0){ // x+
      refl_color = texture2D(u_cube_right_sampler, refl_uv).rgb;
      //refl_color = vec3(1.0,0.0,0.0); // red
    }
    else if(refl_face == 1.0){ // x-
      refl_color = texture2D(u_cube_left_sampler, refl_uv).rgb;
      //refl_color = vec3(0.0,1.0,0.0); // green
    }
    else if(refl_face == 2.0){ // y+
      refl_color = texture2D(u_cube_up_sampler, refl_uv).rgb;
      //refl_color = vec3(0.0,0.0,1.0); // blue
    }
    else if(refl_face == 3.0){ // y-
      refl_color = texture2D(u_cube_down_sampler, refl_uv).rgb;
      //refl_color = vec3(1.0,1.0,0.0); // yellow
    }
    else if(refl_face == 4.0){ // z+
      refl_color = texture2D(u_cube_front_sampler, refl_uv).rgb;
      //refl_color = vec3(1.0,0.0,1.0); // magenta
    }
    else if(refl_face == 5.0){ // z-
      refl_color = texture2D(u_cube_back_sampler, refl_uv).rgb;
      //refl_color = vec3(0.0,1.0,1.0); // cyan
    }

    float gray = (refl_color.x + refl_color.y + refl_color.z) / 3.0;
    refl_color = vec3(gray, gray, gray);

    float fresnel_factor = dot(view_dir, t_n); // simplification

    //blinn-phong-like shading
    vec3 color_shaded = u_ambient.rgb;

    float diffuse_intensity = clamp(dot(r_n, l), 0.1, 1.0); // retain minimum brightness even at night
    vec3 diffuse =  diffuse_intensity * (depth_to_color(v_depth) + refl_color * (clamp(1.0 - fresnel_factor, 0.2, 0.8))); // retain some reflection even if looking straight down

    color_shaded = color_shaded + diffuse - (0.2 * (1.0 - fresnel_factor));

    float specular_intensity = clamp(pow(max(dot(t_n, h), 0.0), u_shininess), 0.0, 1.0);
    color_shaded += u_specular.rgb * specular_intensity;

    float alpha = depth_to_alpha(v_depth);
    gl_FragColor = vec4(color_shaded, alpha);
    //gl_FragColor = vec4(color_shaded, 1.0);
    //gl_FragColor = vec4(1.0,0.0,0.0,1.0);
  }
}