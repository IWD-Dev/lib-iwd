precision highp float;

uniform mat4 u_modelViewMatrix;
uniform mat4 u_projectionMatrix;
uniform vec2 u_velocity_max;

attribute vec3 a_position;
attribute vec2 a_position_original;
attribute vec2 a_velocity;
attribute float a_depth;

varying vec2 v_position;
varying vec3 v_position_eye;
varying vec2 v_velocity;
varying float v_depth;

// const mat4 z_up_to_y_up = mat4(vec4(1.0,0.0,0.0,0.0), vec4(0.0,0.0,-1.0,0.0), vec4(0.0,1.0,0.0,0.0), vec4(0.0,0.0,0.0,1.0));

void main(void) {
  vec4 pos = u_modelViewMatrix *  vec4(a_position, 1.0);
  //pos.y += 10.0;
  gl_Position = u_projectionMatrix * pos;
  
  v_position = a_position_original;
  v_position_eye = pos.xyz;
  v_velocity = a_velocity;
  v_depth = a_depth;
}
