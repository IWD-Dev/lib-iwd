set(TEST_TARGET_NAME iwd_test)

file(
	GLOB pch_files_test RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} CONFIGURE_DEPENDS
	${PROJECT_SOURCE_DIR}/src/pch.h
	${PROJECT_SOURCE_DIR}/build/src/CMakeFiles/iwd_core.dir/*
	${PROJECT_SOURCE_DIR}/build/test/CMakeFiles/iwd_test.dir/*
)

##############
# Build Test #
##############

add_executable(${TEST_TARGET_NAME} main.cpp)

source_group(TREE ${PROJECT_SOURCE_DIR} FILES main.cpp)
source_group(pch FILES ${pch_files_test})

target_include_directories(${TEST_TARGET_NAME}
	PUBLIC ${PROJECT_SOURCE_DIR}/include/lib-iwd
	PUBLIC ${PROJECT_SOURCE_DIR}/include/vendor
)
target_link_libraries(${TEST_TARGET_NAME} PRIVATE iwd_core compiler_options)

set_target_properties(${TEST_TARGET_NAME} PROPERTIES 
	DEBUG_POSTFIX ${CMAKE_DEBUG_POSTFIX}
	MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>DLL"
)