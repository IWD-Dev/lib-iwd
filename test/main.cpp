
#include <nlohmann/json.hpp>
#include <cxxopts/cxxopts.hpp>
#include <spdlog/spdlog.h>

#include "util/algorithm.h"
#include "data/grid_2d.h"
#include "data/grid_dataset.h"
#include "data/grid_dataset_processor.h"

#include "util/algorithm.h"

#if PROJ_ENABLED
#include "proj.h"
#endif

#include "base64/base64.h"

#include "quadtree/Quadtree.h"

#if GEOTIFF_ENABLED
#include "tiff.h"
#include "xtiffio.h"   /* for TIFF */
#include "geotiffio.h" /* for GeoTIFF */
enum { VERSION = 0, MAJOR, MINOR };
#endif

#if NETCDFCXX_ENABLED
#include <netcdf>
#endif

#include "io/time_parser.h"

using namespace IWD::data;

int main(int argc, char *argv[])
{
	// std::filesystem::path dir = "C:/DEV/pfad";
	// std::filesystem::path path = "toller_n�me";
	// std::cout << (dir / path).generic_u8string();

	// struct base {
	//	int a = 0;
	//	bool b = false;
	//	char c = '2';

	//	base &operator=(const base &base) = default;
	//	base &operator=(std::shared_ptr<base> base) { base::operator=(*base);};
	//};

	// struct derived : public base {
	//	double d = 4.0;
	//	float f = 1.25f;

	//	derived(const base &base) { base::operator=(base);}
	//	derived(std::shared_ptr<base> base) { base::operator=(base); }

	//	derived &operator=(std::shared_ptr<base> base) { base::operator=(base);
	//	}
	//};

	// auto b = std::make_shared<base>();
	// auto d = std::make_shared<derived>();
	// d = b;

	//	auto grid = std::make_shared<IWD::data::grid_dataset>();
	//	auto idx = grid->append_timestep(1.0);
	//	grid->emplace_component<grid_dataset::position_2D>(grid_dataset::position_2D(7, 7), 0);
	//	grid->emplace_component<grid_dataset::position_3D>(grid_dataset::position_3D(7, 7), 0);
	//	grid->test_component();
	//
	//	grid_dataset_processor proc;
	//	//proc.calculate_bbox(grid);
	//	proc.center_data(grid, grid->get_component<grid_dataset::position_3D>(0));
	//
	//
	//	auto &n = grid->get_component<grid_dataset::position_2D>(0);
	//	n(0, 0) = glm::dvec2(3.0, 4.0);
	//	auto n2 = grid->get_component<grid_dataset::position_2D>(0);
	//	auto x = n2(0, 0);
	//
	//#if PROJ_ENABLED
	//
	//	PJ_CONTEXT *context = proj_context_create();
	//	// https://proj.org/resource_files.html
	//	proj_context_set_database_path(context, "./share/proj/proj.db", NULL, NULL);
	//
	//	PJ *projection = proj_create_crs_to_crs(context, "EPSG:25833", "EPSG:4326", NULL);
	//
	//	if (projection == NULL || proj_errno(projection) != 0) {
	//		std::cerr << "couldn't setup projection: " << proj_errno(projection) << std::endl;
	//		return false;
	//	}
	//
	//	// 377121.7,5618965.9 --> 13.259589,50.709554
	//
	//	PJ_COORD pj_origin = proj_trans(projection, PJ_FWD, proj_coord(377121.7, 5618965.9, 100.0, HUGE_VAL));
	//	auto lam = pj_origin.lp.lam;
	//	auto phi = pj_origin.lp.phi;
	//
	//	std::cout << lam << " " << phi << std::endl;
	//
	//#endif
	//
	//	size_t a = 1;
	//	unsigned long long int ex{-1ull};

	//////////////////////////////////////////////////////////////

	// struct content {
	//	glm::dvec3 p;
	//	double d;
	//	double h;
	//	glm::dvec2 v;
	//};

	// auto getBox = [](content *c) {
	//	return quadtree::Box<double>({c->p.x, c->p.z}, 0.01); // 1cm distance from center
	//};

	// auto box = quadtree::Box<double>({0.0, 0.0}, {10.0, 10.0}); // reverse height

	// quadtree::Quadtree qt =
	//    quadtree::Quadtree<content *, decltype(getBox), std::equal_to<content *>, double>(4, 32, box, getBox);

	// std::vector<content *> nodes;
	// nodes.push_back(new content{{2.5, 0.0, 2.5}, 1.0, 4.0, {0.1, -0.1}});
	//// nodes.push_back(new content{{2.5, 0.0, 7.5}, 2.0, 3.0, {0.2, -0.2}});
	//// nodes.push_back(new content{{7.5, 0.0, 2.5}, 3.0, 2.0, {0.3, -0.3}});
	//// nodes.push_back(new content{{7.5, 0.0, 7.5}, 4.0, 4.0, {0.4, -0.4}});
	//// nodes.push_back(new content{{8.0, 0.0, 8.0}, 5.0, 5.0, {0.5, -0.5}});

	// for (auto i = 0; i < nodes.size(); ++i) {
	//	qt.add(nodes.at(i));
	//}

	// auto q1 = qt.query({{2.5, 2.5}, 1.0}); // search all nodes 1m from coord 2.5, 2.5
	// auto q2 = qt.query({{2.5, 2.5}, 0.01});
	// auto q3 = qt.query({{2.6, 2.6}, 0.051});
	// auto q4 = qt.query({{2.6 - 0.1, 2.6 - 0.1}, {0.1 * 2.0, 0.1 * 2.0}});
	// auto q5 = qt.query({{2.6, 2.6}, 0.4});

	// for (auto i = 0; i < nodes.size(); ++i) {
	//	delete nodes[i];
	//}

	//////////////////////////////////////////////////////////////

//#if GEOTIFF_ENABLED
//
//	TIFF *tif = (TIFF *)0;  /* TIFF-level descriptor */
//	GTIF *gtif = (GTIF *)0; /* GeoKey-level descriptor */
//	int versions[3];
//
//	geocode_t model; /* all key-codes are of this type */
//
//	// std::string fname = "C:\\DEV\\lib-iwd\\no_commit\\test\\rotterdam.tif";
//	std::string fname = "C:\\DEV\\lib-iwd\\no_commit\\test\\dresden_depth.tif";
//
//	/* Open TIFF descriptor to read GeoTIFF tags */
//	tif = XTIFFOpen(fname.c_str(), "r");
//	if (!tif) return -1;
//
//	/* Open GTIF Key parser; keys will be read at this time. */
//	gtif = GTIFNew(tif);
//	if (!gtif) return -1;
//
//	/* Get the GeoTIFF directory info */
//	GTIFDirectoryInfo(gtif, versions, 0);
//	if (versions[MAJOR] > 1) {
//		printf("this file is too new for me\n");
//		return -1;
//	}
//	if (!GTIFKeyGet(gtif, GTModelTypeGeoKey, &model, 0, 1)) {
//		printf("Yikes! no Model Type\n");
//		return -1;
//	}
//
//	int size;
//	tagtype_t type;
//
//	/* ASCII keys are variable-length; compute size */
//	int cit_length = GTIFKeyInfo(gtif, GTCitationGeoKey, &size, &type);
//	std::string citation;
//
//	if (cit_length > 0) {
//		citation.resize(size * cit_length);
//		GTIFKeyGet(gtif, GTCitationGeoKey, (void *)citation.c_str(), 0, cit_length);
//		printf("Citation:%s\n", citation.c_str());
//	}
//
//	/* ASCII keys are variable-length; compute size */
//	int epsg_length = GTIFKeyInfo(gtif, ProjectedCSTypeGeoKey, &size, &type);
//	short epsg;
//
//	if (epsg_length > 0) {
//		// epsg.resize(size * epsg_length);
//		GTIFKeyGet(gtif, ProjectedCSTypeGeoKey, (void *)&epsg, 0, epsg_length);
//		printf("EPSG:%d\n", epsg);
//	}
//
//	int width, height, tile_width, tile_height;
//	short format, bits;
//	/* Get some TIFF info on this image */
//	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
//	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
//	TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &format); // 3 = float
//	TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bits);  // 32 = SP float
//
//	bool has_tiles = static_cast<bool>(TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width));
//	TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height);
//
//	// https://stackoverflow.com/a/53691774
//	double *pxp;
//	uint16 count;
//
//	auto code = GTIFTagCode("ModelPixelScaleTag"); // 33550
//
//	if (TIFFGetField(tif, code, &count, &pxp)) {
//		int a = 0;
//		for (int i = 0; i < count; ++i) std::cout << pxp[i] << std::endl;
//	}
//
//	std::cout << std::endl;
//
//	auto tie = GTIFTagCode("ModelTiepointTag"); // 33922
//
//	if (TIFFGetField(tif, tie, &count, &pxp)) {
//		int a = 0;
//		for (int i = 0; i < count; ++i) std::cout << pxp[i] << std::endl;
//	}
//
//	
//	auto get_id = [&](uint32 x, uint32 y, uint32 width) -> uint32 { return y * width + x; };
//
//	if (!has_tiles) {
//		auto npixels = width * height;
//		//auto raster = (float_t *)_TIFFmalloc(npixels * sizeof(float_t));
//		auto scan_length = TIFFScanlineSize(tif);
//
//		std::vector<float> image(npixels, -std::numeric_limits<float>::max());
//
//		for (uint32 y = 0; y < height; ++y) {
//			auto off = get_id(0, y, width);
//			auto ret = TIFFReadScanline(tif, image.data() + off, y);
//
//			if (ret == -1) {
//				int a = 0;
//			}
//			else {
//				int a = 1;
//			}
//		}
//
//		for (auto f : image) {
//			if (f != -std::numeric_limits<float>::max()) {
//				auto stop = 0;
//			}
//		}
//
//		int dbg = 0;
//	}
//	else {
//
//		auto tile_size = TIFFTileSize(tif);        // bytes total
//		auto tile_row_size = TIFFTileRowSize(tif); // bytes in row
//		auto tile_num = TIFFNumberOfTiles(tif);
//		auto image_width = IWD::util::ceil_to_multiple(width, tile_width);
//		auto image_height = IWD::util::ceil_to_multiple(height, tile_height);
//
//
//		// http://www.verypdf.com/document/tiff6/pg_0066.htm
//
//		std::vector<float> image(
//		    (tile_size * tile_num) / sizeof(float), // width * height + padding
//		    -std::numeric_limits<float>::max());     // fill with default value -std::numeric_limits<float>::max()
//		std::vector<float> tile_buffer(tile_size / sizeof(float));
//
//		IWD::util::log::setup();
//
//		IWD::util::log::info("TEST", "Start TIFF reading");
//
//		for (uint32 y = 0; y < height; y += tile_height) {
//			for (uint32 x = 0; x < width; x += tile_width) {
//				auto tile_num = TIFFComputeTile(tif, x, y, 0, 0);
//
//				uint64_t bytecount = TIFFGetStrileByteCount(tif, tile_num);
//
//				if (bytecount != 0) { // if 0, already prefilled -> do nothing
//					auto ret = TIFFReadEncodedTile(tif, tile_num, tile_buffer.data(), tile_size);
//
//					// fill when error
//					if (!ret) {
//						std::fill(tile_buffer.begin(), tile_buffer.end(), -std::numeric_limits<float>::max());
//						std::cout << "WARNING: filling tile with empty value" << std::endl;
//					}
//
//					// copy buffer block to outer buffer per scanline
//					for (uint32 ty = 0; ty < tile_height; ++ty) {
//						auto img_off = get_id(x, ty, image_width);
//						auto buf_off = get_id(0, ty, tile_width);
//
//						std::memcpy(image.data() + img_off, tile_buffer.data() + buf_off, tile_width * sizeof(float));
//					}
//				}
//			}
//		}
//
//		IWD::util::log::info("TEST", "End TIFF reading");
//	}
//
//	/* get rid of the key parser */
//	GTIFFree(gtif);
//
//	/* close the TIFF file descriptor */
//	XTIFFClose(tif);
//
//#endif

	//netCDF::NcFile dataFile("C:/DEV/lib-iwd/no_commit/test/3di/results_3di.nc", netCDF::NcFile::FileMode::read);

	//auto num_nodes = dataFile.getDim("nMesh2D_nodes");
	//auto ns = num_nodes.getSize();

	//auto timesteps = dataFile.getDim("time").getSize();
	//auto corners = dataFile.getDim("nCorner_Nodes").getSize();

	///////////////////

	//auto data = dataFile.getVar("Mesh2DFace_xcc");
	//auto dc = data.getDimCount(); // 1
	//auto ddims = data.getDim(0); 
	//auto dds = ddims.getSize(); // should be same as num_nodes
	//std::vector<double> posx(dds);
	//data.getVar(posx.data());

	///////////////////

	//auto epsg_data = dataFile.getVar("projected_coordinate_system");
	//auto epsg_attr = epsg_data.getAtt("epsg");
	//auto l = epsg_attr.getAttLength();
	//std::string epsg_str(l, ' ');
	//epsg_attr.getValues(epsg_str.data());

	///////////////////

	//// 1 surface_water_2d -> look for this
	//// 2 groundwater_2d
	//// 5 open_water_boundary_2d
	//// 6 groundwater_boundary_2d

	//auto type_data = dataFile.getVar("Mesh2DNode_type");
	//auto type_name = type_data.getType().getName();
	//std::vector<int> types(ns, 0);
	//type_data.getVar(types.data());

	///////////////////

	//auto contour_x = dataFile.getVar("Mesh2DContour_x");
	//auto cxdims = contour_x.getDims(); // [0] = nodeid, [1] == 4
	//auto cxdimsv = {cxdims[0].getSize(), cxdims[1].getSize()};

	//std::vector<std::array<double, 4>> corners_x(cxdims[0].getSize(), {0.0,0.0,0.0,0.0});
	//contour_x.getVar(reinterpret_cast<double*>(corners_x.data()));


	//auto wse = dataFile.getVar("Mesh2D_s1");
	//std::vector<float> wsef(ns * timesteps, 0.0f);
	//for (size_t t = 0; t < timesteps; ++t) {
	//	std::vector<size_t> offset = {t, 0};
	//	std::vector<size_t> count = {1, ns};
	//	wse.getVar(offset, count, wsef.data() + t * ns);
	//}

	
	IWD::io::time_parser::parse("EMPTY", {});

	int stop = 0;

	return 0;
}
