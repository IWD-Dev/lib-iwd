#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

// latitude = phi; longitude = lambda
// depends on axis though, for us most likely ENU

namespace IWD::util::wgs84 {

/// <summary>
/// Computes transformation matrix on WGS84 ellipsoid.
/// </summary>
/// <param name="latitude">Latitude of position.</param>
/// <param name="longitude">Longitutde of position.</param>
/// <param name="height">Height above sea level of position.</param>
/// <returns>Transformation Matrix for given coordinate.</returns>
glm::dmat4 get_transform(double latitude, double longitude, double height);

/// <summary>
/// Computes WGS84 cartesian coordinate.
/// </summary>
/// <param name="latitude">Latitude of position.</param>
/// <param name="longitude">Longitutde of position.</param>
/// <param name="height">Height above sea level of position.</param>
/// <returns>Cartesian coordinate on WGS84 ellipsoid</returns>
glm::dvec3 get_position(double latitude, double longitude, double height);

//glm::dmat4 get_modelmatrix(glm::dvec3 position);

//glm::dmat4 heading_pitch_roll_to_fixed_frame(glm::dvec3 position, double heading, double pitch,
//                                             double roll);

//glm::dquat quaternion_from_heading_pitch_roll(double heading, double pitch, double roll);

//glm::dmat4 matrix4_from_translation_quaternion_scale(glm::dvec3 translation, glm::dquat rotation,
//                                                     glm::dvec3 scale);

//glm::dmat4 east_north_up_to_fixed_frame(glm::dvec3 position);
} // namespace IWD::util::wgs84
