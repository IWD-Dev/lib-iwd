#pragma once

#include <iostream>

#define FMT_HEADER_ONLY
#define FMT_USE_WINDOWS_H 0 // don't need windows system logging
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace IWD::util::log {

/// <summary>
/// Sets up logger. Must be called before any log::xxx() method is used!
/// </summary>
inline void setup()
{
	spdlog::set_level(spdlog::level::info);
	spdlog::set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%l] %v");

	auto log = spdlog::stdout_color_mt("console");
}

/// <summary>
/// Sets logging level.
/// </summary>
/// <param name="level">Logging level.</param>
inline void set_level(spdlog::level::level_enum level) { spdlog::set_level(level); }

/// <summary>
/// Prints formatted message with info level.
/// </summary>
/// <param name="tag">Tag to categorize.</param>
/// <param name="msg">Message to print.</param>
inline void info(const std::string &tag, const std::string &msg) { spdlog::get("console")->info(fmt::format("{} {}",tag, msg)); }

/// <summary>
/// Prints formatted message with warning level.
/// </summary>
/// <param name="tag">Tag to categorize.</param>
/// <param name="msg">Message to print.</param>
inline void warn(const std::string &tag, const std::string &msg)
{
	spdlog::get("console")->warn(fmt::format("{} {}", tag, msg));
}

/// <summary>
/// Prints formatted message with error level.
/// </summary>
/// <param name="tag">Tag to categorize.</param>
/// <param name="msg">Message to print.</param>
inline void error(const std::string &tag, const std::string &msg)
{
	spdlog::get("console")->error(fmt::format("{} {}", tag, msg));
}

/// <summary>
/// Prints message without any formatting .
/// </summary>
/// <param name="msg">Message to print.</param>
inline void plain(const std::string &msg) { std::cout << msg << std::endl; }

/// <summary>
/// Insert newline and flush output stream buffer.
/// </summary>
inline void endl() { std::cout << std::endl; }

} // namespace IWD::util::log