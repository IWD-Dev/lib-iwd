#pragma once

#include <array>
#include <vector>
#include <limits>
#include <optional>

#include "data/dataset_components.h"

#include "util/algorithm.h"
#include "delaunator-cpp/delaunator.hpp"

namespace IWD::util::triangle {

/// <summary>
/// Simple line type consisting of two positions.
/// </summary>
/// <typeparam name="T">Type of position.</typeparam>
/// <param name="start">Start position.</param>
/// <param name="end">End position.</param>
template <typename T> struct line_t {
	data::components::position_t<3, T> start;
	data::components::position_t<3, T> end;

	line_t() = default;
	line_t(data::components::position_t<3, T> start_, data::components::position_t<3, T> end_) : start(start_), end(end_) {}
};

/// <summary>
/// Simple triangle type consisting of tree positions and a triple index.
/// </summary>
/// <typeparam name="T">Type of position.</typeparam>
/// <returns></returns>
template <typename T> struct triangle_t {
	std::array<data::components::position_t<3, T>, 3> v;
	data::components::index_t<3> i;
};

/// <summary>
/// Intersection result storing whether a intersection occured and if so where.
/// </summary>
/// <typeparam name="T">Type of position.</typeparam>
/// <returns></returns>
template <typename T> struct intersection_result {
	data::components::position_t<3, T> pos{};
	bool on_line1{false};
	bool on_line2{false};
};

/// <summary>
/// Computes if two lines intersect.
/// http://jsfiddle.net/justin_c_rounds/Gd2S2/
/// </summary>
/// <typeparam name="T">Type of position within lines.</typeparam>
/// <param name="l1">Line 1.</param>
/// <param name="l2">Line 2.</param>
/// <returns>Intersection result.</returns>
template <typename T> intersection_result<T> check_line_intersection(line_t<T> &l1, line_t<T> &l2)
{
	intersection_result<T> result;

	T denominator =
	    ((l2.end.z - l2.start.z) * (l1.end.x - l1.start.x)) - ((l2.end.x - l2.start.x) * (l1.end.z - l1.start.z));

	if (denominator == 0) {
		return result;
	}

	T a = l1.start.z - l2.start.z;
	T b = l1.start.x - l2.start.x;
	T numerator1 = ((l2.end.x - l2.start.x) * a) - ((l2.end.z - l2.start.z) * b);
	T numerator2 = ((l1.end.x - l2.start.x) * a) - ((l1.end.z - l1.start.z) * b);
	a = numerator1 / denominator;
	b = numerator2 / denominator;

	result.pos.x = l1.start.x + (a * (l1.end.x - l1.start.x));
	result.pos.z = l1.start.z + (a * (l1.end.z - l1.start.z));

	// if line1 is a segment and line2 is infinite, they intersect if:
	if (a > 0 && a < 1) {
		result.on_line1 = true;
	}
	// if line2 is a segment and line1 is infinite, they intersect if:
	if (b > 0 && b < 1) {
		result.on_line2 = true;
	}

	return result;
}

/// <summary>
/// Calculates barycentric coordinates for a given triangle and position within.
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="triangle"></param>
/// <param name="position"></param>
/// <returns>Barycentric coordinates.</returns>
template <typename T>
glm::tvec3<T> calculate_barycentric_coordinates(triangle_t<T> &triangle, data::components::position_t<3, T> position)
{
	auto p0 = triangle.v[0];
	auto p1 = triangle.v[1];
	auto p2 = triangle.v[2];

	glm::dvec3 v0 = p1 - p0, v1 = p2 - p0, v2 = position - p0;
	double d00 = glm::dot(v0, v0);
	double d01 = glm::dot(v0, v1);
	double d11 = glm::dot(v1, v1);
	double d20 = glm::dot(v2, v0);
	double d21 = glm::dot(v2, v1);
	double denom = d00 * d11 - d01 * d01;
	auto v = (d11 * d20 - d01 * d21) / denom;
	auto w = (d00 * d21 - d01 * d20) / denom;
	auto u = 1.0 - v - w;

	return {u, v, w};
}
/// <summary>
/// Interpolates a value by applying barycentric weights.
/// </summary>
/// <typeparam name="T">Type of weights.</typeparam>
/// <typeparam name="B">Type of values.</typeparam>
/// <param name="weights">Weights corresponding to 'a', 'b' and 'c'.</param>
/// <param name="a">Value a.</param>
/// <param name="b">Value b.</param>
/// <param name="c">Value c.</param>
template <typename T, typename B>
B interpolate_barycentric(const glm::tvec3<T> &weights, const B &a, const B &b, const B &c)
{
	return weights.x * a + weights.y * b + weights.z * c;
}

/// <summary>
/// Internal function to split a triangle given by three positions 'a', 'b' and 'c' and distance to a plane into
/// multiple triangles. Calculates new triangle indices.
/// </summary>
/// <typeparam name="T">Type of position.</typeparam>
/// <param name="out">Vector of splitted triangles.</param>
/// <param name="a">Position a.</param>
/// <param name="b">Position b.</param>
/// <param name="c">Position c.</param>
/// <param name="i1">Index 1.</param>
/// <param name="i2">Index 2.</param>
/// <param name="i3">Index 3.</param>
/// <param name="d1">Distance 1.</param>
/// <param name="d2">Distance 2.</param>
/// <param name="d3">Distance 3.</param>
/// <param name="last_index">Sequential last triangle index.</param>
template <typename T>
void split_triangle_by_plane_distance(std::vector<triangle_t<T>> &out, data::components::position_t<3, T> &a,
                                      data::components::position_t<3, T> &b, data::components::position_t<3, T> &c,
                                      int32_t i1, int32_t i2, int32_t i3, T d1, T d2, T d3, int32_t &last_index)
{
	// Calculate the intersection point from a to b
	auto ab = a + (d1 / (d1 - d2)) * (b - a);

	if (d1 < T(0.0)) {
		// b to c crosses the clipping plane
		if (d3 < T(0.0)) {
			// Calculate intersection point from b to c
			auto bc = b + (d2 / (d2 - d3)) * (c - b);
			auto i_bc = ++last_index;
			auto i_ab = ++last_index;

			out.push_back({{b, bc, ab}, glm::ivec3(i2, i_bc, i_ab)});
			out.push_back({{bc, c, a}, glm::ivec3(i_bc, i3, i1)});
			out.push_back({{ab, bc, a}, glm::ivec3(i_ab, i_bc, i1)});
		}

		// c to a crosses the clipping plane
		else {
			// Calculate intersection point from a to c
			auto ac = a + (d1 / (d1 - d3)) * (c - a);
			auto i_ab = ++last_index;
			auto i_ac = ++last_index;

			out.push_back({{a, ab, ac}, glm::ivec3(i1, i_ab, i_ac)});
			out.push_back({{ab, b, c}, glm::ivec3(i_ab, i2, i3)});
			out.push_back({{ac, ab, c}, glm::ivec3(i_ac, i_ab, i3)});
		}
	}
	else {
		// c to a crosses the clipping plane
		if (d3 < T(0.0)) {
			// Calculate intersection point from a to c
			auto ac = a + (d1 / (d1 - d3)) * (c - a);
			auto i_ab = ++last_index;
			auto i_ac = ++last_index;

			out.push_back({{a, ab, ac}, glm::ivec3(i1, i_ab, i_ac)});
			out.push_back({{ac, ab, b}, glm::ivec3(i_ac, i_ab, i2)});
			out.push_back({{b, c, ac}, glm::ivec3(i2, i3, i_ac)});
		}

		// b to c crosses the clipping plane
		else {
			// Calculate intersection point from b to c
			auto bc = b + (d2 / (d2 - d3)) * (c - b);
			auto i_bc = ++last_index;
			auto i_ab = ++last_index;

			out.push_back({{b, bc, ab}, glm::ivec3(i2, i_bc, i_ab)});
			out.push_back({{a, ab, bc}, glm::ivec3(i1, i_ab, i_bc)});
			out.push_back({{c, a, bc}, glm::ivec3(i3, i1, i_bc)});
		}
	}
}

/// <summary>
/// Splits a triangle into multiple by position of a given tile size through using hesse normal form planes.
/// For now ignores double-x or double-z intersections, could be problematic with triangles larger than tile_sizes.
/// https://gamedevelopment.tutsplus.com/tutorials/how-to-dynamically-slice-a-convex-shape--gamedev-14479
/// </summary>
/// <typeparam name="T">Type of position.</typeparam>
/// <param name="triangle">Triangle to split.</param>
/// <param name="tile_size">Tile/plane size to clip against.</param>
/// <param name="last_index">Sequential last triangle index.</param>
/// <returns>Potentially splitted triangle. If size() == 1, no split was performed.</returns>
template <typename T>
std::vector<triangle_t<T>> split_triangle(triangle_t<T> &triangle, T tile_size, int32_t &last_index)
{
	if (tile_size == T(0) || util::sgn(tile_size) == T(-1)) return {triangle};

	auto split_func = [&](triangle_t<T> &tri, glm::tvec3<T> &n, T d) -> std::vector<triangle_t<T>> {
		// Compute distance of each triangle vertex to the clipping plane
		T d1 = glm::dot(tri.v[0], n) - d;
		T d2 = glm::dot(tri.v[1], n) - d;
		T d3 = glm::dot(tri.v[2], n) - d;

		std::vector<triangle_t<T>> tmp_result;

		// a to b crosses the clipping plane
		if (d1 * d2 < T(0))
			split_triangle_by_plane_distance(tmp_result, tri.v[0], tri.v[1], tri.v[2], tri.i.x, tri.i.y, tri.i.z, d1,
			                                 d2, d3, last_index);

		// a to c crosses the clipping plane
		else if (d1 * d3 < T(0))
			split_triangle_by_plane_distance(tmp_result, tri.v[2], tri.v[0], tri.v[1], tri.i.z, tri.i.x, tri.i.y, d3,
			                                 d1, d2, last_index);

		// b to c crosses the clipping plane
		else if (d2 * d3 < T(0))
			split_triangle_by_plane_distance(tmp_result, tri.v[1], tri.v[2], tri.v[0], tri.i.y, tri.i.z, tri.i.x, d2,
			                                 d3, d1, last_index);

		//// No clipping plane intersection; keep the whole triangle
		// else
		//	result.push_back(tri);

		return tmp_result;
	};

	std::vector<triangle_t<T>> result;
	data::components::bounding_box_t<3, T> bbox;
	bbox.calculate(std::vector<data::components::position_t<3, T>>{triangle.v[0], triangle.v[1], triangle.v[2]});

	auto tile_x = util::ceil_to_multiple(bbox.min.x, tile_size);
	auto tile_z = util::ceil_to_multiple(bbox.min.z, tile_size);

	// split on x-axis if bbox intersects plane
	if (bbox.min.x < tile_x && bbox.max.x > tile_x) {
		auto n = glm::tvec3<T>(1.0, 0.0, 0.0);
		T d = tile_x;

		result = split_func(triangle, n, d);
	}

	// split on z-axis if bbox intersects plane
	if (bbox.min.z < tile_z && bbox.max.z > tile_z) {
		auto n = glm::tvec3<T>(0.0, 0.0, 1.0);
		T d = tile_z;

		// x-plane wasn't split, split original triangle
		if (result.empty()) {
			result = split_func(triangle, n, d);
		}
		// x-plane was split, split all x-splitted triangles
		else {
			std::vector<triangle_t<T>> tmp_result;
			for (auto &tri : result) {
				auto r = split_func(tri, n, d);

				// no change
				if (r.empty()) {
					tmp_result.push_back(tri);
				}
				// split further
				else {
					tmp_result.push_back(tri); // necessary, fixes index - don't know why though
					tmp_result.insert(tmp_result.end(), r.begin(), r.end());
				}
			}
			result = tmp_result; // size either 5 or 9
			result.shrink_to_fit();
		}
	}

	if (result.empty()) result.push_back({triangle});

	return result;
}

} // namespace IWD::util::triangle