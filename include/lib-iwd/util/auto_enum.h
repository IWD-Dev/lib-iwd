#pragma once

#include <string>
#include "magic_enum/magic_enum.hpp"

namespace IWD::util {

namespace auto_enum {
	/*
	examplary usage

	auto dt = IWD::data::dataset_descriptor::type::sms_2dm;
	auto dt_inv = IWD::data::dataset_descriptor::type(99);

	auto n1 = IWD::util::auto_enum::name_from_type(dt);
	auto n_inv = IWD::util::auto_enum::name_from_type(dt_inv);

	auto t1 = IWD::util::auto_enum::type_from_name<IWD::data::dataset_descriptor::type>("sms_2dm");
	auto t_inv = IWD::util::auto_enum::type_from_name<IWD::data::dataset_descriptor::type>("inv_name");

	auto args = cmd_options_parse_and_validate(options, argc, argv);
	*/

	/// <summary>
	/// Gets name from type T value.
	/// </summary>
	/// <typeparam name="T">Type of t.</typeparam>
	/// <param name="t">Value to get name of.</param>
	/// <returns>Name if found, empty stringview otherwise.</returns>
	template <typename T> inline auto name_from_type(T t) -> std::string_view { return magic_enum::enum_name(t); }

	// //usage: auto type = ..::auto_enum::type_from_name<Type>(name);
	// returns enum value -1 by default if error was found
	// unsigned strongly typed enums will return std::numeric_limits<T>::max() instead

	/// <summary>
	/// Gets Type T value from name.
	/// </summary>
	/// <typeparam name="T">Type of value.</typeparam>
	/// <param name="s">Name to look up.</param>
	/// <param name="_default">Default value if no corresponding type was found.</param>
	/// <returns>If found, return value otherwise returns _default. Be aware of behaviour of unsigned numbers with
	/// _default = T(-1).</returns>
	template <typename T> inline auto type_from_name(const std::string &s, T _default = T(-1)) -> T
	{
		return magic_enum::enum_cast<T>(s).value_or(_default);
	}
} // namespace auto_enum
} // namespace IWD::util
