#pragma once

#include <string>

namespace IWD::util {

/// <summary>
/// Checks whether two numerical values are nearly equal.
/// </summary>
/// <typeparam name="T">Type of values.</typeparam>
/// <param name="a">Value 'a'.</param>
/// <param name="b">Value 'b'.</param>
/// <param name="threshold">Threshold to define equality.</param>
/// <returns>True if equal.</returns>
template <typename T> inline bool nearly_equal(T a, T b, T threshold = static_cast<T>(0.00001))
{
	return fabs(a - b) < threshold;
}

/// <summary>
/// Checks whether a numerical value is nearly greater than another.
/// </summary>
/// <typeparam name="T">Type of values.</typeparam>
/// <param name="a">Value 'a'.</param>
/// <param name="b">Value 'b'.</param>
/// <param name="threshold">Threshold to define distance.</param>
/// <returns>True if a is same or nearly greater equal.</returns>
template <typename T> inline bool nearly_greater_equal(T a, T b, T threshold = static_cast<T>(0.00001))
{
	return (a + threshold) >= b;
}

/// <summary>
/// Squares a value.
/// </summary>
/// <typeparam name="T">Type of 'value'.</typeparam>
/// <param name="val">Value to square.</param>
/// <returns>val * val</returns>
template <typename T> inline T sqr(T value) { return value * value; }

/// <summary>
/// Floors value to next multiple.
/// </summary>
/// <typeparam name="T">Type of 'value'.</typeparam>
/// <param name="value">Value to floor to multiple.</param>
/// <param name="multiple">Multiple to floor onto.</param>
/// <returns>Value floored to nearest multiple.</returns>
template <typename T> T floor_to_multiple(T value, T multiple)
{
	if (multiple == T(0)) return value;
	return static_cast<T>(std::floor(static_cast<double>(value) / static_cast<double>(multiple)) *
	                      static_cast<double>(multiple));
}

/// <summary>
/// Ceils value to nearest multiple.
/// </summary>
/// <typeparam name="T">Type of 'value'.</typeparam>
/// <param name="value">Value to ceil to multiple.</param>
/// <param name="multiple">Multiple to ceil onto.</param>
/// <returns>Value ceiled to nearest multiple.</returns>
template <typename T> T ceil_to_multiple(T value, T multiple)
{
	if (multiple == T(0)) return value;
	return static_cast<T>(std::ceil(static_cast<double>(value) / static_cast<double>(multiple)) *
	                      static_cast<double>(multiple));
}

/// <summary>
/// Rounds value to nearest multiple.
/// </summary>
/// <typeparam name="T">Type of 'value'.</typeparam>
/// <param name="value">Value to round to multiple.</param>
/// <param name="multiple">Multiple to round onto.</param>
/// <returns>Value rounded to nearest multiple.</returns>
template <typename T> T round_to_multiple(T value, T multiple)
{
	if (multiple == T(0)) return value;
	return static_cast<T>(std::round(static_cast<double>(value) / static_cast<double>(multiple)) *
	                      static_cast<double>(multiple));
}

/// <summary>
/// Calculates signum of value.
/// </summary>
/// <typeparam name="T">Type of 'value'.</typeparam>
/// <param name="val">Value to apply signum.</param>
/// <returns> [-1, 0 or 1] if value =[negative, 0, positive]</returns>
template <typename T> int sgn(T value) { return (T(0) < value) - (value < T(0)); }

// to_numerical spezialized for some types
template <typename T> T to_numerical(const std::string &str) { return static_cast<T>(-1); }
template <> int to_numerical<int>(const std::string &str);
template <> float to_numerical<float>(const std::string &str);
template <> double to_numerical<double>(const std::string &str);

} // namespace IWD::util