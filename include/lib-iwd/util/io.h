#pragma once

#include <string>
#include <filesystem>

namespace IWD::util {
/// <summary>
/// Reads the context of a whole file into a std::string.
/// </summary>
/// <param name="path">Path to file to read.</param>
/// <returns>Text content of file.</returns>
std::string read_file_as_text(const std::filesystem::path &path);

/// <summary>
/// Splits string into multiple tokens by seperator 'sep'.
/// </summary>
/// <param name="str">String to split.</param>
/// <param name="sep">Seperator for splitting.</param>
/// <param name="size_hint">Pre-allocates size_hint tokens.</param>
/// <returns>Splitted string.</returns>
std::vector<std::string> split_string(const std::string &str, char sep, size_t size_hint = 3);

/// <summary>
/// Gets substring right of delimiter.
/// </summary>
/// <param name="str">String to split.</param>
/// <param name="delim">Delimter used for splitting.</param>
/// <returns>Substring right of delimiter.</returns>
std::string get_right_of_delim(std::string const &str, std::string const &delim);

/// <summary>
/// Transforms string into lowercase.
/// </summary>
/// <param name="str">String to transform.</param>
/// <returns>Lower-case 'str'.</returns>
std::string to_lower(const std::string &str);

/// <summary>
/// Removes multiple following inner whitespaces in a string.
/// </summary>
/// <param name="str">String to remove inner whitespace from.</param>
/// <param name="whitespace">Whitespace character to remove multiples of.</param>
/// <returns></returns>
std::string remove_inner_whitespace(std::string &str, const char whitespace = ' ');

/// <summary>
/// Trims strings whitespace characters. ltrim = left, rtrim = right, trim = both
/// https://stackoverflow.com/questions/1798112/removing-leading-and-trailing-spaces-from-a-string
/// </summary>
/// <param name="s"></param>
/// <param name="t"></param>
/// <returns></returns>
std::string &ltrim(std::string &s, const char *t = " \t\n\r\f\v");
std::string &rtrim(std::string &s, const char *t = " \t\n\r\f\v");
std::string &trim(std::string &s, const char *t = " \t\n\r\f\v");

} // namespace IWD::util
