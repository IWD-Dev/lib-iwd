#pragma once

#include "data/dataset.h"
#include "data/dataset_processing_options.h"

#include "util/logger.h"

namespace IWD::io {

/// <summary>
/// Class for writing Wavefront OBJ.
/// </summary>
class obj_writer {
  public:

	/// <summary>
	/// Writes vertices and indices to .obj file.
	/// Does not compute normals.
	/// </summary>
	/// <typeparam name="T">Type of 'vertices'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <typeparam name="I">Type of 'indices'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="vertices">Vertex container to write.</param>
	/// <param name="indices">Index containter to write.</param>
	/// <param name="options">Options containing output path.</param>
	/// <returns>True if file was written sucessfully.</returns>
	template <typename T, typename I>
	static bool write(const T &vertices, const I &indices, std::shared_ptr<const data::job_processing_options> options)
	{
		// auto-close RAII
		std::ofstream file;

		auto of_name = options->output_directory / (options->output_basename.u8string() + ".obj");

		if (file.open(of_name); file.is_open()) {
			std::ios::sync_with_stdio(false); // might be placebo
			util::log::info("{obj_writer}", fmt::format("Writing output file: {0}", of_name.u8string()));

			file << fmt::format("# lib-iwd {0}\n", version::to_string());

			// IMPROVE: write in larger blocks
			for (const auto &v : vertices) {
				file << fmt::format("v {0} {1} {2}\n", v.x, v.y, v.z);
			}
			for (const auto &i : indices) {
				file << fmt::format("f {0} {1} {2}\n", i.x + 1, i.z + 1, i.y + 1);
			}

			std::ios::sync_with_stdio(true);
		}
		else {
			util::log::error("{obj_writer}", fmt::format("Could not open output file: {0}", of_name.u8string()));
			return false;
		}

		return true;
	}
};

} // namespace IWD::io