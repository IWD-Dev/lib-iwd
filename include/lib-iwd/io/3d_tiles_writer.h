#pragma once

#include <filesystem>
#include <memory>

#include "nlohmann/json.hpp"

#include "data/dataset.h"
#include "data/dataset_processing_options.h"

namespace IWD::io {

/// <summary>
/// Class for writing 3D Tiles: https://github.com/CesiumGS/3d-tiles
/// </summary>
class _3d_tiles_writer {
  public:
	/// <summary>
	/// https://github.com/CesiumGS/3d-tiles/tree/master/specification/TileFormats/Batched3DModel#layout
	/// </summary>
	struct b3dm_header {
		unsigned char magic[4] = {'b', '3', 'd', 'm'}; // always "b3dm"
		uint32_t version = 1u;
		uint32_t length;
		uint32_t feature_table_json_length;
		uint32_t feature_table_binary_length;
		uint32_t batch_table_json_length;
		uint32_t batch_table_binary_length;
	};


	/// <summary>
	/// Auxilary Information of a single tile. Used for building the tileset.json.
	/// </summary>
	struct tile_info {
		data::components::common::bounding_box_3D bbox;
		data::components::common::bounding_box_2D bbox_lat_long;
		std::string uri;
		uint32_t lod; // 0 = lowest, 1, 2, ...
		uint32_t epsg_current = 0;
		uint32_t epsg_original = 0;
		std::shared_ptr<tile_info> higher_lod_tile; // next tile in lod hierarchy, if highest: has_value = false
	};

	/// <summary>
	/// Writes 3D Tileset relying on information of 'info' and 'rtc' to 'path'.
	/// </summary>
	/// <param name="info">Information of all tiles in tileset.</param>
	/// <param name="rtc">Datasets rtc info. Typically set by dataset_processor::project_CRS_to_CRS().</param>
	/// <param name="path"></param>
	/// <returns>True if file was written successfully.</returns>
	static bool write(const std::vector<tile_info> info, const data::components::rtc_info &rtc,
	                  const std::filesystem::path &path);

	// header without binary gltf at the end
	// properly padded to 8 byte

	/// <summary>
	/// Builds binary header of b3dm file, properly padded to 8-byte boundary.
	/// glTF -> glb payload must be appended and bytes 8 to 11 (b3dm_header.length) must be updated afterwards.
	/// </summary>
	/// <returns>Binary buffer of b3dm header.</returns>
	static std::vector<std::byte> build_b3dm_header_binary();

  protected:
	/// <summary>
	/// Builds b3dm feature header.
	/// </summary>
	/// <returns>JSON containing {"BATCH_LENGTH" : 0}.</returns>
	static nlohmann::json build_b3dm_feature_header_json();

	/// <summary>
	/// Builds b3dm feature header.
	/// </summary>
	/// <returns>JSON containing {"id" : 0}.</returns>
	// static nlohmann::json build_b3dm_batch_header_json();

	/// <summary>
	/// Builds .json tileset for all tiles given by info.
	/// https://github.com/CesiumGS/3d-tiles/tree/master/specification
	/// </summary>
	/// <param name="info">Tile information of all tiles contained in the tileset.</param>
	/// <param name="rtc">Datasets rtc info. Typically set by dataset_processor::project_CRS_to_CRS().</param>
	/// <returns>JSON containing whole tileset.</returns>
	static nlohmann::json build_3d_tiles_json(const std::vector<tile_info> info, const data::components::rtc_info &rtc);
};

} // namespace IWD::io