#pragma once

#include "data/dataset.h"
#include "data/dataset_processing_options.h"

#include "util/logger.h"

namespace IWD::io {

/// <summary>
/// Class for writing STL.
/// </summary>
class stl_writer {
  public:
	/// <summary>
	/// Writes vertices and indices to .stl file.
	/// Does not compute normals.
	/// </summary>
	/// <typeparam name="T">Type of 'vertices'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <typeparam name="I">Type of 'indices'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="vertices">Vertex container to write.</param>
	/// <param name="indices">Index containter to write.</param>
	/// <param name="options">Options containing output path.</param>
	/// <returns>True if file was written sucessfully.</returns>
	template <typename T, typename I>
	static bool write(const T &vertices, const I &indices, std::shared_ptr<const data::job_processing_options> options)
	{
		// auto-close RAII
		std::ofstream file;

		auto of_name = options->output_directory / (options->output_basename.u8string() + ".stl");

		if (file.open(of_name); file.is_open()) {
			std::ios::sync_with_stdio(false); // might be placebo
			util::log::info("{stl_writer}", fmt::format("Writing output file: {0}", of_name.u8string()));

			file << "solid surface\n";

			for (const auto &i : indices) {
				file << fmt::format("\tfacet normal {0} {1} {2}\n", 0.0, 0.0, 0.0); // invalid normal

				{
					file << "\t\touter loop\n";

					// swap yz -> zy
					file << fmt::format("\t\t\tvertex {0} {1} {2}\n", vertices[i.x].x, vertices[i.x].z,
					                    vertices[i.x].y);
					file << fmt::format("\t\t\tvertex {0} {1} {2}\n", vertices[i.y].x, vertices[i.y].z,
					                    vertices[i.y].y);
					file << fmt::format("\t\t\tvertex {0} {1} {2}\n", vertices[i.z].x, vertices[i.z].z,
					                    vertices[i.z].y);

					file << "\t\tendloop\n";
				}

				file << "\tendfacet\n";
			}

			file << "endsolid surface\n";

			std::ios::sync_with_stdio(true);
		}
		else {
			util::log::error("{stl_writer}", fmt::format("Could not open output file: {0}", of_name.u8string()));
			return false;
		}

		return true;
	}
};

} // namespace IWD::io