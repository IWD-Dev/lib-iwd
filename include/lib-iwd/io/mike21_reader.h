#pragma once

#include "data/triangle_dataset.h"
#include "data/triangle_dataset_processor.h"

namespace IWD::io {


/// <summary>
/// Class for reading Mike21 datasets.
/// </summary>
class mike21_reader {
  public:

	/// <summary>
	/// Read Mike21 dataset by given options.
	/// Reads Mesh data, depth, velocity_u, velocity_v.
	/// Interpolates cell-centered (scattered) datapoints onto nodes.
	/// Converts quads to triangles.
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="options">Options to use for reading.</param>
	/// <returns>True if reading was successful.</returns>
	static bool read(std::shared_ptr<data::triangle_dataset> dataset,
	                 std::shared_ptr<const data::mike21_processing_options> options);

  private:
	/// <summary>
	/// Types of scalars to read.
	/// </summary>
	enum struct scalar_type : int32_t {depth = 0, velocity_u, velocity_v}; 

	/// <summary>
	/// Raw cell-centered data is read into a vector of scatter_points and later interpolated.
	/// </summary>
	struct scatter_point {
		data::components::position_t<2, double> position{};
		data::components::velocity_t<2, double> velocity{};
		data::components::depth_t<double> depth{};
	};

	/// <summary>
	/// Reads mesh file by given options.
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="options">Options to use for reading.</param>
	/// <returns>True if reading was successful.</returns>
	static bool read_mesh(std::shared_ptr<data::triangle_dataset> dataset,
	                      std::shared_ptr<const data::mike21_processing_options> options);

	// read only after mesh

	/// <summary>
	/// Reads a scalar-type data. Must only be called after read_mesh()!
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="path">Path of file to read.</param>
	/// <param name="type">Type of file to read.</param>
	/// <returns>True if reading was successful.</returns>
	static bool read_scalar(std::shared_ptr<data::triangle_dataset> dataset,
	                      std::filesystem::path path, scalar_type type);

	/// <summary>
	/// Interpolated cell-centered data onto nodes (vertices). Internally uses a quadtree for lookup.
	/// May remote std::vector<scatter_points> component from dataset afterwards to clear memory.
	/// </summary>
	/// <param name="dataset"></param>
	/// <returns>True if interpolation was successful.</returns>
	static bool interpolate_scattered_data(std::shared_ptr<data::triangle_dataset> dataset);

	/// <summary>
	/// Converts quad to triangle indices by splitting along the shortest edge.
	/// Removes and clears component data::components::common::quad_index>.
	/// </summary>
	/// <param name="dataset">Dataset to process.</param>
	static void quad_to_triangles(std::shared_ptr<data::triangle_dataset> dataset);
};

} // namespace IWD::io