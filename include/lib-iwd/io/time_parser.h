#pragma once

#include <vector>
#include <set>
#include <string>


namespace IWD::io {

/// <summary>
/// Class for reading time_graphs used for specifying export timesteps.
/// </summary>
class time_parser {
  public:
	using time_graph = std::vector<std::pair<size_t, double>>;

	/// <summary>
	/// Parses time graph given by string.
	/// </summary>
	/// <param name="str">String to parse.</param>
	/// <param name="graph">Graph to parse.</param>
	/// <returns>Evaluated list of timesteps.</returns>
	static std::vector<size_t> parse(const std::string& str, const time_graph& graph);

	/// <summary>
	/// Finds closest timestep
	/// </summary>
	/// <param name="graph">Graph used for search.</param>
	/// <param name="time">Time to seach for.</param>
	/// <returns>Closest timestep. Last element if > last elements time.</returns>
	static size_t find_closest_timestep(const time_graph &graph, double time);
};

} // namespace IWD::io