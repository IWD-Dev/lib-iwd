#pragma once

#include "data/triangle_dataset.h"
#include "data/triangle_dataset_processor.h"

namespace IWD::io {

/// <summary>
/// Class for reading Aquaveo SMS 2dm files.
/// </summary>
class sms_2dm_reader {
  public:
	/// <summary>
	/// Reads SMS 2dm file.
	/// Reads: mesh, depth, waterlevel, velocity.
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="options">Options containing paths to read.</param>
	/// <returns>True if all datasets were read successfully.</returns>
	static bool read(std::shared_ptr<data::triangle_dataset> dataset,
	                 std::shared_ptr<const data::sms_2dm_processing_options> options);

  private:
	/// <summary>
	/// Types of scalars to read.
	/// </summary>
	enum struct scalar_type : int32_t {depth = 0, height, velocity}; 

	/// <summary>
	/// Reads 2dm mesh.
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="options">Options containing paths to read.</param>
	/// <returns>True if dataset was read successfully.</returns>
	static bool read_mesh(std::shared_ptr<data::triangle_dataset> dataset,
	                      std::shared_ptr<const data::sms_2dm_processing_options> options);

	/// <summary>
	/// Reads 2dm scalar data. Read only after mesh was read!
	/// </summary>
	/// <param name="dataset">Dataset to read into.</param>
	/// <param name="path">Path to scalar file.</param>
	/// <param name="type">Type of scalar.</param>
	/// <returns>True if dataset was read successfully.</returns>
	static bool read_scalar(std::shared_ptr<data::triangle_dataset> dataset,
	                      std::filesystem::path path, scalar_type type);
};

} // namespace IWD::io