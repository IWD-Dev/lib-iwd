#pragma once

#include "nlohmann/json.hpp"

#include "data/dataset.h"
#include "data/dataset_processing_options.h"

namespace IWD::io {

/// <summary>
/// Class for writing glTF 2.0 files.
/// https://github.com/KhronosGroup/glTF/tree/master/specification/2.0
/// </summary>
class gltf_writer {
  public:
	/// <summary>
	/// Parameters required for building JSON.
	/// </summary>
	struct processing_params {
		data::components::common::bounding_box_3D bbox;
		data::components::common::bounding_box_2D bbox_original;
		data::components::depth_t<double> depth_min;
		data::components::depth_t<double> depth_max;
		data::components::velocity_t<2, double> velocity_min;
		data::components::velocity_t<2, double> velocity_max;
		bool is_3d_tile = false;
	};

	/// <summary>
	/// https://github.com/KhronosGroup/glTF/tree/master/extensions/1.0/Khronos/KHR_binary_glTF
	/// </summary>
	struct glb_header {
		unsigned char magic[4] = {'g', 'l', 'T', 'F'}; // always "glTF"
		uint32_t version = 2u;
		uint32_t length;
	};

	/// <summary>
	/// https://github.com/KhronosGroup/glTF/tree/master/extensions/1.0/Khronos/KHR_binary_glTF
	/// </summary>
	struct glb_chunk_header {
		enum struct chunk_type : uint32_t { json = 0x4E4F534A, bin = 0x004E4942 };
		uint32_t length;
		chunk_type type;
	};

	/// <summary>
	/// Writes glTF/glb/b3dm depending on given options and parameters.
	/// </summary>
	/// <param name="dataset">Dataset to get data from.</param>
	/// <param name="options">Options to use for constructing JSON.</param>
	/// <param name="params">Params to use for constructing JSON.</param>
	/// <param name="as_b3dm">Set to true if data shall be written as b3dm instead. Default: false.</param>
	/// <returns></returns>
	static bool write(std::shared_ptr<data::dataset> dataset,
	                  std::shared_ptr<const data::job_processing_options> options, const processing_params &params,
	                  bool as_b3dm = false);

  protected:
	/// <summary>
	/// Internally called by write(). Builds JSON by given options and parameters.
	/// </summary>
	/// <param name="dataset">Dataset to get data from.</param>
	/// <param name="options">Options to use for constructing JSON.</param>
	/// <param name="params">Params to use for constructing JSON.</param>
	/// <returns>JSON object containing glTF data.</returns>
	static nlohmann::json build_json(std::shared_ptr<data::dataset> dataset,
	                                 std::shared_ptr<const data::job_processing_options> options,
	                                 const processing_params &params);
};

} // namespace IWD::io