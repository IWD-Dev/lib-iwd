#pragma once

#include <filesystem>
#include <vector>
#include <memory>

#include "data/dataset_processor.h"
#include "definitions.h"

namespace IWD::io {

/// <summary>
/// Class for reading the CLI programs config JSON files.
/// </summary>
class config_reader {
  public:
	/// <summary>
	/// Return type of read method.
	/// </summary>
	struct result {
		std::shared_ptr<data::dataset_processing_options_global> global_settings;
		std::vector<std::shared_ptr<data::job_processing_options>> jobs;
		IWD::exit_code status = IWD::exit_code::normal;

		result() noexcept;
	};

	// also checks general validity of config file, uses validate() afterwards to get a proper validation

	/// <summary>
	/// Reads and parses provided JSON file given by 'path'.
	/// Performes general validation (e.g. type-checks). Internally calls validate() for further validation at the end.
	/// </summary>
	/// <param name="path">JSON file to read.</param>
	/// <returns>Result of reading. Check result.status before further handling.</returns>
	static config_reader::result read(const std::filesystem::path &path);

  private:
	// potentially modifies status of parameter result

	/// <summary>
	/// Validates all jobs of a result by checking job-specific inputs and option combinations.
	/// </summary>
	/// <param name="result">Data to validate. 'result'.status may be changed if validation fails.</param>
	static void validate(config_reader::result &result);
};

} // namespace IWD::io