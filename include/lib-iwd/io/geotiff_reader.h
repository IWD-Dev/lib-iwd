#pragma once

#if GEOTIFF_ENABLED

#include <array>
#include <limits>

#include "tiffio.h"
#include "glm/glm.hpp"

#include "data/triangle_dataset.h"
#include "data/triangle_dataset_processor.h"
#include "util/logger.h"

#include "util/algorithm.h"

namespace IWD::io {

/// <summary>
/// Class for reading GeoTIFFs. Currently supports regular (scanline) and tiled GeoTIFFS [uncompressed/compressed].
/// </summary>
class geotiff_reader {
  public:
	/// <summary>
	/// Types of scalars to read.
	/// </summary>
	enum struct scalar_type : int32_t { dem = 0, depth, velocity_u, velocity_v };

	/// <summary>
	/// Internal GeoTIFF header used for comparison and processing.
	/// </summary>
	struct geotiff_header {
		/// <summary>
		/// x- and y-origin is stored in [3] and [4] respectively.
		/// </summary>
		std::array<double, 6> tie_points;

		/// <summary>
		/// x- and y-pixelscale is stored in [0] and [1] respectively.
		/// </summary>
		std::array<double, 3> pixel_scale;

		short epsg;
		std::string citation;

		/// <summary>
		/// May include tile padding: width + (padding - width % padding).
		/// </summary>
		int width;

		/// <summary>
		/// May include tile padding: height + (padding height % padding).
		/// </summary>
		int height;

		/// <summary>
		/// Compares all members against another geotiff_header 'other'.
		/// </summary>
		/// <param name="other">Other geotiff_header to comapre against.</param>
		/// <returns>True if same.</returns>
		bool compare(const geotiff_header &other);
	};

	/// <summary>
	/// Reads dem, depth, velocity_u and velocity_v from paths given by 'options'.
	/// Validates headers and computes triangle mesh internally.
	/// </summary>
	/// <param name="dataset">Dataset to store data into.</param>
	/// <param name="options">Options used for reading.</param>
	/// <param name="epsg">EPSG from GeoTIFF (dem) file is written into this adress.</param>
	/// <returns>True if reading was successful.</returns>
	static bool read(std::shared_ptr<data::triangle_dataset> dataset,
	                 std::shared_ptr<const data::geotiff_processing_options> options, uint32_t &epsg);

	/// <summary>
	/// Implemenation of reading a single GeoTIFF. Chooses appropriate methods underneath.
	/// </summary>
	/// <param name="dataset">Dataset to store data into.</param>
	/// <param name="path">Path of GeoTIFF to read.</param>
	/// <param name="type">Scalar type to read into.</param>
	/// <returns>result.first = True if reading was successful. result.second = geotiff_header for comparison of
	/// equality.</returns>
	static std::pair<bool, geotiff_header> read_scalar(std::shared_ptr<data::triangle_dataset> dataset,
	                                                   std::filesystem::path path, scalar_type type);

	/// <summary>
	/// Computes positions and triangle indicies by using tie_points and pixel_scale.
	/// </summary>
	/// <param name="dataset">Dataset to store data into.</param>
	/// <param name="header">GeoTIFF header to read tie_points and pixel_scale from.</param>
	/// <returns></returns>
	static bool compute_positions(std::shared_ptr<data::triangle_dataset> dataset, const geotiff_header &header);

  private:
	/// <summary>
	/// Reads scanline TIFF.
	/// </summary>
	/// <typeparam name="T">Type of container. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="tif">TIFF file to get information from.</param>
	/// <param name="container">Container to read into.</param>
	/// <returns>True if reading was successful.</returns>
	template <typename T> static bool read_scanline_geotiff(TIFF *tif, T &container)
	{
		int width = 0, height = 0;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);

		typename T::value_type dflt =
		    glm::vec<T::value_type::value_dimensions, typename T::value_type::value_type>(-std::numeric_limits<float>::max());
		container.resize(static_cast<size_t>(width) * height, dflt);

		auto get_off = [](uint32_t x, uint32_t y, uint32_t _width) -> uint32_t { return y * _width + x; };

		for (uint32_t y = 0; y < static_cast<uint32_t>(height); ++y) {
			auto off = get_off(0, y, width);
			auto ret = TIFFReadScanline(tif, container.data() + off, y);

			if (ret == -1) {
				util::log::warn("geotiff_reader", fmt::format("Error while reading scanline {}", y));
			}
		}

		return true;
	}

	/// <summary>
	/// Reads scanline TIFF.
	/// </summary>
	/// <typeparam name="T">Type of container. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="tif">TIFF file to get information from.</param>
	/// <param name="container">Container to read into.</param>
	/// <returns>True if reading was successful.</returns>
	template <typename T> static bool read_tiled_geotiff(TIFF *tif, T &container)
	{
		int width = 0, height = 0, tile_width, tile_height;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);
		bool has_tiles = static_cast<bool>(TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width));
		has_tiles &= static_cast<bool>(TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height));

		if (!has_tiles) return false;

		auto tile_size = TIFFTileSize(tif); // bytes total
		// auto tile_row_size = TIFFTileRowSize(tif); // bytes in row
		// auto tile_num = TIFFNumberOfTiles(tif);
		auto total_width = IWD::util::ceil_to_multiple(width, tile_width);
		auto total_height = IWD::util::ceil_to_multiple(height, tile_height);

		typename T::value_type dflt =
		    glm::vec<T::value_type::value_dimensions, typename T::value_type::value_type>(-std::numeric_limits<float>::max());
		container.resize(static_cast<size_t>(total_width) * total_height, dflt);

		auto get_off = [](uint32_t x, uint32_t y, uint32_t _width) -> uint32_t { return y * _width + x; };

		std::vector<float> tile_buffer(tile_size / sizeof(float));

		for (int32_t y = 0; y < height; y += tile_height) {
			for (int32_t x = 0; x < width; x += tile_width) {
				auto tile_id = TIFFComputeTile(tif, x, y, 0, 0);

				uint64_t bytecount = TIFFGetStrileByteCount(tif, tile_id);

				if (bytecount != 0) { // if 0, already prefilled with dflt -> do nothing
					auto ret = TIFFReadEncodedTile(tif, tile_id, tile_buffer.data(), tile_size);

					// fill when error
					if (!ret) {
						std::fill(tile_buffer.begin(), tile_buffer.end(), -std::numeric_limits<float>::max());
						std::cout << "WARNING: filling tile with empty value" << std::endl;
					}

					// copy buffer block to outer buffer per scanline
					for (uint32_t ty = 0; ty < static_cast<uint32_t>(tile_height); ++ty) {
						auto img_off = get_off(x, ty, total_width);
						auto buf_off = get_off(0, ty, tile_width);

						std::memcpy(container.data() + img_off, tile_buffer.data() + buf_off,
						            tile_width * sizeof(float));
					}
				}
			}
		}

		return true;
	}
};

} // namespace IWD::io

#endif