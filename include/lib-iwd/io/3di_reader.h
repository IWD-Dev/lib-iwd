#pragma once

#if NETCDFCXX_ENABLED

#include "data/triangle_dataset.h"
#include "data/triangle_dataset_processor.h"

namespace IWD::io {

/// <summary>
/// Class for reading 3Di NetCDF files.
/// </summary>
class _3di_reader {
  public:
	/// <summary>
	/// Reads 3Di NetCDF file given by 'options' into 'dataset'.
	/// </summary>
	/// <param name="dataset">Dataset to store data into.</param>
	/// <param name="options">Options for processing.</param>
	/// <param name="epsg">EPSG from 3Di file is written into this adress.</param>
	/// <returns>True if reading was successful.</returns>
	static bool read(std::shared_ptr<data::triangle_dataset> dataset,
	                 std::shared_ptr<const data::_3di_processing_options> options, uint32_t &epsg);
};

} // namespace IWD::io

#endif