#pragma once

#include <vector>
#include <map>
#include <cassert>

#include <entt/entt.hpp>

#include "data/dataset_components_common.h"
#include "util/logger.h"

namespace IWD::data {

/// <summary>
/// Main flexible and extensible storage for all kinds of datasets.
/// Makes heavy use of the Entity-Component-Systme EnTT. All non-temporary data should be stored within an component.
/// </summary>
class dataset_component_storage {
  protected:
	/// <summary>
	/// The main registry.
	/// </summary>
	entt::registry registry;

	/// <summary>
	/// The data storage which holds all data for a single timestep. 1 entity per timestep.
	/// </summary>
	std::vector<entt::entity> entities;

	/// <summary>
	/// Actual timesteps of the dataset. A dataset has at least a single timestep (if no temporal data available, 0).
	/// Is tied to entities.size().
	/// </summary>
	std::vector<double> timesteps;

  public:
	dataset_component_storage() = default;

	/// <summary>
	/// Clears all allocated memory from all timesteps when destroyed.
	/// </summary>
	virtual ~dataset_component_storage() { clear(); }

	/// <summary>
	/// Gets registry. Might be removed later on.
	/// </summary>
	/// <returns>Registry</returns>
	// inline entt::registry &get_registry() { return registry; }

	/// <summary>
	/// Gets entities. Might be removed later on.
	/// </summary>
	/// <returns>All entities.</returns>
	// inline std::vector<entt::entity> &get_entities() { return entities; }

	/// <summary>
	/// Gets timesteps as constant vector reference.
	/// </summary>
	/// <returns>All timesteps.</returns>
	inline const std::vector<double> &get_timesteps() { return timesteps; }

	// appends entity and modifies timesteps
	// returns index of created timestep

	/// <summary>
	/// Appends a new entity and timestep at the end with 'time'.
	/// </summary>
	/// <param name="time">Time to insert new timestep at</param>
	/// <returns>Index of appended timestep if valid. If time is < timesteps.back() returns
	/// std::numeric_limits<size_t>::max(); as error.</returns>
	size_t append_timestep(double time);

	/// <summary>
	/// Overwrites timesteps value at given index.
	/// </summary>
	/// <param name="timestep">Index to override</param>
	/// <param name="time">Time to set.</param>
	/// <returns>True if ok. False if 'time' < timesteps.back().</returns>
	bool update_timestep(size_t timestep, double time);

	/// <summary>
	/// Emplaces component at given timestep. Multiple variants for const and move. Raises an error or silently fails if
	/// an overwrite would occur -> use emplace_or_replace_component() if unsure.
	/// </summary>
	/// <typeparam name="T">Type of component</typeparam>
	/// <param name="components">Value to set.</param>
	/// <param name="timestep">Timestep to emplace into.</param>
	/// <returns>Reference to emplaced value.</returns>
	template <typename T> decltype(auto) /* T& */ emplace_component(T &components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace<T>(entities[timestep], components);
		return registry.get<T>(entities[timestep]);
	}
	template <typename T> decltype(auto) /* T& */ emplace_component(const T &components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace<T>(entities[timestep], components);
		return registry.get<T>(entities[timestep]);
	}
	template <typename T> decltype(auto) /* T& */ emplace_component(T &&components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace<T>(entities[timestep], std::move(components));
		return registry.get<T>(entities[timestep]);
	}

	/// <summary>
	/// Emplaces or replaces component at given timestep. Multiple variants for const and move.
	/// </summary>
	/// <typeparam name="T">Type of component</typeparam>
	/// <param name="components">Value to set.</param>
	/// <param name="timestep">Timestep to emplace into.</param>
	/// <returns>Reference to emplaced value.</returns>
	template <typename T> decltype(auto) /* T& */ emplace_or_replace_component(T &components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace_or_replace<T>(entities[timestep], components);
		return registry.get<T>(entities[timestep]);
	}
	template <typename T> decltype(auto) /* T& */ emplace_or_replace_component(const T &components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace_or_replace<T>(entities[timestep], components);
		return registry.get<T>(entities[timestep]);
	}
	template <typename T> decltype(auto) /* T& */ emplace_or_replace_component(T &&components, size_t timestep)
	{
		assert(timestep < entities.size());
		registry.emplace_or_replace<T>(entities[timestep], std::move(components));
		return registry.get<T>(entities[timestep]);
	}

	/// <summary>
	/// Gets a component from given timestep. If unsure that component exists, use has_component() before or use
	/// try_get_component().
	/// </summary>
	/// <typeparam name="T">Type of component.</typeparam>
	/// <param name="timestep">Timestep to retrieve value from.</param>
	/// <returns>Reference to value.</returns>
	template <typename T> decltype(auto) /* T& */ get_component(size_t timestep)
	{
		assert(timestep < entities.size());
		return registry.get<T>(entities[timestep]);
	}

	/// <summary>
	/// Tries to get a component from given timestep. If component doesn't exist result will be nullptr.
	/// </summary>
	/// <typeparam name="T">Type of component.</typeparam>
	/// <param name="timestep">Timestep to retrieve value from.</param>
	/// <returns>Pointer to value. May be nullptr.</returns>
	template <typename T> decltype(auto) /* T* */ try_get_component(size_t timestep)
	{
		assert(timestep < entities.size());
		return registry.try_get<T>(entities[timestep]);
	}

	/// <summary>
	/// Checks whether a component exists at 'timestep'.
	/// </summary>
	/// <typeparam name="T">Type to check for.</typeparam>
	/// <param name="timestep">Timestep to check for.</param>
	/// <returns>True if found.</returns>
	template <typename T> bool has_component(size_t timestep)
	{
		assert(timestep < entities.size());
		return registry.all_of<T>(entities[timestep]);
	}

	// clear single component of a timestep

	/// <summary>
	/// Clear and destroy a specific component for a given timestep if it exists.
	/// </summary>
	/// <typeparam name="T">Type of component.</typeparam>
	/// <param name="timestep">Timestep to remove component for.</param>
	template <typename T> void clear_component(size_t timestep)
	{
		assert(timestep < entities.size());
		registry.remove_if_exists<T>(entities[timestep]);
	}

	/// <summary>
	/// Clear and destroy all components for a given timestep.
	/// </summary>
	/// <param name="timestep">Timestep to remove all components for.</param>
	void clear_all_components(size_t timestep);

	/// <summary>
	/// Clears all data of all timesteps.
	/// </summary>
	void clear();

	// IMPROVE: wrap more if needed

	/// <summary>
	/// Copies a component from another storage for given timestep.
	/// </summary>
	/// <typeparam name="T">Type to copy.</typeparam>
	/// <param name="other">Storage to copy from.</param>
	/// <param name="timestep">Timestep to copy from.</param>
	/// <returns>True if ok. False if 'other' has no T or this has not enough timesteps.</returns>
	template <typename T> bool copy_component(std::shared_ptr<dataset_component_storage> other, size_t timestep)
	{
		if (!other->has_component<T>(timestep)) {
			util::log::error(std::string("{dataset_component_storage}"),
			                 "Can't copy component as other dataset_component_storage has no corresponding component.");
			return false;
		}

		// IMPROVE: for every timestep element check if same time
		if (this->timesteps.size() != other->get_timesteps().size()) {
			util::log::error(std::string("{dataset_component_storage}"),
			                 "Can't copy component as timesteps do not match.");
			return false;
		}

		this->emplace_or_replace_component(other->get_component<T>(timestep), timestep);

		return true;
	}

	/// <summary>
	/// Copies a component from another storage for given timestep by using an index_map.
	/// </summary>
	/// <typeparam name="T">Type to copy.</typeparam>
	/// <param name="other">Storage to copy from.</param>
	/// <param name="idx_map">Index map to use for copying.</param>
	/// <param name="timestep">Timestep to copy from.</param>
	/// <returns>True if ok. False if 'other' has no T or this has not enough timesteps.</returns>
	template <typename T>
	bool copy_component_index_mapped(std::shared_ptr<dataset_component_storage> other,
	                                 const components::common::index_map &idx_map, size_t timestep)
	{
		if (!other->has_component<T>(timestep)) {
			util::log::error(std::string("{dataset_component_storage}"),
			                 "Can't copy component as other dataset_component_storage has no corresponding component.");
			return false;
		}

		/*if (!(other->has_component<components::common::triangle_index>(0) &&
		      this->has_component<components::common::triangle_index>(0))) {
		    util::log::error(std::string("{dataset_component_storage}"),
		                     "Can't copy component as a dataset_component_storage have no corresponding components.");
		    return false;
		}*/

		// IMPROVE: for every timestep element check if same
		if (this->timesteps.size() != other->get_timesteps().size()) {
			util::log::error(std::string("{dataset_component_storage}"),
			                 "Can't copy component as timesteps do not match.");
			return false;
		}

		const auto &src = other->get_component<T>(timestep);
		auto &dst = this->emplace_or_replace_component<T>({}, timestep);
		dst.resize(idx_map.size());

		for (const auto &idx : idx_map) {
			dst[idx.first] = src[idx.second];
		}
		dst.shrink_to_fit();

		return true;
	}

	// bool copy_all_components(std::shared_ptr<dataset_component_storage> other) {
	//	const auto &other_reg = other->registry;
	//	for (const auto &entity : other->entities) {
	//		other_reg.visit(entity, [&](const auto info) {
	//			auto storage = other->get_registry().storage(info);
	//			//storage->emplace(registry, other, storage->get(entity));
	//			int a = 0;
	//		});
	//	}
	//	return true;
	//}
};

} // namespace IWD::data