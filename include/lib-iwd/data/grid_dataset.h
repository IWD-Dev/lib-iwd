#pragma once

#include "data/dataset.h"

#include "data/grid_2d.h"

namespace IWD::data {

/// <summary>
/// Dataset specialization for grid datasets. Currently unused!
/// </summary>
class grid_dataset : public dataset {
  public:
	template <typename T> using storage = grid_2d<T>;

	// pre-defined regularly used types
	using position_2D = storage<components::position_t<2, double>>;
	using position_3D = storage<components::position_t<3, double>>;
	using velocity_2D = storage<components::velocity_t<2, double>>;
	using velocity_3D = storage<components::velocity_t<3, double>>;
	using depth = storage<components::depth_t<double>>;
	using height = storage<components::height_t<double>>;
	using valid = storage<components::valid_t>;
	using kind = storage<components::kind_t>;

	grid_dataset() = default;
	virtual ~grid_dataset() = default;

	dataset::type get_type() noexcept override;

	size_t vertex_count() override;
	size_t index_count() override;
};

} // namespace IWD::data