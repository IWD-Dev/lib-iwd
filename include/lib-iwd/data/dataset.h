#pragma once

#include <memory>

#include "data/dataset_components.h"
#include "data/dataset_component_storage.h"

namespace IWD::data {

/// <summary>
/// Main data storage component. Interface to access inherited dataset_component_storage members. Most, if not all data
/// access is handled through the dataset. See dataset_component_storage for the handling of data components.
/// </summary>
class dataset : public dataset_component_storage {
  public:
	/// <summary>
	/// Types of datasets. New types must be added.
	/// </summary>
	enum struct type : int32_t { error = -1, triangle = 0, grid };

	/// <summary>
	/// Default destructor.
	/// </summary>
	virtual ~dataset() = default;

	/// <summary>
	/// Gets type of inheriting dataset (e.g. triangle_dataset, grid_dataset).
	/// </summary>
	/// <returns>Dataset type.</returns>
	virtual type get_type() = 0;

	/// <summary>
	/// Gets vertex count. For performance-wise cheap retrieval at runtime.
	/// </summary>
	/// <returns></returns>
	virtual size_t vertex_count() = 0;

	/// <summary>
	/// Gets index count. For performance-wise cheap retrieval at runtime.
	/// </summary>
	/// <returns></returns>
	virtual size_t index_count() = 0;
};

} // namespace IWD::data