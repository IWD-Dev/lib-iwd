#pragma once

#include <memory>
#include <limits>
#include <type_traits>
#include <optional>
#include <map>

#include "proj.h"

#include "definitions.h"

#include "data/dataset.h"
#include "data/dataset_components_common.h"
#include "data/dataset_processing_options.h"
#include "data/grid_dataset.h"
#include "data/triangle_dataset.h"

#include "util/logger.h"
#include "util/wgs84.h"

namespace IWD::data {

/// <summary>
/// General dataset processor which provides implementations for common operations transcending the more specialized
/// types.
/// </summary>
class dataset_processor {
  public:
  protected:
	dataset_processor(); // not strictly needed as there are pure virtual methods
#if PROJ_ENABLED
	PJ_CONTEXT *context = nullptr;
	std::map<std::string, PJ *> projections; // key e.g. "28896-4326"
#endif

  public:
	virtual ~dataset_processor();

	/// <summary>
	/// Virtual major method for processors. Must be implemented by children.
	/// </summary>
	/// <param name="options">Job to process.</param>
	/// <returns>State of processing.</returns>
	virtual IWD::exit_code process(std::shared_ptr<job_processing_options> options) = 0;

	// mode_2d: if posititions are 3d, just use x-z coordinates

	/// <summary>
	/// Centers 'positions' so that bounding box center is (0,0,0).
	/// </summary>
	/// <typeparam name="T">Type of 'positions'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="positions">Positions to center.</param>
	/// <param name="mode_2d">If position is 3D, centers only x and z coordinates, skipping y (see
	/// ::level_data).</param>
	template <typename T> void center_data(T &positions, bool mode_2d = false)
	{
		auto bbox =
		    components::bounding_box_t<std::remove_reference_t<decltype(positions)>::value_type::value_dimensions,
		                               typename std::remove_reference_t<decltype(positions)>::value_type::value_type>();

		bbox.calculate(positions);

		auto center = (bbox.min + bbox.max) * 0.5;
		// if 2d positions, do nothing
		if constexpr (std::remove_reference_t<decltype(positions)>::value_type::value_dimensions == 3)
			if (mode_2d) center.y = 0.0;

		for (auto &p : positions) {
			p -= center;
		}
	}

	/// <summary>
	/// Levels y component of 'positions' so tht bounding box .y = 0.
	/// </summary>
	/// <typeparam name="T">Type of 'positions'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="positions">Positions to level.</param>
	template <typename T> void level_data(T &positions)
	{
		if constexpr (std::remove_reference_t<decltype(positions)>::value_type::value_dimensions == 3) {
			return;
		}
		else {
			auto bbox =
			    components::bounding_box_t<std::remove_reference_t<decltype(positions)>::value_type::value_dimensions,
			                               typename std::remove_reference_t<decltype(positions)>::value_type::value_type>();

			bbox.calculate(positions);

			for (auto &p : positions) {
				p -= bbox.min.y;
			}
		}
	}

	/// <summary>
	/// Convenience function for calculating a bounding box from positions. Automatically deduces necessary types.
	/// </summary>
	/// <typeparam name="T">Type of 'positions'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="positions">Positions to calculate the bounding box for.</param>
	/// <returns></returns>
	template <typename T> decltype(auto) calculate_bbox(const T &positions)
	{
		auto bbox =
		    components::bounding_box_t<std::remove_reference_t<decltype(positions)>::value_type::value_dimensions,
		                               typename std::remove_reference_t<decltype(positions)>::value_type::value_type>();
		bbox.calculate(positions);

		return bbox;
	}

	/// <summary>
	/// Reverse the winding order of all indices. Useful for left-hand to right-hand coordinate systems or when input
	/// indices are swizzled.
	/// </summary>
	/// <typeparam name="T">Type of 'indices'. T::value_type must be index_t<3>. Can usually be omitted through C++17
	/// CTAD.</typeparam> <param name="indices">Indices to reverse the order of.</param>
	template <typename T> void reverse_triangle_winding(T &indices)
	{
		if constexpr (T::value_type::value_dimensions == 3)
			for (auto &i : indices) {
				std::swap(i.y, i.z);
			}
	}

	/// <summary>
	/// Calculates minima and maxima of a given container. Uses glm::min and glm::max in case of T::value_type is child
	/// of glm::vec.
	/// </summary>
	/// <typeparam name="T">Type of 'values'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="values">Values to calculate minima and maxima.</param>
	/// <returns></returns>
	template <typename T> decltype(auto) calculate_minmax(const T &values)
	{
		if (values.empty()) return std::make_tuple(typename T::value_type(), typename T::value_type());

		auto min = values[0];
		auto max = values[0];

		// underlying data is of some kind of glm::vec derivate
		if constexpr (std::is_base_of_v<glm::vec<T::value_type::value_dimensions, typename T::value_type::value_type>,
		                                typename T::value_type>) {
			for (const auto &v : values) {
				min = glm::min(
				    min, static_cast<glm::vec<T::value_type::value_dimensions, typename T::value_type::value_type>>(v));
				max = glm::max(
				    max, static_cast<glm::vec<T::value_type::value_dimensions, typename T::value_type::value_type>>(v));
			}
		}
		else {
			for (const auto &v : values) {
				min = std::min(min, v);
				max = std::max(max, v);
			}
		}

		return std::make_tuple(min, max);
	}

#if PROJ_ENABLED
	/// <summary>
	/// Projects 'positions' from one coordinate system 'epsg_in' to another 'epsg_out'. Stores meta information
	/// ::components::rtc_info and latitude + longitude ::components::common::lat_long.
	/// </summary>
	/// <typeparam name="T">Type of 'positions'. Can usually be omitted through C++17 CTAD.</typeparam>
	/// <param name="dataset">Dataset to store RTC info, latitude and longitude.</param>
	/// <param name="positions">Positions to project</param>
	/// <param name="epsg_in">Source EPSG.</param>
	/// <param name="epsg_out">Target EPSG.</param>
	/// <returns>True if projection was successful.</returns>
	template <typename T>
	bool project_CRS_to_CRS(std::shared_ptr<dataset> dataset, T &positions, uint32_t epsg_in, uint32_t epsg_out,
	                        size_t timestep)
	{
		if (epsg_in == 0 || epsg_out == 0) {
			util::log::error("{dataset_processor}",
			                 fmt::format("EPSG cannot be 0. epsg_in: {} epsg_out: {}", epsg_in, epsg_out));
			return false;
		}

		auto &bbox = dataset->emplace_or_replace_component<components::common::bounding_box_3D>(
		    calculate_bbox(positions), timestep);

		if (!dataset->has_component<components::rtc_info>(0)) dataset->emplace_component<components::rtc_info>({}, 0);
		auto &rtc_info = dataset->get_component<components::rtc_info>(0);
		rtc_info.center_original = (bbox.min + bbox.max) / 2.0;

		PJ *projection = nullptr;
		auto pj_loc = projections.find(fmt::format("{0}-{1}", epsg_in, epsg_out));
		if (pj_loc != projections.end()) {
			projection = pj_loc->second;
		}
		else {
			// really heavy function!
			projection = proj_create_crs_to_crs(context, fmt::format("EPSG:{}", epsg_in).c_str(),
			                                        fmt::format("EPSG:{}", epsg_out).c_str(), nullptr);

			if (projection == nullptr || proj_errno(projection) != 0) {
				util::log::error("{dataset_processor}",
				                 fmt::format("Couldn't setup projection: {}", proj_errno(projection)));
				proj_context_destroy(context);

				return false;
			}
			else {
				projections.insert({fmt::format("{0}-{1}", epsg_in, epsg_out), projection});
			}
		}

		PJ_COORD pj_origin = proj_trans(
		    projection, PJ_FWD, proj_coord(rtc_info.center_original.x, rtc_info.center_original.z, 0.0, HUGE_VAL));

		// store information for glTF export
		rtc_info.center_new = IWD::util::wgs84::get_position(proj_torad(pj_origin.lp.lam), proj_torad(pj_origin.lp.phi),
		                                                     rtc_info.center_original.y);

		const glm::dmat4 y_up_to_z_up = {1.0, 0.0, 0.0,  0.0, /**/
		                                 0.0, 0.0, -1.0, 0.0, /**/
		                                 0.0, 1.0, 0.0,  0.0, /**/
		                                 0.0, 0.0, 0.0,  1.0};

		auto &lat_long = dataset->emplace_or_replace_component<components::common::lat_long>({}, 0);
		lat_long.reserve(positions.size());

		for (auto &p : positions) {
			PJ_COORD pj = proj_trans(projection, PJ_FWD, proj_coord(p.x, p.z, 0.0, HUGE_VAL));
			lat_long.push_back(glm::dvec2(pj.lp.phi, pj.lp.lam));

			glm::dvec3 pos = IWD::util::wgs84::get_position(proj_torad(pj.lp.lam), proj_torad(pj.lp.phi), p.y);

			auto p_tmp = pos - rtc_info.center_new;
			p = glm::dvec3(y_up_to_z_up * glm::dvec4(p_tmp, 1.0));
		}

		return true;
	}

#endif // !PROJ_ENABLED
};

} // namespace IWD::data