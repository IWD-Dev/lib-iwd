#pragma once

#include <limits>

#include "util/auto_enum.h"

namespace IWD::data::components {

template <glm::length_t V, typename T> using vec = glm::vec<V, T, glm::packed_highp>;
template <typename T> using vec1 = glm::tvec1<T, glm::packed_highp>;
template <typename T> using vec2 = glm::tvec2<T, glm::packed_highp>;
template <typename T> using vec3 = glm::tvec3<T, glm::packed_highp>;

// -- types depending on storage implementation -- //

/// <summary>
/// Position type
/// </summary>
/// <typeparam name="V">Length of the Vector. Valid values are 1 to 4.</typeparam>
/// <typeparam name="T">Type of the Vector. Valid types are float, double, int, unsigned int.</typeparam>
template <glm::length_t V, typename T> struct position_t : public vec<V, T> {
	using value_type = T;
	const static glm::length_t value_dimensions = V;

	position_t<V, T>() = default;
	position_t<V, T>(const vec<V, T> &v) noexcept { vec<V, T>::operator=(v); }
	position_t<V, T> &operator=(const vec<V, T> &v) noexcept
	{
		vec<V, T>::operator=(v);
		return *this;
	}
	position_t<V, T> &operator=(vec<V, T> &&v) noexcept
	{
		vec<V, T>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Longitute and latitude component in degrees.
/// </summary>
/// <typeparam name="T">Type of the Vector. Valid types are float, double.</typeparam>
template <typename T> struct long_lat_t : public vec2<T> {
	using value_type = T;

	long_lat_t<T>() = default;
	long_lat_t<T>(const vec2<T> &v) noexcept { vec2<T>::operator=(v); }
	long_lat_t<T> &operator=(const vec2<T> &v) noexcept
	{
		vec2<T>::operator=(v);
		return *this;
	}
	long_lat_t<T> &operator=(vec2<T> &&v) noexcept
	{
		vec2<T>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Velocity type
/// </summary>
/// <typeparam name="V">Length of the Vector. Valid values are 1 to 4.</typeparam>
/// <typeparam name="T">Type of the Vector. Valid types are float, double, int, unsigned int.</typeparam>
template <glm::length_t V, typename T> struct velocity_t : public vec<V, T> {
	using value_type = T;
	const static glm::length_t value_dimensions = V;

	velocity_t<V, T>() = default;
	velocity_t<V, T>(const vec<V, T> &v) noexcept { vec<V, T>::operator=(v); }
	velocity_t<V, T> &operator=(const vec<V, T> &v) noexcept
	{
		vec<V, T>::operator=(v);
		return *this;
	}
	velocity_t<V, T> &operator=(vec<V, T> &&v) noexcept
	{
		vec<V, T>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Depth type
/// </summary>
/// <typeparam name="T">Type of the Component. Valid types are float, double, int, unsigned int.</typeparam>
template <typename T> struct depth_t : public vec1<T> {
	using value_type = T;
	const static glm::length_t value_dimensions = 1;

	depth_t<T>() = default;
	depth_t<T>(const vec1<T> &v) noexcept { vec1<T>::operator=(v); }
	depth_t<T> &operator=(const vec1<T> &v) noexcept
	{
		vec1<T>::operator=(v);
		return *this;
	}
	depth_t<T> &operator=(vec1<T> &&v) noexcept
	{
		vec1<T>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Height type
/// </summary>
/// <typeparam name="T">Type of the Component. Valid types are float, double, int, unsigned int.</typeparam>
template <typename T> struct height_t : public vec1<T> {
	using value_type = T;
	const static glm::length_t value_dimensions = 1;

	height_t<T>() = default;
	height_t<T>(const vec1<T> &v) noexcept { vec1<T>::operator=(v); }
	height_t<T> &operator=(const vec1<T> &v) noexcept
	{
		vec1<T>::operator=(v);
		return *this;
	}
	height_t<T> &operator=(vec1<T> &&v) noexcept
	{
		vec1<T>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Valid type
/// </summary>
struct valid_t : public vec1<bool> {
	valid_t &operator=(const vec1<bool> &v) noexcept
	{
		vec1<bool>::operator=(v);
		return *this;
	}
	valid_t &operator=(vec1<bool> &&v) noexcept
	{
		vec1<bool>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Possible cell types. Currently unused.
/// </summary>
enum struct cell_type : int32_t { error = -1, wet = 0, dry, ground };

/// <summary>
/// Cell type. See struct 'cell_type'.
/// </summary>
struct kind_t : public vec1<cell_type> {
	kind_t &operator=(const vec1<cell_type> &v) noexcept
	{
		vec1<cell_type>::operator=(v);
		return *this;
	}
	kind_t &operator=(vec1<cell_type> &&v) noexcept
	{
		vec1<cell_type>::operator=(std::move(v));
		return *this;
	}
};

// -- types unaffected by storage implementation -- //

/// <summary>
/// Index type
/// </summary>
/// <typeparam name="V">Length of the Vector. Valid values are 1 to 4.</typeparam>
template <glm::length_t V> struct index_t : public vec<V, int32_t> {
	const static glm::length_t value_dimensions = V;

	index_t() = default;
	index_t(const vec<V, int32_t> &v) noexcept { vec<V, int32_t>::operator=(v); }
	index_t &operator=(const vec<V, int32_t> &v) noexcept
	{
		vec<V, int32_t>::operator=(v);
		return *this;
	}
	index_t &operator=(vec<V, int32_t> &&v) noexcept
	{
		vec<V, uint32_t>::operator=(std::move(v));
		return *this;
	}
};

/// <summary>
/// Bounding type
/// </summary>
/// <typeparam name="V">Dimension of the Bounding Box. Valid values are 1 to 4.</typeparam>
/// <typeparam name="T">Type of the Bounding Box. Valid types are float, double, int, unsigned int.</typeparam>
template <glm::length_t V, typename T> struct bounding_box_t {
	using value_type = T;
	const static glm::length_t value_dimensions = V;

	position_t<V, T> min;
	position_t<V, T> max;

	/// <summary>
	/// Default constructor initializes Bounding Box with numerical limits for template type T.
	/// </summary>
	/// <returns></returns>
	bounding_box_t() noexcept
	{
		min = vec<V, T>{std::numeric_limits<T>::max()};
		max = vec<V, T>{-std::numeric_limits<T>::max()};
	};

	/// <summary>
	/// Calculates and sets Bounding box from given position container
	/// </summary>
	/// <typeparam name="U">Type of 'positions'. Can usually be omitted since C++17 CTAD.</typeparam>
	/// <param name="positions">Positions to calculate Bounding Box of. Must be a forward-iterable container, e.g.
	/// std::vector<position_t<V, T>>.</param>
	template <typename U> void calculate(const U &positions)
	{
		min = vec<V, T>{std::numeric_limits<T>::max()};
		max = vec<V, T>{-std::numeric_limits<T>::max()};

		// internally containing position_t<V, T>
		for (vec<V, T> p : positions) {
			min = glm::min(min, p);
			max = glm::max(max, p);
		}
	}

	/// <summary>
	/// Checks if 'pos' is within the Bounding Box.
	/// </summary>
	/// <param name="pos">Position to check.</param>
	/// <returns>True if inside.</returns>
	bool contains(const position_t<V, T> &pos)
	{
		bool ret = true;
		for (glm::length_t i = 0; i < value_dimensions; ++i) {
			ret &= (pos[i] >= min[i] && pos[i] < max[i]);
		}
		return ret;
	}

	/// <summary>
	/// Checks if 'pos' is within the Bounding Box. In case of 3D or 4D 'pos', uses .x and .z component only. Useful
	/// for 2.5D datasets where no overlap occurs.
	/// </summary>
	/// <param name="pos">Position to check.</param>
	/// <returns>True if inside.</returns>
	bool contains_2d(const position_t<V, T> &pos)
	{
		auto idx_2d = value_dimensions > 2 ? 2 : 1;
		bool ret = true;
		ret &= (pos[0] >= min[0] && pos[0] < max[0]);
		ret &= (pos[idx_2d] >= min[idx_2d] && pos[idx_2d] < max[idx_2d]);
		return ret;
	}
};

/// <summary>
/// Wrapper containing relevant information for datasets that are "Relative To Coordinate" (RTC).
/// </summary>
struct rtc_info {
	/// <summary>
	/// Center in original (unprojected) coordinates.
	/// </summary>
	position_t<3, double> center_original;

	/// <summary>
	/// Center in new (projected) coordinates.
	/// </summary>
	position_t<3, double> center_new;

	/// <summary>
	/// Offset used to calculate RTC. Can be added to a position to restore pre-RTC (but projected) coordinate.
	/// </summary>
	position_t<3, double> offset;
};

} // namespace IWD::data::components
