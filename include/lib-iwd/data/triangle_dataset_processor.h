#pragma once

#include <memory>
#include <tuple>

#include "triangle_dataset.h"
#include "data/dataset_processor.h"

namespace IWD::data {

/// <summary>
/// Processor for triangle datasets.
/// </summary>
class triangle_dataset_processor : public dataset_processor {

  public:
	/// <summary>
	/// All-in-One main method to run. Processes a whole dataset by given 'options'.
	/// </summary>
	/// <param name="options">Options for processing.</param>
	/// <returns>Various IWD::exit_codes depending on processing.</returns>
	IWD::exit_code process(std::shared_ptr<job_processing_options> options) override;

	/// <summary>
	/// Tiles and splits a dataset into multiple.
	/// </summary>
	/// <param name="dataset">Dataset to split.</param>
	/// <param name="tile_size">Tile size to split on.</param>
	/// <param name="tile_epsg">Currently unused.</param>
	/// <returns>result.first = True if everything went ok. result.second = Vector of tiled datasets.</returns>
	std::tuple<bool, std::vector<std::shared_ptr<triangle_dataset>>>
	tile_dataset(std::shared_ptr<triangle_dataset> dataset, double tile_size, uint32_t tile_epsg = 0u);

	/// <summary>
	/// Removes all dry cells (triangles, quads) by marking all totally dry vertices over all timesteps.
	/// </summary>
	/// <param name="dataset">Dataset to remove dry cells from.</param>
	/// <param name="dry_threshold">Depth in cm to consider dry. Default 1cm.</param>
	/// <returns>True if removal went ok.</returns>
	bool remove_dry_cells(std::shared_ptr<triangle_dataset> dataset, double dry_threshold = 0.01);

	/// <summary>
	/// temporary storage for lod-bound datasets
	/// </summary>
	struct dataset_lod {
		/// <summary>
		/// Vector later may store tiles under the same lod.
		/// </summary>
		std::vector<std::shared_ptr<triangle_dataset>> datasets;
		uint32_t lod;
	};

	/// <summary>
	/// Simplifies mesh and underlying vertex data to fit target_vertex_count.
	/// </summary>
	/// <param name="dataset">Dataset to simplify.</param>
	/// <param name="lod_levels">How many lods shall be generated. Every iteration has n/2 vertices.</param>
	/// <returns>result.first = True if everything went ok. result.second = Vector of lod reduced datasets.</returns>
	std::tuple<bool, std::vector<dataset_lod>>
	simplify_mesh(std::shared_ptr<triangle_dataset> dataset, uint32_t lod_levels = 3);

	/// <summary>
	/// Linearized various data in a single binary buffer, mainly for exporting to glTF.
	/// </summary>
	/// <param name="dataset">Dataset to linearize.</param>
	/// <param name="timestep">Timestep to choose data of.</param>
	/// <returns>Raw binary buffer block.</returns>
	std::vector<std::byte> linearize_data(std::shared_ptr<triangle_dataset> dataset, size_t timestep);
};

} // namespace IWD::data