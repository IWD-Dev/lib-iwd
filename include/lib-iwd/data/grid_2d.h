#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <cassert>
#include <algorithm>

namespace IWD::data {

/// <summary>
/// Currently unused!
/// </summary>
/// <typeparam name="T">Type of stored data.</typeparam>
template <typename T> class grid_2d {

	// TODO: may implement as random access iterator
	// https://stackoverflow.com/questions/3582608/how-to-correctly-implement-custom-iterators-and-const-iterators

	// template <typename T> class grid_2d_iterator;

  public:
	class grid_2d_iterator {
		friend class grid_2d<T>;

	  public:
		using iterator_category = std::forward_iterator_tag;
		using value_type = T;
		using difference_type = ptrdiff_t;
		using pointer = grid_2d<T> *;
		using reference = T &;

	  private:
		typename std::vector<T>::iterator curr;
		int32_t col;
		int32_t row;
		pointer ptr;

	  public:
		grid_2d_iterator(pointer _ptr) : ptr(_ptr), col(0), row(0) {}

		grid_2d_iterator(const grid_2d_iterator &other)
		{
			curr = other.curr;
			col = other.col;
			row = other.row;
			ptr = other.ptr;
		}

		grid_2d_iterator &operator=(const grid_2d_iterator &other)
		{
			curr = other.curr;
			col = other.col;
			row = other.row;
			ptr = other.ptr;
			return *this;
		}

		grid_2d_iterator &operator++() // pre
		{
			col = (col + 1) % ptr->ncols();
			if (col != 0)
				++curr;
			else {
				++row;
				curr = row < ptr->nrows() ? ++curr : ptr->end().get_curr();
			}
			return *this;
		}

		grid_2d_iterator operator++(int) // post
		{
			grid_2d_iterator ret(*this);
			++(*this);
			return ret;
		}

		T *operator->() const { return &(operator*()); }

		bool operator==(const grid_2d_iterator &other) const
		{
			// IMPROVE: handle check in release
			assert(this->ptr->ncols() == other.ptr->ncols() && this->ptr->nrows() == other.ptr->nrows());

			return curr == other.curr;
		}
		bool operator==(grid_2d_iterator &&other) const
		{
			// IMPROVE: handle check in release
			assert(this->ptr->ncols() == other.ptr->ncols() && this->ptr->nrows() == other.ptr->nrows());

			return curr == std::move(other.curr);
		}

		bool operator!=(const grid_2d_iterator &other) const { return !(*this == other); }

		T &operator*() { return *curr; }

		auto get_curr() const -> decltype(curr) { return curr; }
		int32_t get_col() const { return col; }
		int32_t get_row() const { return row; }
	};

	//---------------------------------------------------------------//

	grid_2d_iterator begin()
	{
		grid_2d_iterator it(this);
		it.curr = data.begin();
		return it;
	}

	grid_2d_iterator end()
	{
		grid_2d_iterator it(this);
		it.curr = data.end();
		return it;
	}

	grid_2d(int32_t ncols = 1, int32_t nrows = 1)
	{
		assert(ncols != 0 && nrows != 0);
		/*if (ncols == 0 || nrows == 0)
		    throw std::domain_error("ncols and nrows musn't be 0; ncols: " + std::to_string(ncols) +
		                            " nrows: " + std::to_string(nrows));*/

		this->cols = ncols;
		this->rows = nrows;
		this->data.resize((rows) * (cols));
	}

	virtual ~grid_2d() = default;

	std::vector<T>& get_data() { return data; }
	std::vector<T>& get_data() const { return data; }

	// direct access
	T &at_idx(int32_t n) { return data[n]; };
	const T &at_idx(int32_t n) const { return data[n]; };

	// ranged access
	T &at(int32_t col, int32_t row) { return data[row * cols + col]; }
	const T &at(int32_t col, int32_t row) const { return data[row * cols + col]; }

	// ranged access with clamp
	T &at_clamp(int32_t col, int32_t row)
	{
		int32_t s_col = std::clamp(col, 0, cols - 1);
		int32_t s_row = std::clamp(row, 0, rows - 1);
		return data[s_row * cols + s_col];
	}
	const T &at_clamp(int32_t col, int32_t row) const
	{
		int32_t s_col = std::clamp(col, 0, cols - 1);
		int32_t s_row = std::clamp(row, 0, rows - 1);
		return data[s_row * cols + s_col];
	}

	// direct access
	inline T &operator[](int32_t idx) { return at_idx(idx); };
	inline const T &operator[](int32_t idx) const { return at_idx(idx); };

	// ranged access
	inline T &operator()(int32_t col, int32_t row) { return at(col, row); };
	inline const T &operator()(int32_t col, int32_t row) const { return at(col, row); };

	// get copy of column
	std::vector<T> get_column(int32_t column)
	{
		assert(column >= 0 && column < cols);

		std::vector<T> new_column{};
		new_column.reserve(rows);
		for (int32_t row = 0; row < rows; ++row) {
			new_column.push_back(this->at(column, row));
		}
		return new_column;
	}

	// get copy of column
	std::vector<T> get_column(int32_t column) const
	{
		assert(column >= 0 && column < cols);

		std::vector<T> new_column{};
		new_column.reserve(rows);
		for (int32_t row = 0; row < rows; ++row) {
			new_column.push_back(this->at(column, row));
		}
		return new_column;
	}

	// get copy of column
	std::vector<T> get_row(int32_t row)
	{
		assert(row >= 0 && row < rows);

		std::vector<T> new_row{};
		new_row.reserve(cols);
		for (int32_t col = 0; col < cols; ++col) {
			new_row.push_back(this->at(col, row));
		}
		return new_row;
	}

	// get copy of column
	std::vector<T> get_row(int32_t row) const
	{
		assert(row >= 0 && row < rows);

		std::vector<T> new_row{};
		new_row.reserve(cols);
		for (int32_t col = 0; col < cols; ++col) {
			new_row.push_back(this->at(col, row));
		}
		return new_row;
	}

	int32_t ncols() noexcept { return cols; }
	int32_t ncols() const noexcept { return cols; }
	int32_t nrows() noexcept { return rows; }
	int32_t nrows() const noexcept { return rows; }

	void fill(T value) { std::fill(std::begin(data), std::end(data), value); }

	// TODO: these should do something sane instead
	// for now, leave it so that templates compile
	void shring_to_fit() { data.shrink_to_fit(); }
	void reserve(size_t size) { data.reserve(size); }

	/*std::string to_string(bool with_ghost = false)
	{
	    std::string str{};

	    if (!with_ghost) {
	        for (int32_t row = 0; row < rows; ++row) {
	            for (int32_t col = 0; col < cols - 1; ++col) {
	                str += std::to_string(at(col, row)) + "\t";
	            }
	            str += std::to_string(at(cols, row)) + "\n";
	        }
	    }
	    else {
	        for (int32_t row = 0; row < rows + 2; ++row) {
	            for (int32_t col = 0; col < cols + 1; ++col) {
	                str += std::to_string(data[row * (cols + 2) + col]) + "\t";
	            }
	            str += std::to_string(data[row * (cols + 2) + cols + 1]) + "\n";
	        }
	    }
	    return str;
	}*/
	/*std::string to_string(bool with_ghost = false) const
	{
	    std::string str{};

	    if (!with_ghost) {
	        for (int32_t row = 0; row < rows; ++row) {
	            for (int32_t col = 0; col < cols - 1; ++col) {
	                str += std::to_string(at(col, row)) + "\t";
	            }
	            str += std::to_string(at(cols, row)) + "\n";
	        }
	    }
	    else {
	        for (int32_t row = 0; row < rows + 2; ++row) {
	            for (int32_t col = 0; col < cols + 1; ++col) {
	                str += std::to_string(data[row * (cols + 2) + col]) + "\t";
	            }
	            str += std::to_string(data[row * (cols + 2) + cols + 1]) + "\n";
	        }
	    }
	    return str;
	}*/

  private:
	int32_t cols;
	int32_t rows;
	std::vector<T> data;
};

} // namespace IWD::data