#pragma once

#include <memory>
#include <tuple>

#include "data/dataset_processor.h"
#include "grid_dataset.h"

namespace IWD::data {

/// <summary>
/// Currently unused!
/// </summary>
class grid_dataset_processor : public dataset_processor {

  public:
	IWD::exit_code process(std::shared_ptr<job_processing_options> options) override;

	std::tuple<bool, std::vector<std::shared_ptr<grid_dataset>>>
	tile_dataset(std::shared_ptr<grid_dataset> dataset, double tile_size, uint32_t tile_epsg = 0u);

	std::vector<std::byte> linearize_data(std::shared_ptr<grid_dataset> dataset);
};

} // namespace IWD::data