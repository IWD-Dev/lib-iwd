#pragma once

#include <vector>

#include "data/dataset.h"

namespace IWD::data {

/// <summary>
/// Dataset specialization for triangle datasets.
/// </summary>
class triangle_dataset : public dataset {
  public:
	template <typename T> using storage = std::vector<T>;

	// pre-defined regularly used types
	using position_2D = storage<components::position_t<2, double>>;
	using position_3D = storage<components::position_t<3, double>>;
	using velocity_2D = storage<components::velocity_t<2, float>>;
	using velocity_3D = storage<components::velocity_t<3, float>>;
	using depth = storage<components::depth_t<float>>;
	using height = storage<components::height_t<float>>;
	using valid = storage<components::valid_t>;
	using kind = storage<components::kind_t>;


	triangle_dataset() = default;
	virtual ~triangle_dataset() = default;

	dataset::type get_type() noexcept override;

	size_t vertex_count() override;
	size_t index_count() override;
};

} // namespace IWD::data