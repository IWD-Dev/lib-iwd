#pragma once

#include <vector>
#include <map>
#include <unordered_map>

#include "data/dataset_components.h"

namespace IWD::data::components::common {

//IMPROVE: make struct (own types) instead

using bounding_box_2D = bounding_box_t<2, double>;
using bounding_box_3D = bounding_box_t<3, double>;

using triangle_index = std::vector<index_t<3>>;
using quad_index = std::vector<index_t<4>>;

using lat_long = std::vector<long_lat_t<double>>;


// [new_index -> old_index]
using index_map = std::map<uint32_t, uint32_t>; 
//using index_map = std::unordered_map<int32_t, int32_t>;  // unordered_map is sufficient

// raw binary data, condensed and linearized
using linearized_binary = std::vector<std::byte>;

// <length, offset>
using linearized_binary_offsets = std::vector<std::pair<size_t, size_t>>;

} // namespace IWD::data::components::common
