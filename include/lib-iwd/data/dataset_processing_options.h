#pragma once

#include <vector>
#include <memory>
#include <filesystem>
#include <string>

namespace IWD::data {

/// <summary>
/// Dataset input types.
/// </summary>
enum struct dataset_input_type : int32_t { error = -1, esri_ascii = 0, sms_2dm, mike21, geotiff, _3di, uninitialized = 999 };

/// <summary>
/// Dataset output types.
/// </summary>
enum struct dataset_output_type : int32_t { error = -1, obj = 0, stl, gltf, _3d_tiles, bin };

/// <summary>
/// List of Dataset output types which are handled by the ::triangle_dataset_processor.
/// </summary>
const std::vector<dataset_input_type> triangle_types = {dataset_input_type::sms_2dm, dataset_input_type::mike21,
                                                        dataset_input_type::_3di, dataset_input_type::geotiff};

/// <summary>
/// List of Dataset output types which are handled by the ::triangle_dataset_processor.
/// </summary>
const std::vector<dataset_input_type> grid_types = {/*dataset_input_type::esri_ascii*/};

/// <summary>
/// Contains all global options for processing a dataset.
/// Struct is unpacked, as doesn't matter for performance.
/// </summary>
struct dataset_processing_options_global {
	bool center_data = false;
	bool level_data = false;
	bool remove_dry_cells = false;
	bool gltf_embed = false;
	bool gltf_glb = false;
	uint32_t simplify = 1u;
	uint32_t epsg_input = 0u;
	uint32_t epsg_output = 0u;
	std::vector<dataset_output_type> output_formats;
	std::string output_timesteps = "";
	double tile_size = 0.0;
	uint32_t tile_epsg = 0;
	uint32_t lod_levels = 0;
};

/// <summary>
/// More specialized options. Besides general members from ::dataset_processing_options_global, adds input/output.
/// </summary>
struct job_processing_options : public dataset_processing_options_global {
	dataset_input_type input_type{dataset_input_type::uninitialized};
	std::filesystem::path output_directory;
	std::filesystem::path output_basename;

	/// <summary>
	/// Copies all members from parent object ::dataset_processing_options_global.
	/// </summary>
	/// <param name="options">Parent to copy from.</param>
	void copy_from_base(std::shared_ptr<dataset_processing_options_global> options) noexcept;
};

// might move to sms_2dm_reader.h
/// <summary>
/// SMS 2dm options. Specifies input files.
/// </summary>
struct sms_2dm_processing_options : public job_processing_options {
	std::filesystem::path input_mesh_file;
	std::filesystem::path input_depth_file;
	std::filesystem::path input_waterlevel_file;
	std::filesystem::path input_velocity_file;

	sms_2dm_processing_options() = default;
	sms_2dm_processing_options(std::shared_ptr<job_processing_options> options) noexcept;

	/// <summary>
	/// Copies all members from parent object ::job_processing_options and ::dataset_processing_options_global.
	/// </summary>
	/// <param name="options">Parent to copy from.</param>
	void copy_from_base(std::shared_ptr<job_processing_options> options) noexcept;
};

// might move to mike21_reader.h
/// <summary>
/// Mike21 options. Specifies input files.
/// </summary>
struct mike21_processing_options : public job_processing_options {
	std::filesystem::path input_mesh_file;
	std::filesystem::path input_depth_file;
	std::filesystem::path input_velocity_u_file;
	std::filesystem::path input_velocity_v_file;

	mike21_processing_options() = default;
	mike21_processing_options(std::shared_ptr<job_processing_options> options) noexcept;

	/// <summary>
	/// Copies all members from parent object ::job_processing_options and ::dataset_processing_options_global.
	/// </summary>
	/// <param name="options">Parent to copy from.</param>
	void copy_from_base(std::shared_ptr<job_processing_options> options) noexcept;
};

// might move to geotiff_reader.h
/// <summary>
/// GeoTIFF options. Specifies input files.
/// </summary>
struct geotiff_processing_options : public job_processing_options {
	std::filesystem::path input_dem_file;
	std::filesystem::path input_depth_file;
	std::filesystem::path input_velocity_u_file;
	std::filesystem::path input_velocity_v_file;

	geotiff_processing_options() = default;
	geotiff_processing_options(std::shared_ptr<job_processing_options> options) noexcept;

	/// <summary>
	/// Copies all members from parent object ::job_processing_options and ::dataset_processing_options_global.
	/// </summary>
	/// <param name="options">Parent to copy from.</param>
	void copy_from_base(std::shared_ptr<job_processing_options> options) noexcept;
};

// might move to 3di_reader.h
/// <summary>
/// 3Di options. Specifies input files.
/// </summary>
struct _3di_processing_options : public job_processing_options {
	std::filesystem::path input_file;

	_3di_processing_options() = default;
	_3di_processing_options(std::shared_ptr<job_processing_options> options) noexcept;

	/// <summary>
	/// Copies all members from parent object ::job_processing_options and ::dataset_processing_options_global.
	/// </summary>
	/// <param name="options">Parent to copy from.</param>
	void copy_from_base(std::shared_ptr<job_processing_options> options) noexcept;
};

} // namespace IWD::io