#pragma once

#include <string>

namespace IWD {
/// <summary>
/// Exit code of the program.
/// </summary>
enum exit_code : int {
	normal = 0 /** 0 - Everything went ok. */,
	invalid_arguments /** 1 - Some invalid arguments occurred. */,
	read_error /** 2 - Error while reading files. */,
	processing_error /** 3 - Error while processing datasets. */,
	write_error /** 4 - Error while writing data. */
};

namespace version {

	const uint32_t LIB_IWD_MAJOR_VERSION = 0;
	const uint32_t LIB_IWD_MINOR_VERSION = 5;
	const uint32_t LIB_IWD_PATCH_VERSION = 0;

	/// <summary>
	/// Converts version number to string.
	/// </summary>
	/// <returns>Version number as std::string.</returns>
	inline static std::string to_string()
	{
		return std::to_string(LIB_IWD_MAJOR_VERSION) + "." + std::to_string(LIB_IWD_MINOR_VERSION) + "." +
		       std::to_string(LIB_IWD_PATCH_VERSION);
	}

} // namespace version

} // namespace IWD