# lib-IWD

---

## What is lib-IWD?

lib-IWD is a multi-functional library for importing, processing and exporting simulation data from surface runoff and flood models written in C++17. It supports various different kinds of input and output formats and is able to process them in a systemized way. Besided the library itself, a CLI program is available and will be built if selected.




## Features

* Compact, adaptive and extensible data storage using the Entity Component System (ECS) [entt] (https://github.com/skypjack/entt)
* Handling of common spatiotemporal surface-runoff data (mesh, depth, height, velocity)
* Dataformat agnostic processing operations such as:
 * Projecting positions from one EPSG to another EPSG (for georeferenced engines)
 * Transforming to local coordinates for better IEEE754 float_32 accuracy on GPU (for non-georeferenced engines)
 * Splitting datasets into multiple tiled subdatasets
     * Performing accurate splits and reinterpolation on triangles
 * Removal of dry cells (triangles) from dataset, keeping the export small
 * Generation of multiple Level of Detail variants
 * Reading, processing and exporting multiple datasets in batch


* Currently importable data formats:
    * [3Di Water Management] (https://docs.3di.live/c_results.html)
    * [Aquaveo SMS 2dm] (https://www.aquaveo.com/software/sms-surface-water-modeling-system-introduction) (ascii)
    * [DHI MIKE21] (https://www.mikepoweredbydhi.com/products) (node-centered, ascii)
    * [GeoTIFF] (http://docs.opengeospatial.org/is/19-008r4/19-008r4.html)

* Currently exportable data formats:
    * Wavefront OBJ
    * STL (ascii)
    * [glTF 2.0] (https://github.com/KhronosGroup/glTF/tree/master/specification/2.0) (external, base64-embedded, glb)
    * [Cesium 3D Tiles] (https://github.com/CesiumGS/3d-tiles)
        * [Batched 3D Model] (https://github.com/CesiumGS/3d-tiles/blob/master/specification/TileFormats/Batched3DModel/README.md)
	    
---

## How to build:

The primary way to build the lib-IWD library is to generate appropriate build files for your system using [CMake](https://cmake.org/).

### Core-functionality

In it's most basic configuration, lib-IWD requires no further external dependencies. 
All of the core third party sources are included within the repository, e.g. ./include/vendor.
  
### Extended functionality
  
There are several external dependencies when building the full library. The easiest, cross-platform way to build and install all required dependencies is to use [vcpkg](https://github.com/microsoft/vcpkg). After bootstrapping and installing vcpkg make sure to add it's root directory into the VCPKG_ROOT environment variable.

Afterwards install packages according to your systems triplet (e.g. package_name:x64-windows, package_name2:x64-linux):

* Projection of georeferenced coordinate systems via EPSG
    * *proj* + [ *libjpeg-turbo, liblzma, zlib, sqlite3, tiff* ]
* GeoTIFF support
    * *libgeotiff* + [ *proj* ]
* NetCDF support (3Di)
    * *netcdf-cxx4* + [ *szip, zlib, curl, hdf5, netcdf-c* ]


## Deployment:

* Ship the application with:
    * Proj resources found under "$PROJ_DIR/share/proj". (e.g. ./IWD-CLI.exe, ./share/proj/#resources#)
    * All required .dll/.so which are automatically copied to build directory by vcpkg.
 
## Disclaimer:
The lib-IWD library is working, yet still in development.

Developed by Lars Backhaus,
Technische Universität Dresden - Institute of Hydraulic Engineering and Technical Hydromechanics.

# Config File Specification

A config file is a JSON text file representing one to multiple jobs to process. It contains a main object consisting of an object **global_settings** and an array **jobs**.

```yaml
{
  "global_settings": {},
  "jobs": [{}]
}
```

## global_settings

The **global_settings** object contains settings for all datasets, which **jobs** will inherit from. All settings and the **global_settings** object itself are optional. Valid settings are:

| Option   		| Type  	| Default 	| Comment |
|---|---|---|---|
| epsg_input  		| int  	| 0  		| CRS to transform positions from. May not be needed.  |
| epsg_output  		| int  	| 0  		| CRS to transform positions into.  |
| gltf_glb			| bool   	| false  	| Exports gltf as binary glb.  |
| gltf_embed  		| bool   	| false  	| Embeds payload as base64 encoded data.  |
| lod_levels  		| int   	| 0  		| Generates N LoD Levels with (#triangles/2) each, Range: [0-3].  |
| output_formats  	| [string]	| []  	| Possible values: "obj", "stl", "gltf", "3d_tiles"  |
| output_timesteps  	| string	| ""  	| Concatenated list of timesteps. See below.  |
| remove\_dry\_cells 	| bool   	| false  	| Removes cells that are never wet (all timesteps).  |
| tile_size  		| float   	| 0.0  	| Splits dataset into tiles by tile_size meters in original CRS.  |
  
     
### output_timesteps

Selected timesteps via **output_timesteps** follow a specific syntax which can be freely combined and will be clamped to valid ranges:

| Value | Result |
|---|---|
| "0,3,5" | Exports timesteps 0,3 and 5.|
| "1-7" | Exports timesteps 1 to 7. |
| "3000.45T" | Exports the timestep nearest to simulation time 3000.45. |
| "100T-680.5T" | Exports all timesteps from the nearest to simulation time 100 to 680.5. |
| "5+" | Export all timesteps starting from 5 onwards. |
| "start" | Export first timestep. Equal to "0". |
| "end" | Exports last timestep. Good, if unknown. |
| "all" | Exports all timesteps. Equal to "0+". |
| "last_*N*" | Exports *N* last timesteps" |
| "0,3-5,500T,last_3" | Exports timestep 0, 3 to 5, nearest to simulation time 500 and the last 3. |

## jobs

The *job* object within the **jobs** array is defined by **input**, **output**, **type** and **settings**.
Processing **settings** are optional and inherit from the **global_settings**. They will overwrite the inherited values.

The member names will follow a set convention if applicable.

```yaml
{
  "jobs": [{
  	"input": {},
  	"output": {},
  	"type": string,
  	"settings": {}
  }]
}
```

### input

The **input** object will vary depending on the given **type**.  Valid input **type**s are:

| Attribute | Values |
|---|---|
| type | "sms_2dm", "mike21", "geotiff", "3di" |


#### SMS 2dm
| Attribute | Description |
|---|---|
| mesh | ASCII file containing mesh data. |
| depth | ASCII file containing depth data. |
| waterlevel | ASCII file containing waterlevel (MSL) data. |
| velocity | ASCII file containing velocity data (2D). |

#### Mike 21
| Attribute | Description |
|---|---|
| mesh | ASCII file containing mesh data. |
| depth | ASCII file containing depth data. |
| velocity_u | ASCII file containing velocity data (u \| x component). |
| velocity_v | ASCII file containing velocity data (v \| y component). |

#### GeoTIFF
| Attribute | Description |
|---|---|
| dem | GeoTIFF file containing dem data. |
| depth | GeoTIFF file containing depth data. |
| velocity_u | GeoTIFF file containing velocity data (u \| x component). |
| velocity_v | GeoTIFF file containing velocity data (v \| y component). |


#### 3Di
| Attribute | Description |
|---|---|
| nc | NetCDF4 file containing all required data. |

### output

The **output** objects currently stays the same for any given **type**.

File names will be generated from **directory** and **basename** plus identifier, timestep and LoD.

```yaml
"output": {
  "basename": "example",
  "directory": "PATH_TO_OUTPUT_DIRECTORY"
}
```

---

# Example

```yaml
{
  "global_settings": {
	"epsg_input" : 25833,
	"epsg_output": 4326,
	"gltf_glb": true,
    "output_formats": [
      "3d_tiles"
    ],
    "remove_dry_cells": true,
	"lod_levels": 3,
    "tile_size": 10000.0
  },
  "jobs": [
    {
      "input": {
        "depth": "PATH_TO_DEPTH_FILE.txt",
        "mesh": "PATH_TO_MESH_FILE.mesh",
        "velocity_u": "PATH_TO_VELOCITY_U_FILE.txt",
        "velocity_v": "PATH_TO_VELOCITY_V_FILE.txt"
      },
      "output": {
        "basename": "example",
        "directory": "PATH_TO_OUTPUT_DIRECTORY"
      },
      "settings": {
        "output_timesteps": "end"
      },
      "type": "mike21"
    }
  ]
}
```
