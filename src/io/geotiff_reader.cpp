#if GEOTIFF_ENABLED

#include "io/geotiff_reader.h"

#include "tiff.h"
#include "xtiffio.h"
#include "geotiffio.h"

#include "util/io.h"
#include "util/algorithm.h"

using namespace IWD::util;

const std::string LOG_TAG = "{geotiff_reader}";

namespace IWD::io {

bool geotiff_reader::read(std::shared_ptr<data::triangle_dataset> dataset,
                          std::shared_ptr<const data::geotiff_processing_options> options, uint32_t &epsg)
{

	auto res_dem = read_scalar(dataset, options->input_dem_file, geotiff_reader::scalar_type::dem);
	auto res_depth = read_scalar(dataset, options->input_depth_file, geotiff_reader::scalar_type::depth);
	auto res_velocity_u = read_scalar(dataset, options->input_velocity_u_file, geotiff_reader::scalar_type::velocity_u);
	auto res_velocity_v = read_scalar(dataset, options->input_velocity_v_file, geotiff_reader::scalar_type::velocity_v);

	auto ress = std::vector{res_dem.first,
	                        res_depth.first,
	                        res_velocity_u.first,
	                        res_velocity_v.first,
	                        res_dem.second.compare(res_depth.second),
	                        res_dem.second.compare(res_velocity_u.second),
	                        res_dem.second.compare(res_velocity_v.second)};
	// all must be true and geotiff headers match
	bool ret = std::all_of(ress.begin(), ress.end(), [](bool b) { return b; });

	ret &= compute_positions(dataset, res_dem.second);

	// set epsg before return
	epsg = static_cast<uint32_t>(res_dem.second.epsg);

	return ret;
}

std::pair<bool, geotiff_reader::geotiff_header>
geotiff_reader::read_scalar(std::shared_ptr<data::triangle_dataset> dataset, std::filesystem::path path,
                            scalar_type type)
{
	util::log::info(LOG_TAG, fmt::format("Reading scalar file: {0}", path.u8string()));

	std::string file = path.u8string();
	geotiff_reader::geotiff_header header;

	TIFF *tif = nullptr;
	GTIF *gtif = nullptr;

	tif = XTIFFOpen(file.c_str(), "r");
	if (!tif) {
		log::error(LOG_TAG, fmt::format("Could not open or parse TIFF: {}", file));
		return {false, header};
	}

	gtif = GTIFNew(tif);
	if (!gtif) {
		log::error(LOG_TAG, fmt::format("Could not open or parse Keys of GeoTIFF: {}", file));
		return {false, header};
	}

	enum gtiff_versions { VERSION = 0, MAJOR, MINOR };
	int versions[3];
	geocode_t model;

	GTIFDirectoryInfo(gtif, versions, 0);
	if (versions[gtiff_versions::MAJOR] > 1) {
		log::error(LOG_TAG, "GeoTIFF Version is newer than supported version.");
		return {false, header};
	}
	if (!GTIFKeyGet(gtif, GTModelTypeGeoKey, &model, 0, 1)) {
		log::error(LOG_TAG, "GeoTIFF has no model key.");
		return {false, header};
	}

	{
		int size = 0;
		tagtype_t tag_type;
		int cit_length = GTIFKeyInfo(gtif, GTCitationGeoKey, &size, &tag_type);
		if (cit_length > 0) {
			header.citation.resize(static_cast<size_t>(size) * cit_length);
			GTIFKeyGet(gtif, GTCitationGeoKey, static_cast<void *>(header.citation.data()), 0, cit_length);
		}
	}

	{
		int size = 0;
		tagtype_t tag_type;
		int epsg_length = GTIFKeyInfo(gtif, ProjectedCSTypeGeoKey, &size, &tag_type);
		if (epsg_length > 0) {
			GTIFKeyGet(gtif, ProjectedCSTypeGeoKey, static_cast<void *>(&header.epsg), 0, epsg_length);
		}
	}

	{
		uint16_t count;
		double *pixel_scale;
		auto pixel_scale_code = GTIFTagCode("ModelPixelScaleTag"); // 33550
		if (TIFFGetField(tif, pixel_scale_code, &count, &pixel_scale)) {
			for (int i = 0; i < count; ++i) header.pixel_scale[i] = pixel_scale[i];
		}
	}

	{
		uint16_t count;
		double *tie_points;
		auto tie = GTIFTagCode("ModelTiepointTag"); // 33922
		if (TIFFGetField(tif, tie, &count, &tie_points)) {
			for (int i = 0; i < count; ++i) header.tie_points[i] = tie_points[i];
		}
	}

	int width = 0, height = 0;
	TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
	TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);

	short format = 0, bits = 0;
	TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &format); // 3 = float
	TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bits);  // 32 = SP float

	if (format != 3) {
		log::error(LOG_TAG, "GeoTIFF does contain different from IEEE 754 float.");
		return {false, header};
	}
	if (bits != 32) {
		log::error(LOG_TAG, "GeoTIFF bitcount does not match 32 (IEEE 754 float).");
		return {false, header};
	}

	int tile_width = 0, tile_height = 0;
	bool has_tiles = static_cast<bool>(TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tile_width));
	if (has_tiles) TIFFGetField(tif, TIFFTAG_TILELENGTH, &tile_height);

	if (!has_tiles) {
		header.width = width;
		header.height = height;
	}
	else {
		header.width = IWD::util::ceil_to_multiple(width, tile_width);
		header.height = IWD::util::ceil_to_multiple(height, tile_height);
	}

	switch (type) {
	case scalar_type::dem: {
		auto hei = std::vector<data::components::height_t<float>>();

		if (has_tiles)
			geotiff_reader::read_tiled_geotiff(tif, hei);
		else
			geotiff_reader::read_scanline_geotiff(tif, hei);

		auto &position = dataset->emplace_or_replace_component<data::triangle_dataset::position_3D>({}, 0);
		position.resize(hei.size(), glm::dvec3(-std::numeric_limits<float>::max()));

		for (size_t t = 0; t < hei.size(); ++t) {
			position[t].y = hei[t].x;
		}

		break;
	}
	case scalar_type::depth: {
		auto &depth = dataset->emplace_or_replace_component<data::triangle_dataset::depth>({}, 0);

		if (has_tiles)
			geotiff_reader::read_tiled_geotiff(tif, depth);
		else
			geotiff_reader::read_scanline_geotiff(tif, depth);

		// important for removal of dry cells later on
		for (auto &d : depth) {
			if (d.x == -std::numeric_limits<float>::max()) d = glm::fvec1(0.0);
		}

		break;
	}
	case scalar_type::velocity_u: {
		auto vx = std::vector<data::components::velocity_t<1, float>>();

		if (has_tiles)
			geotiff_reader::read_tiled_geotiff(tif, vx);
		else
			geotiff_reader::read_scanline_geotiff(tif, vx);

		auto &velocity = dataset->has_component<data::triangle_dataset::velocity_2D>(0)
		                     ? dataset->get_component<data::triangle_dataset::velocity_2D>(0)
		                     : dataset->emplace_or_replace_component<data::triangle_dataset::velocity_2D>({}, 0);

		if (velocity.size() != vx.size()) // x was first
			velocity.resize(vx.size(), glm::fvec2(-std::numeric_limits<float>::max()));

		for (size_t t = 0; t < vx.size(); ++t) {
			velocity[t].x = vx[t].x;
		}

		break;
	}
	case scalar_type::velocity_v: {
		auto vy = std::vector<data::components::velocity_t<1, float>>();

		if (has_tiles)
			geotiff_reader::read_tiled_geotiff(tif, vy);
		else
			geotiff_reader::read_scanline_geotiff(tif, vy);

		auto &velocity = dataset->has_component<data::triangle_dataset::velocity_2D>(0)
		                     ? dataset->get_component<data::triangle_dataset::velocity_2D>(0)
		                     : dataset->emplace_or_replace_component<data::triangle_dataset::velocity_2D>({}, 0);

		if (velocity.size() != vy.size()) // y was first
			velocity.resize(vy.size(), glm::fvec2(-std::numeric_limits<float>::max()));

		for (size_t t = 0; t < vy.size(); ++t) {
			velocity[t].y = vy[t].x;
		}

		break;
	}
	}

	return {true, header};

} // namespace IWD::io

bool geotiff_reader::compute_positions(std::shared_ptr<data::triangle_dataset> dataset, const geotiff_header &header)
{

	if (!dataset->has_component<data::triangle_dataset::position_3D>(0)) {
		auto &pos = dataset->emplace_or_replace_component<data::triangle_dataset::position_3D>({}, 0);
		pos.resize(static_cast<size_t>(header.width) * header.height, glm::dvec3(-std::numeric_limits<float>::max()));
	}

	auto &positions = dataset->get_component<data::triangle_dataset::position_3D>(0);
	if (positions.size() != (static_cast<size_t>(header.width) * header.height)) {
		log::error(LOG_TAG, "Position size does not match width * height.");
		return false;
	}

	auto x_scale = header.pixel_scale[0];
	auto y_scale = header.pixel_scale[1];
	auto x_origin = header.tie_points[3]; // upper left
	auto y_origin = header.tie_points[4]; // upper left

	auto get_off = [&](uint32_t x, uint32_t y, uint32_t width) -> uint32_t { return y * width + x; };

	// https://en.wikipedia.org/wiki/World_file
	for (int y = 0; y < header.height; ++y) {
		for (int x = 0; x < header.width; ++x) {
			auto off = get_off(x, y, header.width);
			positions[off].x = x_origin + x_scale * x + (0.5 * x_scale); // goes east
			positions[off].z = y_origin - y_scale * y + (0.5 * y_scale); // goes south
		}
	}

	auto &indices = dataset->emplace_or_replace_component<data::components::common::triangle_index>({}, 0);
	indices.reserve(2 * ((static_cast<size_t>(header.width) - 1) * (static_cast<size_t>(header.height)) - 1));

	for (int y = 0; y < header.height - 1; ++y) {
		for (int x = 0; x < header.width - 1; ++x) {
			auto ul = get_off(x, y, header.width);
			auto dl = get_off(x, y + 1, header.width);
			auto ur = get_off(x + 1, y, header.width);
			auto dr = get_off(x + 1, y + 1, header.width);

			indices.push_back(glm::ivec3(ul, dl, dr));
			indices.push_back(glm::ivec3(dr, ur, ul));
		}
	}

	indices.shrink_to_fit();

	return true;
}

bool geotiff_reader::geotiff_header::compare(const geotiff_header &other)
{

	bool tie = true;
	for (int32_t i = 0; i < this->tie_points.size(); ++i) {
		tie &= this->tie_points[i] == other.tie_points[i];
	}
	if (!tie) log::error(LOG_TAG, "GeoTIFF tie points are not matching.");

	bool px = true;
	for (int32_t i = 0; i < this->pixel_scale.size(); ++i) {
		px &= this->pixel_scale[i] == other.pixel_scale[i];
	}
	if (!px) log::error(LOG_TAG, "GeoTIFF pixel scales are not matching.");

	bool eps = this->epsg == other.epsg;
	if (!eps) log::error(LOG_TAG, "GeoTIFF EPSGs are not matching.");

	bool cit = this->citation == other.citation;

	return tie && px && eps && cit;
}

} // namespace IWD::io

#endif