#include "io/3d_tiles_writer.h"

#include "proj.h"

#include "io/gltf_writer.h"

#include "util/logger.h"

using namespace IWD::data;
using namespace IWD::util;
using json = nlohmann::json;

const std::string LOG_TAG = "{3d_tiles_writer}";

namespace IWD::io {

std::vector<std::byte>
    _3d_tiles_writer::build_b3dm_header_binary(/*const data::components::position_t<3, double> &rtc*/)
{
	json jsf = build_b3dm_feature_header_json(/*rtc*/);
	std::string jfstr = jsf.dump();

	b3dm_header b3h;

	// https://github.com/CesiumGS/3d-tiles/tree/master/specification/TileFormats/Batched3DModel#padding
	// https://github.com/CesiumGS/3d-tiles/blob/master/specification/TileFormats/FeatureTable/README.md#padding
	// 1. json-part of feature table must end on 8 byte boundary within tile binary
	// 2. no feature table binary, no batch table -> thus ends with feature table json
	// 3. binary glTF must start on 8 byte boundary
	int padding = (sizeof(b3dm_header) + jfstr.size()) % 8;
	for (int i = 0; i < 8 - padding; ++i) jfstr += char(0x20);
	// already padded to 8, only check self
	// padding = jbstr.size() % 8;
	// for (int i = 0; i < 8 - padding; ++i) jbstr += char(0x20);

	b3h.feature_table_json_length = static_cast<uint32_t>(jfstr.size());
	b3h.feature_table_binary_length = 0u;
	b3h.batch_table_json_length = 0u /*static_cast<uint32_t>(jbstr.size())*/;
	b3h.batch_table_binary_length = 0u;
	size_t header_size = sizeof(b3dm_header) + jfstr.size() /* + jbstr.size()*/;

	b3h.length =
	    static_cast<uint32_t>(header_size); // does NOT contain glTF binary size yet, must add later (see gltf_writer)

	std::vector<std::byte> bytes(header_size);

	memcpy(bytes.data() + 0, &b3h.magic, 4 * sizeof(unsigned char));
	memcpy(bytes.data() + 4, &b3h.version, sizeof(uint32_t));
	memcpy(bytes.data() + 8, &b3h.length, sizeof(uint32_t));
	memcpy(bytes.data() + 12, &b3h.feature_table_json_length, sizeof(uint32_t));
	memcpy(bytes.data() + 16, &b3h.feature_table_binary_length, sizeof(uint32_t));
	memcpy(bytes.data() + 20, &b3h.batch_table_json_length, sizeof(uint32_t));
	memcpy(bytes.data() + 24, &b3h.batch_table_binary_length, sizeof(uint32_t));
	memcpy(bytes.data() + 28, jfstr.c_str(), jfstr.size());
	// memcpy(bytes.data() + 28 + jfstr.size(), jbstr.c_str(), jbstr.size());

	return bytes;
}

nlohmann::json _3d_tiles_writer::build_b3dm_feature_header_json(/*const data::components::position_t<3, double> &rtc*/)
{
	json js;
	js["BATCH_LENGTH"] = 0u /*1u*/;
	return js;
}

// nlohmann::json _3d_tiles_writer::build_b3dm_batch_header_json()
//{
//	json js;
//	js["id"] = {0};
//	return js;
//}

bool _3d_tiles_writer::write(const std::vector<tile_info> info, const data::components::rtc_info &rtc,
                             const std::filesystem::path &path)
{
	std::ofstream file;

	if (file.open(path); file.is_open()) {
		util::log::info(LOG_TAG, fmt::format("Writing output file: {0}", path.u8string()));

		auto js = build_3d_tiles_json(info, rtc);
		file << std::setw(3) << js;
	}
	else {
		log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", path.u8string()));
		return false;
	}
	return true;
}

nlohmann::json _3d_tiles_writer::build_3d_tiles_json(const std::vector<tile_info> info,
                                                     const data::components::rtc_info &rtc)
{
	const std::array<uint32_t, 3> geometric_error_hierarchy_mapping = {250, 100, 0};

	nlohmann::json js;

	components::common::bounding_box_3D bbox_all;
	for (const auto &i : info) {
		bbox_all.min = glm::min(bbox_all.min, static_cast<glm::dvec3>(i.bbox.min));
		bbox_all.max = glm::max(bbox_all.max, static_cast<glm::dvec3>(i.bbox.max));
	}

	components::common::bounding_box_2D bbox_all_lat_long;
	for (const auto &i : info) {
		bbox_all_lat_long.min = glm::min(bbox_all_lat_long.min, static_cast<glm::dvec2>(i.bbox_lat_long.min));
		bbox_all_lat_long.max = glm::max(bbox_all_lat_long.max, static_cast<glm::dvec2>(i.bbox_lat_long.max));
	}

	auto bbox_all_diff = bbox_all.max - bbox_all.min;
	// must be high to render. Set to largest dimension of dataset in meters
	auto highest_geom_error = static_cast<uint32_t>(std::max(std::abs(bbox_all_diff.x), std::abs(bbox_all_diff.z)));

	js["asset"] = {{"version", "1.0"}};
	js["geometricError"] = highest_geom_error;

	// put RTC in extras, so we can query the lightweight tileset.json later
	js["extras"] = {{"RTC_CENTER", {rtc.center_new.x, rtc.center_new.y, rtc.center_new.z}}};

	auto make_box = [&](const data::components::common::bounding_box_2D &bbox) -> nlohmann::json {
		nlohmann::json j;

		j["region"] = json::array({proj_torad(bbox.min.x + 0.0001), proj_torad(bbox.min.y + 0.0001),
		                           proj_torad(bbox.max.x - 0.0001), proj_torad(bbox.max.y - 0.0001),
		                           rtc.center_original.y - 25.0, rtc.center_original.y + 25.0});

		return j;
	};

	uint32_t max_lod = 0;
	{
		auto current = &info[0];
		while (current->higher_lod_tile) {
			++max_lod;
			current = current->higher_lod_tile.get();
		}
	}

	auto make_tile_json = [&](const tile_info &i) -> nlohmann::json {
		nlohmann::json j;

		auto box = make_box(i.bbox_lat_long);

		j["boundingVolume"] = box;
		// j["viewerRequestVolume"] = box; // if not defined will be the same as boundingVolume
		j["geometricError"] = geometric_error_hierarchy_mapping[std::clamp(
		    max_lod < 2u ? i.lod + (2u - max_lod) : i.lod, 0u, 2u)]; // matching geometric_error_hierarchy_mapping::size
		j["refine"] = "REPLACE";
		if (!i.uri.empty()) j["content"] = {{"uri", i.uri}};
		j["children"] = json::array();

		return j;
	};

	tile_info rti = {bbox_all, bbox_all_lat_long, "", 0 /*must be replaced with large value*/, 0, {}};

	js["root"] = make_tile_json(rti);
	js["root"]["geometricError"] = highest_geom_error;

	for (const auto &i : info) {
		auto current = &i;
		auto j = make_tile_json(i);
		auto children = &j["children"];

		while (current->higher_lod_tile) {
			children->push_back(make_tile_json(*current->higher_lod_tile));
			children = &((*children)[0]["children"]);
			current = current->higher_lod_tile.get();
		}

		js["root"]["children"].push_back(j);
	}

	return js;
}
} // namespace IWD::io