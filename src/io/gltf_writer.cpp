#include "io/gltf_writer.h"

#include <iomanip>

#include "khronos/webgl_definitions.h"

#include "base64/base64.h"
#include "util/logger.h"

#include "definitions.h"

using namespace IWD::data;
using namespace IWD::util;
using json = nlohmann::json;

const std::string LOG_TAG = "{gltf_writer}";

namespace IWD::io {

bool gltf_writer::write(std::shared_ptr<dataset> dataset, std::shared_ptr<const job_processing_options> options,
                        const processing_params &params, bool as_b3dm)
{
	if (options->gltf_embed && options->gltf_glb) {
		log::error(LOG_TAG, "Settings 'gltf_embed' and 'gltf_glb' are mutually exclusive. Use only one at a time.");
		return false;
	}

	if (!dataset->has_component<components::common::linearized_binary>(0)) {
		log::error(LOG_TAG, "Writing requires linearization of binary data beforehand");
		return false;
	}
	// IMPROVE: check all required types with entt type-list

	// build json
	auto js = build_json(dataset, options, params);

	// write binary .glb
	// https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#glb-file-format-specification
	if (options->gltf_glb || as_b3dm) {
		std::fstream file;

		auto of_name = options->output_directory / (options->output_basename.u8string() + (as_b3dm ? ".b3dm" : ".glb"));

		auto open_mode = as_b3dm ? (std::ios::binary | std::ios::ate | std::ios::in | std::ios::out)
		                         : std::ios::binary | std::ios::out;

		if (file.open(of_name, open_mode); file.is_open()) {
			util::log::info(LOG_TAG, fmt::format("Writing output file: {0}", of_name.u8string()));

			std::string jstr = js.dump();

			// align json size to 4 bytes
			int padding = jstr.size() % 4 == 0 ? 0 : 4 - jstr.size() % 4;
			for (int i = 0; i < padding; ++i) jstr += char(0x20);

			// NO ADRESS, MAKE COPY! (don't want to alter raw binary)
			auto /*&*/ lin_buf = dataset->get_component<components::common::linearized_binary>(0);

			// align binary size to 4 bytes
			padding = jstr.size() % 4 == 0 ? 0 : 4 - jstr.size() % 4;
			if (padding != 0) lin_buf.reserve(lin_buf.size() + padding);
			for (int i = 0; i < padding; ++i) lin_buf.push_back(std::byte(0x00));

			glb_header header;
			// header + json chunk + bin chunk
			header.length = sizeof(glb_header) + (sizeof(glb_chunk_header) + static_cast<uint32_t>(jstr.size())) +
			                (sizeof(glb_chunk_header) + static_cast<uint32_t>(lin_buf.size()));

			auto json_header = glb_chunk_header{static_cast<uint32_t>(jstr.size()), glb_chunk_header::chunk_type::json};
			auto bin_header =
			    glb_chunk_header{static_cast<uint32_t>(lin_buf.size()), glb_chunk_header::chunk_type::bin};

			// write single values, no hassle with struct padding
			file.write(reinterpret_cast<const char *>(header.magic), 4 * sizeof(unsigned char));
			file.write(reinterpret_cast<const char *>(&header.version), sizeof(uint32_t));
			file.write(reinterpret_cast<const char *>(&header.length), sizeof(uint32_t));

			file.write(reinterpret_cast<const char *>(&json_header.length), sizeof(uint32_t));
			file.write(reinterpret_cast<const char *>(&json_header.type), sizeof(uint32_t));
			file.write(jstr.c_str(), jstr.size());

			file.write(reinterpret_cast<const char *>(&bin_header.length), sizeof(uint32_t));
			file.write(reinterpret_cast<const char *>(&bin_header.type), sizeof(uint32_t));
			file.write(reinterpret_cast<const char *>(lin_buf.data()), lin_buf.size());

			// update b3dm length in binary
			if (as_b3dm) {
				auto file_size = file.tellg();

				padding = file_size % 8 == 0 ? 0 : 8 - file_size % 8;

				if (padding != 0) {
					auto pad_char = std::byte(0x00);
					for (int i = 0; i < padding; ++i)
						file.write(reinterpret_cast<const char *>(&pad_char), sizeof(std::byte));
				}

				uint32_t b3dm_header_length = 0;
				file.seekg(4 * sizeof(unsigned char) + sizeof(uint32_t), file.beg);
				file.read(reinterpret_cast<char *>(&b3dm_header_length), sizeof(uint32_t));

				uint32_t total_length = b3dm_header_length + header.length + padding;
				file.seekp(4 * sizeof(unsigned char) + sizeof(uint32_t), file.beg);
				file.write(reinterpret_cast<const char *>(&total_length), sizeof(uint32_t));
			}
		}
		else {
			log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", of_name.u8string()));
			return false;
		}
	}

	// write embedded .gltf with base64 encoded data
	else if (options->gltf_embed) {
		std::ofstream file;

		auto of_name = options->output_directory / (options->output_basename.u8string() + ".gltf");

		if (file.open(of_name); file.is_open()) {
			util::log::info(LOG_TAG, fmt::format("Writing output file: {0}", of_name.u8string()));
			file << std::setw(3) << js;
		}
		else {
			log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", of_name.u8string()));
			return false;
		}
	}

	// write .gltf with external .bin buffer
	else {
		std::ofstream file;

		auto of_name = options->output_directory / (options->output_basename.u8string() + ".gltf");

		if (file.open(of_name); file.is_open()) {
			util::log::info(LOG_TAG, fmt::format("Writing output file: {0}", of_name.u8string()));
			file << std::setw(3) << js;
			file.close();
		}
		else {
			log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", of_name.u8string()));
			return false;
		}

		auto bf_name = options->output_directory / (options->output_basename.u8string() + ".bin");
		if (file.open(bf_name, std::ios::binary); file.is_open()) {
			util::log::info(LOG_TAG, fmt::format("Writing output file: {0}", bf_name.u8string()));
			const auto &lin_buf = dataset->get_component<components::common::linearized_binary>(0);
			file.write(reinterpret_cast<const char *>(lin_buf.data()), lin_buf.size());
		}
		else {
			log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", bf_name.u8string()));
			return false;
		}
	}

	return true;
}

// example for shader extension: https://github.com/hiloteam/Hilo3d/tree/master/examples/models/SampleTechniques
// https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_techniques_webgl
nlohmann::json gltf_writer::build_json(std::shared_ptr<dataset> dataset,
                                       std::shared_ptr<const job_processing_options> options,
                                       const processing_params &params)
{
	json js;

	// ASSET
	{
		js["asset"] = {{"generator", std::string("lib-iwd ") + version::to_string()}, {"version", "2.0"}};

		// generate timestamp
		auto t = std::time(nullptr);
#pragma warning(suppress : 4996) // supress _CRT_SECURE_NO_WARNINGS once
		auto tm = *std::localtime(&t);
		std::ostringstream oss;
		oss << std::put_time(&tm, "%d-%m-%Y %H-%M-%S");
		js["asset"]["extras"] = {{"timestamp", oss.str()}};
	}

	// ACCESSORS
	{
		js["accessors"] = json::array();

		const auto &bbox = params.bbox;

		js["accessors"].push_back({{"name", "position"},
		                           {"bufferView", 0},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::FLOAT},
		                           {"count", dataset->vertex_count()},
		                           {"type", "VEC3"},
		                           {"min", {bbox.min.x, bbox.min.y, bbox.min.z}},
		                           {"max", {bbox.max.x, bbox.max.y, bbox.max.z}}});

		js["accessors"].push_back({{"name", "position_original"},
		                           {"bufferView", 1},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::FLOAT},
		                           {"count", dataset->vertex_count()},
		                           {"type", "VEC2"},
		                           {"min", {params.bbox_original.min.x, params.bbox_original.min.y}},
		                           {"max", {params.bbox_original.max.x, params.bbox_original.max.y}}});

		js["accessors"].push_back({{"name", "depth"},
		                           {"bufferView", 2},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::FLOAT},
		                           {"count", dataset->vertex_count()},
		                           {"type", "SCALAR"},
		                           {"min", {params.depth_min.x}},
		                           {"max", {params.depth_max.x}}});

		js["accessors"].push_back({{"name", "velocity"},
		                           {"bufferView", 3},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::FLOAT},
		                           {"count", dataset->vertex_count()},
		                           {"type", "VEC2"},
		                           {"min", {params.velocity_min.x, params.velocity_min.y}},
		                           {"max", {params.velocity_max.x, params.velocity_max.y}}});

		/*js["accessors"].push_back({{"name", "batchid"},
		                           {"bufferView", 4},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::FLOAT},
		                           {"count", dataset->vertex_count()},
		                           {"type", "SCALAR"},
		                           {"min", {0}},
		                           {"max", {0}}});*/

		js["accessors"].push_back({{"name", "index"},
		                           {"bufferView", 4},
		                           {"byteOffset", 0},
		                           {"componentType", webgl::UNSIGNED_INT},
		                           {"count", dataset->get_component<components::common::triangle_index>(0).size() * 3},
		                           {"type", "SCALAR"},
		                           {"min", {0}},
		                           {"max", {dataset->vertex_count() - 1}}});
	}

	// BUFFERS
	{
		const auto &lin_buf = dataset->get_component<components::common::linearized_binary>(0);

		if (options->gltf_glb) {
			// glb buffer must be first in array (buffers[0]) and must not have an "uri"
			js["buffers"] = json::array({{{"byteLength", lin_buf.size()}}});
		}
		else if (options->gltf_embed) {
			std::string data64 = base64::base64_encode(reinterpret_cast<const unsigned char *>(lin_buf.data()),
			                                           static_cast<unsigned int>(lin_buf.size()));

			js["buffers"] = {
			    {{"byteLength", lin_buf.size()}, {"uri", "data:application/octet-stream;base64," + data64}}};
		}
		else {
			js["buffers"] =
			    json::array({{{"byteLength", lin_buf.size()}, {"uri", options->output_basename.u8string() + ".bin"}}});
		}

		js["bufferViews"] = json::array();

		const auto &offsets = dataset->get_component<data::components::common::linearized_binary_offsets>(0);
		for (const auto &off : offsets) {
			js["bufferViews"].push_back({{"buffer", 0},
			                             {"byteLength", off.first},
			                             {"byteOffset", off.second},
			                             //{"byteStride", 0},
			                             {"target", webgl::ARRAY_BUFFER}});
		}

		js["bufferViews"].push_back(
		    {{"buffer", 0},
		     {"byteLength", dataset->index_count() * sizeof(data::components::index_t<3>)},
		     {"byteOffset", lin_buf.size() - dataset->index_count() * sizeof(data::components::index_t<3>)},
		     {"target", webgl::ELEMENT_ARRAY_BUFFER}});
	}
	// MESHES
	{
		js["meshes"] = json::array();
		js["meshes"].push_back({{"primitives",
		                         {{{"attributes",
		                            {
		                                {"POSITION", 0}, {"_POSITION_ORIGINAL", 1}, {"_DEPTH", 2}, {"_VELOCITY", 3},
		                                /*{"_BATCHID", 4},*/
		                            }},
		                           {"indices", 4 /*5*/},
		                           {"material", 0},
		                           {"mode", 4}}}}});

		// MATERIALS
		{
			js["materials"] =
			    json::array({{{"name", "water"},
			                  {"alphaMode", "BLEND"},
			                  {"doubleSided", true},
			                  {"extensions",
			                   {{"KHR_techniques_webgl",
			                     {{"technique", 0},
			                      {"values",
			                       {
			                           {"u_ambient", {0.184, 0.152, 0.113, 1}},
			                           {"u_diffuse", {0.333, 0.258, 0.168, 1}},
			                           {"u_emission", {0.1, 0.1, 0.1, 1}},
			                           {"u_specular", {0.5, 0.5, 0.5, 1}},
			                           {"u_shininess", {128.0}},
			                           {"u_transparency", {1.0}},
			                           {"u_modelRotationMatrix", {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}},
			                       }}}}}}}});
		}

		// IMAGES
		{
			// IMPROVE: check cubemaps
			// https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Vendor/EXT_lights_image_based
			// https://github.com/CesiumGS/cesium/issues/7051
			// https://github.com/CesiumGS/cesium/pull/7172
			js["images"] = json::array({{{"uri", "cold_hot.png"}},
			                            {{"uri", "noise.jpg"}},
			                            {{"uri", "front.jpg"}},
			                            {{"uri", "back.jpg"}},
			                            {{"uri", "left.jpg"}},
			                            {{"uri", "right.jpg"}},
			                            {{"uri", "up.jpg"}},
			                            {{"uri", "down.jpg"}},
			                            {{"uri", "wave.jpg"}},
			                            {{"uri", "wave2_inv.jpg"}}});
		}

		// TEXTURES & SAMPLERS
		{
			js["textures"] = json::array();

			js["textures"].push_back({{"sampler", 0}, {"source", 0}}); // velocity_lut
			js["textures"].push_back({{"sampler", 1}, {"source", 1}}); // noise
			js["textures"].push_back({{"sampler", 1}, {"source", 2}}); // cube front
			js["textures"].push_back({{"sampler", 1}, {"source", 3}}); // cube back
			js["textures"].push_back({{"sampler", 1}, {"source", 4}}); // cube left
			js["textures"].push_back({{"sampler", 1}, {"source", 5}}); // cube right
			js["textures"].push_back({{"sampler", 1}, {"source", 6}}); // cube up
			js["textures"].push_back({{"sampler", 1}, {"source", 7}}); // cube down
			js["textures"].push_back({{"sampler", 1}, {"source", 8}}); // wave 1
			js["textures"].push_back({{"sampler", 1}, {"source", 9}}); // wave 2

			js["samplers"] = json::array({{
			                                  {"magFilter", webgl::LINEAR},
			                                  {"minFilter", webgl::LINEAR_MIPMAP_LINEAR},
			                                  {"wrapS", webgl::CLAMP_TO_EDGE},
			                                  {"wrapT", webgl::CLAMP_TO_EDGE},
			                              },
			                              {
			                                  {"magFilter", webgl::LINEAR},
			                                  {"minFilter", webgl::LINEAR_MIPMAP_LINEAR},
			                                  {"wrapS", webgl::REPEAT},
			                                  {"wrapT", webgl::REPEAT},
			                              }});
		}

		// SCENES & NODES
		{
			js["nodes"] = {{{"name", "rootNode"},
			                {"mesh", 0},
			                {"matrix", params.is_3d_tile ? /**/
			                               json::array({1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1})
			                                             : /**/
			                               json::array({0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1})}}};
			js["scenes"] = {{{"name", "rootScene"}, {"nodes", {0}}}};
			js["scene"] = 0;
		}

		// EXTENSIONS
		{
			js["extensionsUsed"] = {"KHR_techniques_webgl"};
			js["extensionsRequired"] = {"KHR_techniques_webgl"};

			json teq = json::array();
			teq.push_back(
			    {{"program", 0},
			     {"attributes",
			      {{"a_depth", {{"semantic", "_DEPTH"}}},
			       {"a_position", {{"semantic", "POSITION"}}},
			       {"a_velocity", {{"semantic", "_VELOCITY"}}},
			       /*{"a_batchId", {{"semantic", "_BATCHID"}}},*/
			       {"a_position_original", {{"semantic", "_POSITION_ORIGINAL"}}}}},
			     {"uniforms",
			      {{"u_modelViewMatrix",
			        {{"semantic",
			          dataset->has_component<components::rtc_info>(0) ? "CESIUM_RTC_MODELVIEW" : "MODELVIEW"},
			         {"type", webgl::FLOAT_MAT4}}},
			       {"u_projectionMatrix", {{"semantic", "PROJECTION"}, {"type", webgl::FLOAT_MAT4}}},
			       {"u_normalMatrix", {{"semantic", "MODELVIEWINVERSETRANSPOSE"}, {"type", webgl::FLOAT_MAT3}}},
			       {"u_viewInverseMatrix", {{"semantic", "VIEWINVERSE"}, {"type", webgl::FLOAT_MAT4}}},
			       {"u_modelRotationMatrix", {{"type", webgl::FLOAT_MAT4}}},
			       {"u_ambient", {{"type", webgl::FLOAT_VEC4}}},
			       {"u_diffuse",
			        {{"semantic", "_3DTILESDIFFUSE"}, // Cesium Sun as Light Source
			         {"type", webgl::FLOAT_VEC4}}},
			       {"u_emission", {{"type", webgl::FLOAT_VEC4}}},
			       {"u_specular", {{"type", webgl::FLOAT_VEC4}}},
			       {"u_shininess", {{"type", webgl::FLOAT}}},
			       {"u_transparency", {{"type", webgl::FLOAT}}},
			       {"u_velocity_min",
			        {{"type", webgl::FLOAT_VEC2}, {"value", {params.velocity_min.x, params.velocity_min.y}}}},
			       {"u_velocity_max",
			        {{"type", webgl::FLOAT_VEC2}, {"value", {params.velocity_max.x, params.velocity_max.y}}}},
			       {"u_velocity_as_color", {{"type", webgl::BOOL}, {"value", {static_cast<int>(false)}}}},
			       {"u_depth_as_color", {{"type", webgl::BOOL}, {"value", {static_cast<int>(false)}}}},
			       {"u_hazard_as_color", {{"type", webgl::BOOL}, {"value", {static_cast<int>(false)}}}},
			       {"u_velocity_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 0}}}}},
			       {"u_noise_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 1}}}}},
			       {"u_cube_front_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 2}}}}},
			       {"u_cube_back_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 3}}}}},
			       {"u_cube_left_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 4}}}}},
			       {"u_cube_right_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 5}}}}},
			       {"u_cube_up_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 6}}}}},
			       {"u_cube_down_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 7}}}}},
			       {"u_wave_1_normal_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 8}}}}},
			       {"u_wave_2_normal_sampler", {{"type", webgl::SAMPLER_2D}, {"value", {{"index", 9}}}}},
			       {"u_depth_min", {{"type", webgl::FLOAT}, {"value", {params.depth_min.x}}}},
			       {"u_depth_max", {{"type", webgl::FLOAT}, {"value", {params.depth_max.x}}}},
			       {"u_wave_scale", {{"type", webgl::FLOAT}, {"value", {0.01}}}},
			       {"u_velocity_scale", {{"type", webgl::FLOAT}, {"value", {1.0}}}},
			       {"u_lambert_beer", {{"type", webgl::FLOAT}, {"value", {0.8}}}},
			       {"u_cycle_time", {{"type", webgl::FLOAT}, {"value", {4.0}}}},
			       {"u_phase_time", {{"type", webgl::FLOAT}, {"value", {0.0}}}},
			       {"u_person_height", {{"type", webgl::FLOAT}, {"value", {1.7}}}},
			       {"u_person_weight", {{"type", webgl::FLOAT}, {"value", {70.0}}}}}}});

			js["extensions"]["KHR_techniques_webgl"] = {{"programs", {{{"fragmentShader", 0}, {"vertexShader", 1}}}},
			                                            {"shaders",
			                                             {{{"type", webgl::FRAGMENT_SHADER}, {"uri", "water.fs"}},
			                                              {{"type", webgl::VERTEX_SHADER}, {"uri", "water.vs"}}}},
			                                            {"techniques", teq}};

			if (dataset->has_component<components::rtc_info>(0)) {
				js["extensionsUsed"] = {"CESIUM_RTC", "KHR_techniques_webgl"};
				js["extensionsRequired"] = {"CESIUM_RTC", "KHR_techniques_webgl"};

				auto rtc_info = dataset->get_component<components::rtc_info>(0);

				js["extensions"]["CESIUM_RTC"] = {
				    {"center", {rtc_info.center_new.x, rtc_info.center_new.y, rtc_info.center_new.z}},
				    {"center_original",
				     {rtc_info.center_original.x, rtc_info.center_original.y, rtc_info.center_original.z}}};
			}
		}

		return js;
	}
}

} // namespace IWD::io