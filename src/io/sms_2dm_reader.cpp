#include "io/sms_2dm_reader.h"

#include "util/io.h"

using namespace IWD::util;

const std::string LOG_TAG = "{sms_2dm_reader}";

namespace IWD::io {

bool sms_2dm_reader::read(std::shared_ptr<data::triangle_dataset> dataset,
                          std::shared_ptr<const data::sms_2dm_processing_options> options)
{
	bool ret = true;
	ret &= read_mesh(dataset, options);
	ret &= read_scalar(dataset, options->input_depth_file, sms_2dm_reader::scalar_type::depth);
	ret &= read_scalar(dataset, options->input_waterlevel_file, sms_2dm_reader::scalar_type::height);
	ret &= read_scalar(dataset, options->input_velocity_file, sms_2dm_reader::scalar_type::velocity);
	return ret;
}

bool sms_2dm_reader::read_mesh(std::shared_ptr<data::triangle_dataset> dataset,
                               std::shared_ptr<const data::sms_2dm_processing_options> options)
{
	// auto-close RAII
	std::ifstream file;

	if (file.open(options->input_mesh_file); file.is_open()) {
		util::log::info(LOG_TAG, fmt::format("Reading mesh file: {0}", options->input_mesh_file.u8string()));

		// setup initial dataset components, construct empty vectors
		auto &positions = dataset->emplace_or_replace_component<data::triangle_dataset::position_3D>({}, 0);
		auto &indices = dataset->emplace_or_replace_component<data::components::common::triangle_index>({}, 0);

		std::string line;
		size_t line_num = 1;

		// skip first line "MESH2D"
		std::getline(file, line);

		while (std::getline(file, line)) {
			++line_num;

			// line = util::trim(line);
			util::remove_inner_whitespace(line);

			auto tokens = util::split_string(line, ' ', 7);

			try {
				if (tokens[0] == "E3T") {
					auto i = glm::ivec3{std::stoi(tokens[2]) - 1, std::stoi(tokens[3]) - 1, std::stoi(tokens[4]) - 1};
					indices.push_back(i);
				}
				else if (tokens[0] == "E4Q") {
					auto i = glm::ivec3{std::stoi(tokens[2]) - 1, std::stoi(tokens[3]) - 1, std::stoi(tokens[4]) - 1};
					indices.push_back(i);

					auto i2 = glm::ivec3{std::stoi(tokens[4]) - 1, std::stoi(tokens[5]) - 1, std::stoi(tokens[2]) - 1};
					indices.push_back(i2);
				}
				else if (tokens[0] == "ND") {
					// contains DEM y, not water level
					// switch y and z axis
					auto p = glm::dvec3{std::stod(tokens[2]), std::stod(tokens[4]), std::stod(tokens[3])};
					positions.push_back(p);
				}
				// there are many more valid tokens which are not required and thus ignored
			}
			// stoi or stod may parse invalid text
			catch (std::invalid_argument) {
				log::error(LOG_TAG, fmt::format("Encountered invalid number in line {0}.", line_num));
				return false;
			}
			catch (std::out_of_range) {
				log::error(LOG_TAG, fmt::format("Encountered out of range number in line {0}.", line_num));
				return false;
			}
		}

		positions.shrink_to_fit();
		indices.shrink_to_fit();
	}
	else {
		log::error(LOG_TAG, fmt::format("Couldn't open file: {0}", options->input_mesh_file.u8string()));
		return false;
	}

	return true;
}

bool sms_2dm_reader::read_scalar(std::shared_ptr<data::triangle_dataset> dataset, std::filesystem::path path,
                                 scalar_type type)
{
	if (!dataset->has_component<data::triangle_dataset::position_3D>(0)) {
		log::error(LOG_TAG, fmt::format("Trying to read scalar before mesh."));
		return false;
	}

	// auto-close RAII
	std::ifstream file;

	if (file.open(path); file.is_open()) {
		util::log::info(LOG_TAG, fmt::format("Reading scalar file: {0}", path.u8string()));

		// preparing common state
		std::string line;
		size_t line_num = 0;
		int32_t current_timestep = -1;
		auto num_vertices = dataset->get_component<data::triangle_dataset::position_3D>(0).size();
		bool reading_data_block = false;

		try {
			while (!file.eof()) {

				// read generic header
				if (!reading_data_block) {
					std::getline(file, line);
					++line_num;

					if (line.find("SCALAR") != std::string::npos || line.find("VECTOR") != std::string::npos) {
						continue;
					}

					if (line.find("ND") != std::string::npos) {
						util::trim(line);
						util::remove_inner_whitespace(line);
						auto tokens = util::split_string(line, ' ', 2);
						auto num_nodes = std::stoi(tokens[1]);
						if (num_nodes != num_vertices) {
							log::error(LOG_TAG, fmt::format("Number of mesh nodes and scalars do not match."));
							return false;
						}
						continue;
					}

					if (line.find("TS") != std::string::npos) {
						++current_timestep;

						util::trim(line);
						util::remove_inner_whitespace(line);
						auto tokens = util::split_string(line, ' ', 2);
						auto time = std::stod(tokens.size() == 3 ? tokens[2] : tokens[1]);

						if (current_timestep == 0)
							dataset->update_timestep(0, time);
						else
							dataset->append_timestep(time);

						// header end
						reading_data_block = true;
					}
				}

				// read values
				else {
					// assumes file read position after "TS"

					switch (type) {
					case scalar_type::depth: {
						auto &depths =
						    dataset->emplace_or_replace_component<data::triangle_dataset::depth>({}, current_timestep);
						depths.reserve(num_vertices);

						// read N lines according to vertex count
						for (size_t i = 0; i < num_vertices; ++i) {
							std::getline(file, line);
							++line_num;

							util::trim(line);
							auto d = glm::tvec1<data::triangle_dataset::depth::value_type::value_type>(std::stof(line));
							depths.push_back(d);
						}
						// data end
						reading_data_block = false;
						break;
					}
					case scalar_type::height: {
						auto &height =
						    dataset->emplace_or_replace_component<data::triangle_dataset::height>({}, current_timestep);
						height.reserve(num_vertices);

						for (size_t i = 0; i < num_vertices; ++i) {
							std::getline(file, line);
							++line_num;

							util::trim(line);
							auto h =
							    glm::tvec1<data::triangle_dataset::height::value_type::value_type>(std::stof(line));
							height.push_back(h);
						}
						reading_data_block = false;
						break;
					}
					case scalar_type::velocity: {
						auto &velocity = dataset->emplace_or_replace_component<data::triangle_dataset::velocity_2D>(
						    {}, current_timestep);
						velocity.reserve(num_vertices);

						for (size_t i = 0; i < num_vertices; ++i) {
							std::getline(file, line);
							++line_num;

							util::trim(line);
							util::remove_inner_whitespace(line);
							auto tokens = util::split_string(line, ' ', 2);
							auto v = glm::tvec2<data::triangle_dataset::velocity_2D::value_type::value_type>(
							    std::stof(tokens[0]), std::stof(tokens[1]));
							velocity.push_back(v);
						}
						reading_data_block = false;
						break;
					}
					default: {
						log::error(LOG_TAG, fmt::format("Invalid scalar type given."));
						return false;
					}
					}
				}
			}
		}
		catch (std::invalid_argument) {
			log::error(LOG_TAG, fmt::format("Encountered invalid value in line {0}.", line_num));
			return false;
		}
		catch (std::out_of_range) {
			log::error(LOG_TAG, fmt::format("Encountered out of range value in line {0}.", line_num));
			return false;
		}
	}

	else {
		log::error(LOG_TAG, fmt::format("Couldn't open file: {0}", path.u8string()));
		return false;
	}

	return true;
}

} // namespace IWD::io