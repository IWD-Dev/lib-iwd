#include "io/time_parser.h"

#include "util/io.h"
#include "util/algorithm.h"
#include "util/logger.h"

using namespace IWD::util;

const std::string LOG_TAG = "{time_parser}";

namespace IWD::io {

std::vector<size_t> time_parser::parse(const std::string &str, const time_graph &graph)
{
	auto tokens = util::split_string(str, ',');
	std::set<size_t> timesteps;

	try {

		for (auto &token : tokens) {
			bool raw_time = token.find("T") != std::string::npos;
			util::trim(token);

			// range
			if (token.find("-") != std::string::npos) {
				size_t timestep_begin = 0, timestep_end = 0;
				auto bounds = util::split_string(token, '-', 2);


				if (raw_time) {
					if (auto f = bounds[0].find("T"); f != std::string::npos)
						bounds[0].erase(f, 1);
					if (auto f = bounds[1].find("T"); f != std::string::npos)
						bounds[1].erase(f, 1);
					timestep_begin = find_closest_timestep(graph, std::stod(bounds[0]));
					timestep_end = find_closest_timestep(graph, std::stod(bounds[1]));
				}
				else {
					timestep_begin = std::stoull(bounds[0]);
					timestep_end = std::stoull(bounds[1]);
				}

				for (size_t t = timestep_begin; t <= timestep_end; ++t) timesteps.insert(t);
			}
			// positive range
			else if (token.find("+") != std::string::npos) {
				size_t timestep = 0;
				token.erase(token.find("+"), 1);
				if (raw_time) {
					token.erase(token.find("T"), 1);
					timestep = find_closest_timestep(graph, std::stod(token));
				}
				else {
					timestep = std::stoull(token);
				}

				for (size_t t = timestep; t < graph.size(); ++t) timesteps.insert(t);
			}
			else if (token.find("start") != std::string::npos) {
				timesteps.insert(0ull);
			}
			else if (token.find("end") != std::string::npos) {
				timesteps.insert(graph.back().first);
			}
			else if (token.find("all") != std::string::npos) {
				for (size_t t = 0; t < graph.size(); ++t) timesteps.insert(t);
			}
			else if (token.find("last_") != std::string::npos) {
				token.erase(token.find("last_"), 1);
				auto n = std::stoull(token);

				for (size_t t = std::max(graph.size() - n, 0ull); t < graph.size(); ++t) timesteps.insert(t);
			}
			// regular single number
			else {
				if (raw_time) {
					token.erase(token.find("T"), 1);
					auto timestep = find_closest_timestep(graph, std::stod(token));
					timesteps.insert(timestep);
				}
				else  {
					auto timestep = std::stoull(token);
					timesteps.insert(std::min(static_cast<size_t>(timestep), graph.back().first));
				}	
			}
		}
	}
	catch (const std::invalid_argument& e) {
		log::error(LOG_TAG, fmt::format("Error in parsing timegraph: {}", str));
		log::error(LOG_TAG, e.what());
	}
	catch (const std::out_of_range &e) {
		log::error(LOG_TAG, fmt::format("Error in parsing timegraph: {}", str));
		log::error(LOG_TAG, e.what());
	}

	std::vector<size_t> ret;
	std::copy(timesteps.begin(), timesteps.end(), std::back_inserter(ret));
	return ret;
}

size_t time_parser::find_closest_timestep(const time_graph &graph, double time)
{
	for (size_t t = 0; t < graph.size() - 1; ++t) {
		auto t1 = graph[t].second;
		auto t2 = graph[t + 1].second;
		if (t1 <= time && t2 > time) {
			return (time - t1 < t2 - time) ? t : t + 1;
		}
	}

	return graph.back().first;
}

} // namespace IWD::io