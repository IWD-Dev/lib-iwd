#include "io/mike21_reader.h"

#include "quadtree/Quadtree.h"

#include "util/io.h"
#include "util/algorithm.h"

using namespace IWD::util;

const std::string LOG_TAG = "{mike21_reader}";

/// <summary>
/// Specialized exception for printing line numbers and line string.
/// </summary>
struct read_exception : public std::exception {
	const char *msg;
	std::string line;
	size_t line_num;

	read_exception(const char *msg, const std::string &line, size_t line_num)
	    : line(line), line_num(line_num){};

	const char *what() const noexcept { return fmt::format("Line {0}: {1} \n {2}", line_num, line, msg).c_str(); }
};

namespace IWD::io {

bool mike21_reader::read(std::shared_ptr<data::triangle_dataset> dataset,
                         std::shared_ptr<const data::mike21_processing_options> options)
{
	try {

		bool ret = true;

		auto &scatter = dataset->emplace_or_replace_component<std::vector<scatter_point>>({}, 0);
		ret &= read_mesh(dataset, options);
		ret &= read_scalar(dataset, options->input_depth_file, mike21_reader::scalar_type::depth);
		ret &= read_scalar(dataset, options->input_velocity_u_file, mike21_reader::scalar_type::velocity_u);
		ret &= read_scalar(dataset, options->input_velocity_v_file, mike21_reader::scalar_type::velocity_v);

		// TODO: may need to do this for all timesteps later on, currently only single timestep MIKE21 Exports
		if (!scatter.empty()) {
			ret &= interpolate_scattered_data(dataset);
		}
		else {
			log::error(LOG_TAG, "Scatter data is empty.");
			return false;
		}
			
		quad_to_triangles(dataset);

		// clear memory
		dataset->clear_component<std::vector<scatter_point>>(0);

		return ret;
	}
	catch (read_exception ex) {
		log::error(LOG_TAG, ex.what());
		return false;
	}
}

bool mike21_reader::read_mesh(std::shared_ptr<data::triangle_dataset> dataset,
                              std::shared_ptr<const data::mike21_processing_options> options)
{
	// auto-close RAII
	std::ifstream file;

	if (file.open(options->input_mesh_file); file.is_open()) {
		util::log::info(LOG_TAG, fmt::format("Reading mesh file: {0}", options->input_mesh_file.u8string()));

		// setup initial dataset components, construct empty vectors
		auto &positions = dataset->emplace_or_replace_component<data::triangle_dataset::position_3D>({}, 0);
		auto &indices_tri = dataset->emplace_or_replace_component<data::components::common::triangle_index>({}, 0);
		auto &indices_quad = dataset->emplace_or_replace_component<data::components::common::quad_index>({}, 0);

		/*
		[0] internal type
		[1] unit of internal type
		[2] # nodes
		[3] # projection info
		*/
		std::string vertex_header;
		std::getline(file, vertex_header);
		auto header_tokens = util::split_string(util::remove_inner_whitespace(vertex_header), ' ', 4);

		size_t num_vertices = 0;
		try {
			num_vertices = std::stoul(header_tokens[2]);
		}
		catch (const std::exception &ex) {
			throw read_exception(ex.what(), vertex_header, 1);
		}

		std::string line;
		size_t line_num = 2;

		try {
			for (size_t i = 0; i < num_vertices; ++i, ++line_num) {
				std::getline(file, line);
				auto tokens = util::split_string(line, ' ', 5);

				auto v = glm::dvec3(std::stod(tokens[1]), std::stod(tokens[3]), std::stod(tokens[2]));
				positions.push_back(v);

				// IMPROVE: maybe store cell material later on
				// materials.push_back(std::stoul(tokens[4]));
			}
		}
		catch (const std::exception &ex) {
			throw read_exception(ex.what(), line, line_num);
		}

		/*
		[0] # elements
		[1] max node per element: 3 or 4
		[2] internal type: 21 only tri, 25 with quads
		*/
		std::string element_header;
		std::getline(file, element_header);
		auto element_header_tokens = util::split_string(element_header, ' ', 3);

		bool has_quads = false;
		size_t num_elements = 0;

		try {
			has_quads = std::stoi(element_header_tokens[1]) == 4;
			num_elements = std::stoul(element_header_tokens[0]);

			if (dataset->has_component<std::vector<scatter_point>>(0)) {
				dataset->get_component<std::vector<scatter_point>>(0).resize(num_elements);
			}
			else {
				log::error(LOG_TAG, fmt::format("Dataset requires std::vector<scatter_point> in timestep 0."));
			}

			if (has_quads) {
				for (size_t i = 0; i < num_elements; ++i, ++line_num) {
					std::getline(file, line);
					auto tokens = util::split_string(line, ' ', 5);

					auto is_quad = std::stoul(tokens[4]) != 0;
					// triangle
					if (!is_quad) {
						auto idx =
						    glm::ivec3{std::stoi(tokens[1]) - 1, std::stoi(tokens[2]) - 1, std::stoi(tokens[3]) - 1};
						indices_tri.push_back(idx);
					}
					// quad
					else {
						auto idx = glm::ivec4{std::stoi(tokens[1]) - 1, std::stoi(tokens[2]) - 1,
						                      std::stoi(tokens[3]) - 1, std::stoi(tokens[4]) - 1};
						indices_quad.push_back(idx);
					}
				}
			}
			else {
				for (size_t i = 0; i < num_elements; ++i, ++line_num) {
					std::getline(file, line);
					auto tokens = util::split_string(line, ' ', 4);

					auto idx = glm::ivec3{std::stoi(tokens[1]) - 1, std::stoi(tokens[2]) - 1, std::stoi(tokens[3]) - 1};
					indices_tri.push_back(idx);
				}
			}
		}
		catch (const std::exception &ex) {
			throw read_exception(ex.what(), line, line_num);
		}

		positions.shrink_to_fit();
		indices_tri.shrink_to_fit();
		indices_quad.shrink_to_fit();
	}
	else {
		log::error(LOG_TAG, fmt::format("Couldn't open file: {0}", options->input_mesh_file.u8string()));
		return false;
	}

	return true;
}

bool mike21_reader::read_scalar(std::shared_ptr<data::triangle_dataset> dataset, std::filesystem::path path,
                                scalar_type type)
{
	// auto-close RAII
	std::ifstream file;

	if (file.open(path); file.is_open()) {
		util::log::info(LOG_TAG, fmt::format("Reading scalar file: {0}", path.u8string()));

		std::string line;
		size_t line_num = 1;

		auto &scatter = dataset->get_component<std::vector<scatter_point>>(0);

		try {
			if (type == scalar_type::depth) {
				for (size_t i = 0; i < scatter.size(); ++i, ++line_num) {
					std::getline(file, line);

					auto tokens = util::split_string(line, '\t', 4);
					scatter[i].position = glm::dvec2{std::stod(tokens[2]), std::stod(tokens[3])};
					scatter[i].depth.x = std::stod(tokens[1]);
				}
			}
			else if (type == scalar_type::velocity_u) {
				for (size_t i = 0; i < scatter.size(); ++i, ++line_num) {
					std::getline(file, line);

					auto tokens = util::split_string(line, '\t', 4);
					scatter[i].velocity.x = std::stod(tokens[1]);
				}
			}
			else if (type == scalar_type::velocity_v) {
				for (size_t i = 0; i < scatter.size(); ++i, ++line_num) {
					std::getline(file, line);

					auto tokens = util::split_string(line, '\t', 4);
					scatter[i].velocity.y = std::stod(tokens[1]);
				}
			}
		}
		catch (const std::exception &ex) {
			throw read_exception(ex.what(), line, line_num);
		}

		return true;
	}
	else {
		log::error(LOG_TAG, fmt::format("Couldn't open file: {0}", path.u8string()));
		return false;
	}

} // namespace IWD::io

bool mike21_reader::interpolate_scattered_data(std::shared_ptr<data::triangle_dataset> dataset)
{
	util::log::info(LOG_TAG, "Interpolating cell to node data.");

	struct intp_data {
		glm::dvec2 velocity{0.0, 0.0};
		double depth{0.0};
		double area{0.0};
		double distance{0.0};
	};

	const auto &scatter = dataset->get_component<std::vector<scatter_point>>(0);
	const auto &positions = dataset->get_component<data::triangle_dataset::position_3D>(0);
	const auto &triangle_indices = dataset->get_component<data::components::common::triangle_index>(0);
	const auto &quad_indices = dataset->get_component<data::components::common::quad_index>(0);

	auto &depth = dataset->emplace_or_replace_component<data::triangle_dataset::depth>(
	    data::triangle_dataset::depth(positions.size()), 0);
	auto &velocity = dataset->emplace_or_replace_component<data::triangle_dataset::velocity_2D>(
	    data::triangle_dataset::velocity_2D(positions.size()), 0);

	std::vector<std::vector<intp_data>> intpd(positions.size());

	data::components::common::bounding_box_3D bbox;
	bbox.calculate(positions);
	// extend bounds by X so objects on edges can have boxes without degeneration (X must be > diameter of objects)
	bbox.min -= glm::dvec3{1.0, 0.0, 1.0};
	bbox.max += glm::dvec3{1.0, 0.0, 1.0};

	auto box = quadtree::Box<double>({bbox.min.x, bbox.min.z}, {bbox.max.x - bbox.min.x, bbox.max.z - bbox.min.z});

	auto get_box = [](const scatter_point *sc) {
		return quadtree::Box<double>({sc->position.x, sc->position.y}, 0.01); // 1cm distance from center
	};

	// storable amount of points := (threshold * 4^depth)
	// for thresh = fix32 -> depth = ceil(log(x) / (2 * log(2)))
	int32_t threshold = 32;
	int32_t max_depth = static_cast<int32_t>(
	    std::ceil(std::log(scatter.size() / static_cast<double>(threshold)) / (2.0 * std::log(2.0))));

	auto qt =
	    quadtree::Quadtree<const scatter_point *, decltype(get_box), std::equal_to<const scatter_point *>, double>(
	        threshold, max_depth + 2 /*safety*/, box, get_box);

	// MUST NOT ALTER scatter UNTIL REFERENCES ARE NOT NEEDED ANYMORE AFTER THIS,
	// otherwise references may be invalidated
	for (const auto &sc : scatter) {
		qt.add(&sc);
	}

	for (size_t t = 0; t < triangle_indices.size(); ++t) {
		const auto &i = triangle_indices[t];

		auto v0 = positions[i.x];
		auto v1 = positions[i.y];
		auto v2 = positions[i.z];
		auto center = (v0 + v1 + v2) / 3.0;
		auto area = glm::length(glm::cross(v1 - v0, v2 - v0)) / 2.0;

		// should be only one, but could be multiple near points
		// might narrow down box radius (10cm currently)
		auto elems = qt.query({{center.x, center.z}, 0.1});

		for (auto &e : elems) {
			if (util::nearly_equal(center.x, e->position.x, 0.01) &&
			    util::nearly_equal(center.z, e->position.y, 0.01)) {
				if (e->depth.x > 0.0) {
					intpd[i.x].push_back({e->velocity, e->depth.x, area, glm::distance({v0.x, v0.z}, e->position)});
					intpd[i.z].push_back({e->velocity, e->depth.x, area, glm::distance({v1.x, v1.z}, e->position)});
					intpd[i.z].push_back({e->velocity, e->depth.x, area, glm::distance({v2.x, v2.z}, e->position)});
				}
				break;
			}
		}
	}

	for (size_t t = 0; t < quad_indices.size(); ++t) {
		const auto &i = quad_indices[t];

		auto v0 = positions[i.x];
		auto v1 = positions[i.y];
		auto v2 = positions[i.z];
		auto v3 = positions[i.w];
		auto center = (v0 + v1 + v2 + v3) / 4.0;
		auto area = glm::length(glm::cross(v1 - v0, v2 - v0)) / 2.0 + glm::length(glm::cross(v3 - v2, v0 - v2)) / 2.0;

		// should be only one, but could be multiple near points
		// might narrow down box radius (10cm currently)
		auto elems = qt.query({{center.x, center.z}, 0.1});

		for (auto &e : elems) {
			if (util::nearly_equal(center.x, e->position.x, 0.01) &&
			    util::nearly_equal(center.z, e->position.y, 0.01)) {
				if (e->depth.x > 0.0) {
					intpd[i.x].push_back({e->velocity, e->depth.x, area, glm::distance({v0.x, v0.z}, e->position)});
					intpd[i.z].push_back({e->velocity, e->depth.x, area, glm::distance({v1.x, v1.z}, e->position)});
					intpd[i.z].push_back({e->velocity, e->depth.x, area, glm::distance({v2.x, v2.z}, e->position)});
					intpd[i.w].push_back({e->velocity, e->depth.x, area, glm::distance({v3.x, v3.z}, e->position)});
				}
				break;
			}
		}
	}

	// define weights for interpolation
	constexpr double weight_area = 0.5;
	constexpr double weight_distance = 1.0 - weight_area;

	for (size_t t = 0; t < intpd.size(); ++t) {
		const auto &pd = intpd[t];

		double total_area{}, total_distance{};

		for (const auto &d : pd) {
			total_area += d.area;
			total_distance += d.distance;
		}

		double depth_weighted{};
		glm::dvec2 velocity_weighted{};

		for (const auto &d : pd) {
			depth_weighted += weight_area * (d.area / total_area * d.depth) +
			                  weight_distance * (d.distance / total_distance * d.depth);
			velocity_weighted += weight_area * (d.area / total_area * d.velocity) +
			                     weight_distance * (d.distance / total_distance * d.velocity);
		}

		depth[t].x = static_cast<float>(depth_weighted);
		velocity[t] = glm::tvec2<data::triangle_dataset::velocity_2D::value_type::value_type>(velocity_weighted);
	}

	return true;
}
void mike21_reader::quad_to_triangles(std::shared_ptr<data::triangle_dataset> dataset)
{
	auto &positions = dataset->get_component<data::triangle_dataset::position_3D>(0);
	auto &triangle_indices = dataset->get_component<data::components::common::triangle_index>(0);
	auto &quad_indices = dataset->get_component<data::components::common::quad_index>(0);

	triangle_indices.reserve(triangle_indices.size() + quad_indices.size() * 2);

	for (auto &q : quad_indices) {
		// use shorter edge to split
		auto dist_ac = glm::distance(positions[q.x], static_cast<glm::dvec3>(positions[q.z]));
		auto dist_bd = glm::distance(positions[q.y], static_cast<glm::dvec3>(positions[q.w]));

		if (dist_ac <= dist_bd) {
			triangle_indices.push_back(glm::ivec3{q.x, q.y, q.z});
			triangle_indices.push_back(glm::ivec3{q.x, q.z, q.w});
		}
		else {
			triangle_indices.push_back(glm::ivec3{q.x, q.y, q.w});
			triangle_indices.push_back(glm::ivec3{q.w, q.y, q.z});
		}
	}

	dataset->clear_component<data::components::common::quad_index>(0);
}
} // namespace IWD::io
