#include "io/config_reader.h"

#include <cctype>

#include "nlohmann/json.hpp"

#include "util/auto_enum.h"
#include "util/algorithm.h"
#include "util/io.h"
#include "util/logger.h"

using namespace IWD::util;
using json = nlohmann::json;

const std::string LOG_TAG = "{config_reader}";

namespace IWD::io {

config_reader::result::result() noexcept
{
	global_settings = std::make_shared<data::dataset_processing_options_global>();
}

config_reader::result config_reader::read(const std::filesystem::path &path)
{
	{
		log::info(LOG_TAG, fmt::format("Reading config file: {0}", path.u8string()));

		config_reader::result res{};
		auto global_settings = res.global_settings;

		std::string config_str = util::read_file_as_text(path);
		if (config_str.empty()) return res;

		json js;
		try {
			js = js.parse(config_str);
		}
		catch (const std::exception &ex) {
			log::error(LOG_TAG, "Error parsing json config file: " + std::string(ex.what()));
			return res;
		}

		try {
			// entirely optional
			if (js.contains("global_settings")) {
				auto &globals = js["global_settings"];

				// lexicographical order
				if (globals.contains("center_data")) global_settings->center_data = globals["center_data"].get<bool>();
				if (globals.contains("epsg_input")) global_settings->epsg_input = globals["epsg_input"].get<uint32_t>();
				if (globals.contains("epsg_output"))
					global_settings->epsg_output = globals["epsg_output"].get<uint32_t>();
				if (globals.contains("gltf_embed")) global_settings->gltf_embed = globals["gltf_embed"].get<bool>();
				if (globals.contains("gltf_glb")) global_settings->gltf_glb = globals["gltf_glb"].get<bool>();
				if (globals.contains("level_data")) global_settings->level_data = globals["level_data"].get<bool>();
				if (globals.contains("output_formats")) {
					auto global_output_formats = globals["output_formats"];
					for (auto &entry : global_output_formats) {
						auto str = IWD::util::to_lower(entry.get<std::string>());

						// prefix with underscore, e.g. 3d_tiles -> _3d_tiles
						if (isdigit(str[0])) str = '_' + str;

						auto type = auto_enum::type_from_name<data::dataset_output_type>(
						    str); // may resolve to dataset_input_type::error

						if (type == data::dataset_output_type::error) {
							// skip type but proceed ()
							log::warn(LOG_TAG, fmt::format("Unrecognized type '{0}' given in {1}", str,
							                               global_output_formats.dump(4, ' ')));
							continue;
						}
						global_settings->output_formats.push_back(type);
					}
				}
				if (globals.contains("output_timesteps"))
					global_settings->output_timesteps = globals["output_timesteps"].get<std::string>();
				if (globals.contains("remove_dry_cells"))
					global_settings->remove_dry_cells = globals["remove_dry_cells"].get<bool>();
				if (globals.contains("simplify")) global_settings->simplify = globals["simplify"].get<uint32_t>();
				if (globals.contains("tile_size")) global_settings->tile_size = globals["tile_size"].get<double>();
				if (globals.contains("tile_epsg")) global_settings->tile_epsg = globals["tile_epsg"].get<uint32_t>();
				if (globals.contains("lod_levels")) global_settings->lod_levels = globals["lod_levels"].get<uint32_t>();
			}

			if (!js.contains("jobs")) {
				log::error(LOG_TAG, fmt::format("Config file does not contain 'jobs': {0}", js.dump(4, ' ')));
				res.status = IWD::exit_code::invalid_arguments;
				return res;
			}
			auto &jobs = js["jobs"];

			if (jobs.empty()) {
				log::error(LOG_TAG, fmt::format("Jobs are empty: {0}", jobs.dump(4, ' ')));
				res.status = IWD::exit_code::invalid_arguments;
				return res;
			}

			for (auto &job : jobs) {
				if (!job.contains("type")) {
					log::error(LOG_TAG, fmt::format("Job does not contain 'type': {0}", job.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					return res;
				}
				auto job_str = job["type"].get<std::string>();
				// prefix with underscore, e.g. 3di -> _3di
				if (isdigit(job_str[0])) job_str = '_' + job_str;
				auto type = util::auto_enum::type_from_name<data::dataset_input_type>(job_str);

				if (type == data::dataset_input_type::error) {
					log::error(LOG_TAG, fmt::format("Unrecognized type '{0}' given in {1}", job_str, job.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					break;
				}
				auto job_opt = std::make_shared<data::job_processing_options>();
				job_opt->copy_from_base(global_settings);
				job_opt->input_type = type;

				if (!job.contains("output")) {
					log::error(LOG_TAG, fmt::format("Job does not contain 'output': {0}", job.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					break;
				}
				auto &output = job["output"];

				if (!output.contains("directory")) {
					log::error(LOG_TAG,
					           fmt::format("Job output does not contain 'directory': {0}", output.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					break;
				}
				// IMPROVE: Check C++20 std::u8string; nlohmann::json uses utf-8 internally
				job_opt->output_directory = output["directory"].get<std::string>();

				if (!output.contains("basename")) {
					log::error(LOG_TAG,
					           fmt::format("Job output does not contain 'basename': {0}", output.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					break;
				}
				// IMPROVE: Check C++20 std::u8string; nlohmann::json uses utf-8 internally
				job_opt->output_basename = output["basename"].get<std::string>();

				// override global settings per job
				if (job.contains("settings")) {
					auto &settings = job["settings"];

					// lexicographical order
					if (settings.contains("center_data")) job_opt->center_data = settings["center_data"].get<bool>();
					if (settings.contains("epsg_input")) job_opt->epsg_input = settings["epsg_input"].get<uint32_t>();
					if (settings.contains("epsg_output"))
						job_opt->epsg_output = settings["epsg_output"].get<uint32_t>();
					if (settings.contains("gltf_embed")) job_opt->gltf_embed = settings["gltf_embed"].get<bool>();
					if (settings.contains("gltf_glb")) job_opt->gltf_glb = settings["gltf_glb"].get<bool>();
					if (settings.contains("level_data")) job_opt->level_data = settings["level_data"].get<bool>();
					if (settings.contains("output_formats")) {
						auto output_formats = settings["output_formats"];
						for (auto &entry : output_formats) {
							auto f_str = IWD::util::to_lower(entry.get<std::string>());

							// prefix with underscore, e.g. 3d_tiles -> _3d_tiles
							if (isdigit(f_str[0])) f_str = '_' + f_str;

							auto f_type = auto_enum::type_from_name<data::dataset_output_type>(
							    f_str); // may resolve to dataset_input_type::error

							if (f_type == data::dataset_output_type::error) {
								// skip type but proceed ()
								log::warn(LOG_TAG, fmt::format("Unrecognized type '{0}' given in {1}", f_str,
								                               output_formats.dump(4, ' ')));
								continue;
							}
							job_opt->output_formats.push_back(f_type);
						}
					}
					if (settings.contains("output_timesteps"))
						global_settings->output_timesteps = settings["output_timesteps"].get<std::string>();
					if (settings.contains("remove_dry_cells"))
						job_opt->remove_dry_cells = settings["remove_dry_cells"].get<bool>();
					if (settings.contains("simplify")) job_opt->simplify = settings["simplify"].get<uint32_t>();
					if (settings.contains("tile_size"))
						global_settings->tile_size = settings["tile_size"].get<double>();
					if (settings.contains("tile_epsg"))
						global_settings->tile_epsg = settings["tile_epsg"].get<uint32_t>();
					if (settings.contains("lod_levels"))
						global_settings->lod_levels = settings["lod_levels"].get<uint32_t>();
				}

				// ------ Input type specifics ------ //
				if (!job.contains("input")) {
					log::error(LOG_TAG, fmt::format("Job does not contain 'input': {0}", job.dump(4, ' ')));
					res.status = IWD::exit_code::invalid_arguments;
					break;
				}

				auto &input = job["input"];

				switch (type) {
				case data::dataset_input_type::esri_ascii: {
					// TODO: no support for ascii yet
					// res.jobs.push_back(proc);
					break;
				}
				case data::dataset_input_type::sms_2dm: {
					auto proc = std::make_shared<data::sms_2dm_processing_options>(job_opt);

					if (!input.contains("mesh")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'mesh': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_mesh_file = input["mesh"].get<std::string>();

					if (!input.contains("depth")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'depth': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_depth_file = input["depth"].get<std::string>();

					if (!input.contains("waterlevel")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'waterlevel': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_waterlevel_file = input["waterlevel"].get<std::string>();

					if (!input.contains("velocity")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'velocity': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_velocity_file = input["velocity"].get<std::string>();

					res.jobs.push_back(proc);
					break;
				}
				case data::dataset_input_type::mike21: {
					auto proc = std::make_shared<data::mike21_processing_options>(job_opt);

					if (!input.contains("mesh")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'mesh': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_mesh_file = input["mesh"].get<std::string>();

					if (!input.contains("depth")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'depth': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_depth_file = input["depth"].get<std::string>();

					if (!input.contains("velocity_u")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'velocity_u': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_velocity_u_file = input["velocity_u"].get<std::string>();

					if (!input.contains("velocity_v")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'velocity_v': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_velocity_v_file = input["velocity_v"].get<std::string>();

					res.jobs.push_back(proc);
					break;
				}
				case data::dataset_input_type::geotiff: {
					auto proc = std::make_shared<data::geotiff_processing_options>(job_opt);

					if (!input.contains("dem")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'dem': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_dem_file = input["dem"].get<std::string>();

					if (!input.contains("depth")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'depth': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_depth_file = input["depth"].get<std::string>();

					if (!input.contains("velocity_u")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'velocity_u': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_velocity_u_file = input["velocity_u"].get<std::string>();

					if (!input.contains("velocity_v")) {
						log::error(LOG_TAG,
						           fmt::format("Job input does not contain 'velocity_v': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_velocity_v_file = input["velocity_v"].get<std::string>();

					res.jobs.push_back(proc);
					break;
				}
				case data::dataset_input_type::_3di: {
					auto proc = std::make_shared<data::_3di_processing_options>(job_opt);

					if (!input.contains("nc")) {
						log::error(LOG_TAG, fmt::format("Job input does not contain 'nc': {0}", input.dump(4, ' ')));
						res.status = IWD::exit_code::invalid_arguments;
						break;
					}
					proc->input_file = input["nc"].get<std::string>();

					res.jobs.push_back(proc);
					break;
				}
				default: {
					// error, unitialized -> handled previously
				}
				}
			}
		}
		catch (const std::exception &ex) {
			log::error(LOG_TAG, fmt::format("Error reading json as config: {0}", std::string(ex.what())));
			return res;
		}

		// may modify status of res
		validate(res);

		return res;
	}
}

void config_reader::validate(config_reader::result &result)
{

	// already faulty
	if (result.status != IWD::exit_code::normal) return;

	// global settings don't need to be checked at this point as job setting already inherited from them

	for (const auto &job : result.jobs) {
		if (job->gltf_embed && job->gltf_glb) {
			log::error(LOG_TAG, "Settings 'gltf_embed' and 'gltf_glb' are mutually exclusive. Use only one at a time.");
			result.status = IWD::exit_code::invalid_arguments;
			return;
		}
		if (std::find(job->output_formats.begin(), job->output_formats.end(), data::dataset_output_type::gltf) ==
		        job->output_formats.end() &&
		    (job->gltf_embed || job->gltf_glb)) {

			if (std::find(job->output_formats.begin(), job->output_formats.end(),
			              data::dataset_output_type::_3d_tiles) == job->output_formats.end()) {
				log::warn(LOG_TAG,
				          "Settings 'gltf_embed' and 'gltf_glb' must be used with output type 'gltf or '3d_tiles'. "
				          "Skipping these settings.");
			}
		}

		if (std::find(job->output_formats.begin(), job->output_formats.end(), data::dataset_output_type::_3d_tiles) !=
		        job->output_formats.end() &&
		    (job->gltf_embed)) {
			log::warn(LOG_TAG,
			          "Setting 'gltf_embed' cannot be used with output format '3d_tiles'. Skipping this setting.");
		}

		if (std::find(job->output_formats.begin(), job->output_formats.end(), data::dataset_output_type::gltf) !=
		        job->output_formats.end() &&
		    std::find(job->output_formats.begin(), job->output_formats.end(), data::dataset_output_type::_3d_tiles) !=
		        job->output_formats.end()) {
			log::error(LOG_TAG, "Output formats 'gltf' and '3d_tiles' cannot be used together. Choose one.");
			result.status = IWD::exit_code::invalid_arguments;
			return;
		}

		if (job->tile_epsg != 0 && job->tile_size == 0.0) {
			log::warn(LOG_TAG, "Settings 'tile_epsg' was used without 'tile_size'. Skipping this setting.");
		}

		if (!std::filesystem::is_directory(job->output_directory)) {
			if (!std::filesystem::create_directory(job->output_directory)) {
				log::error(LOG_TAG, fmt::format("Path '{0}' is not a directory.", job->output_directory.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}
		}
#if PROJ_ENABLED
		if (job->epsg_input == 0u && job->epsg_output != 0u) {
			// geotiffs embed epsg, no input required
			if (!(job->input_type == data::dataset_input_type::geotiff ||
			      job->input_type == data::dataset_input_type::_3di)) {
				log::error(LOG_TAG, "Option epsg_output was set, but no epsg_input was provided.");
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}
		}
#elif
		if (job->epsg_output != 0u || job->epsg_output == 0u) {
			log::error(LOG_TAG,
			           "Option epsg_input or epsg_output was set, but lib-iwd was compiled without \"proj\" support.");
			result.status = IWD::exit_code::invalid_arguments;
			return;
		}
#endif

		switch (job->input_type) {
		case data::dataset_input_type::esri_ascii: {
			break;
		}
		case data::dataset_input_type::sms_2dm: {
			auto sms_job = std::static_pointer_cast<data::sms_2dm_processing_options>(job);

			// IMPROVE: may reorder cases to allow fallthrough of common input types
			// or handle common input before switch statement
			if (!std::filesystem::is_regular_file(sms_job->input_mesh_file)) {
				log::error(LOG_TAG,
				           fmt::format("Job mesh file: {0} does not exist.", sms_job->input_mesh_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(sms_job->input_depth_file)) {
				log::error(LOG_TAG,
				           fmt::format("Job depth file: {0} does not exist.", sms_job->input_depth_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(sms_job->input_waterlevel_file)) {
				log::error(LOG_TAG, fmt::format("Job waterlevel file: {0} does not exist.",
				                                sms_job->input_waterlevel_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(sms_job->input_velocity_file)) {
				log::error(LOG_TAG, fmt::format("Job velocity file: {0} does not exist.",
				                                sms_job->input_velocity_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			break;
		}
		case data::dataset_input_type::mike21: {
			auto mike_job = std::static_pointer_cast<data::mike21_processing_options>(job);

			if (!std::filesystem::is_regular_file(mike_job->input_mesh_file)) {
				log::error(LOG_TAG,
				           fmt::format("Job mesh file: {0} does not exist.", mike_job->input_mesh_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(mike_job->input_depth_file)) {
				log::error(LOG_TAG,
				           fmt::format("Job depth file: {0} does not exist.", mike_job->input_depth_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(mike_job->input_velocity_u_file)) {
				log::error(LOG_TAG, fmt::format("Job velocity_u file: {0} does not exist.",
				                                mike_job->input_velocity_u_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(mike_job->input_velocity_v_file)) {
				log::error(LOG_TAG, fmt::format("Job velocity_v file: {0} does not exist.",
				                                mike_job->input_velocity_v_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}
			break;
		}
		case data::dataset_input_type::geotiff: {
#ifndef GEOTIFF_ENABLED
			log::error(LOG_TAG,
			           "Trying to use GeoTIFF input files without compiling GeoTIFF support into lib-iwd. Aborting.");
			result.status = IWD::exit_code::invalid_arguments;
			return;
#endif
			auto geotiff_job = std::static_pointer_cast<data::geotiff_processing_options>(job);

			if (!std::filesystem::is_regular_file(geotiff_job->input_dem_file)) {
				log::error(LOG_TAG,
				           fmt::format("Job mesh file: {0} does not exist.", geotiff_job->input_dem_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(geotiff_job->input_depth_file)) {
				log::error(LOG_TAG, fmt::format("Job depth file: {0} does not exist.",
				                                geotiff_job->input_depth_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(geotiff_job->input_velocity_u_file)) {
				log::error(LOG_TAG, fmt::format("Job velocity_u file: {0} does not exist.",
				                                geotiff_job->input_velocity_u_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			if (!std::filesystem::is_regular_file(geotiff_job->input_velocity_v_file)) {
				log::error(LOG_TAG, fmt::format("Job velocity_v file: {0} does not exist.",
				                                geotiff_job->input_velocity_v_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}
			break;
		}
		case data::dataset_input_type::_3di: {
#ifndef NETCDFCXX_ENABLED
			log::error(LOG_TAG,
			           "Trying to use 3Di input files without compiling netCDF support into lib-iwd. Aborting.");
			result.status = IWD::exit_code::invalid_arguments;
			return;
#endif
			auto _3di_job = std::static_pointer_cast<data::_3di_processing_options>(job);

			if (!std::filesystem::is_regular_file(_3di_job->input_file)) {
				log::error(LOG_TAG, fmt::format("Job nc file: {0} does not exist.", _3di_job->input_file.u8string()));
				result.status = IWD::exit_code::invalid_arguments;
				return;
			}

			break;
		}
		default: {
			// error, unitialized -> handled previously
		}
		}
	}
}

} // namespace IWD::io