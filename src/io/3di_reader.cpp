#if NETCDFCXX_ENABLED

#include "io/3di_reader.h"

#include <netcdf>

#include "quadtree/Quadtree.h"
#include "delaunator-cpp/delaunator.hpp"

#include "util/io.h"
#include "util/algorithm.h"

using namespace IWD::util;

const std::string LOG_TAG = "{3di_reader}";

namespace IWD::io {
bool _3di_reader::read(std::shared_ptr<data::triangle_dataset> dataset,
                       std::shared_ptr<const data::_3di_processing_options> options, uint32_t &epsg)
{
	util::log::info(LOG_TAG, fmt::format("Reading netCDF file: {0}", options->input_file.u8string()));

	try {
		netCDF::NcFile nc(options->input_file.u8string(), netCDF::NcFile::FileMode::read);
		if (nc.isNull()) {
			util::log::error(LOG_TAG,
			                 fmt::format("Could not open or read input file: {0}", options->input_file.u8string()));
			return false;
		}

		// get dimensions
		size_t num_nodes = 0, num_timesteps = 0;
		{
			auto n_mesh2d = nc.getDim("nMesh2D_nodes");
			if (n_mesh2d.isNull()) {
				util::log::error(LOG_TAG, "Data dimensions do not contain 'nMesh2D_nodes'.");
				return false;
			}
			num_nodes = n_mesh2d.getSize();

			auto time = nc.getDim("time");
			if (time.isNull()) {
				util::log::error(LOG_TAG, "Data dimensions do not contain 'time'.");
				return false;
			}
			num_timesteps = time.getSize();
		}

		// read timesteps
		std::vector<float> timesteps(num_timesteps);
		{
			auto var = nc.getVar("time");
			if (var.isNull()) {
				util::log::error(LOG_TAG, "Data variables do not contain 'time'.");
				return false;
			}
			var.getVar(timesteps.data());
		}

		// read EPSG
		{
			auto var = nc.getVar("projected_coordinate_system");
			if (var.isNull()) {
				util::log::error(LOG_TAG, "Data variables do not contain 'projected_coordinate_system'.");
				return false;
			}

			auto epsg_attr = var.getAtt("epsg");
			if (epsg_attr.isNull()) {
				util::log::error(LOG_TAG,
				                 "Data variables do not contain 'projected_coordinate_system' with attribute 'epsg'.");
				return false;
			}

			auto len = epsg_attr.getAttLength();
			std::string epsg_str(len, ' ');

			epsg_attr.getValues(epsg_str.data());
			try {
				epsg = static_cast<uint32_t>(std::stoi(epsg_str));
			}
			catch (std::invalid_argument &e) {
				util::log::error(LOG_TAG, fmt::format("Could not convert epsg characters to number: {}", epsg_str));
				util::log::error(LOG_TAG, e.what());
				return false;
			}
			catch (std::out_of_range &e) {
				util::log::error(LOG_TAG, e.what());
				return false;
			}
		}

		// read cell types
		std::vector<int32_t> cell_types(num_nodes);
		{
			auto var = nc.getVar("Mesh2DNode_type");
			if (var.isNull()) {
				util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2DNode_type'.");
				return false;
			}
			var.getVar({0ull}, {num_nodes}, cell_types.data());

			size_t i = 0ull;
			for (const auto &t : cell_types) {
				if (t != 1) {
					num_nodes = i; // adjust num_nodes to remove unnecessary data (reads + memory)
					log::info(LOG_TAG,
					          "Found non surface-water cells in dataset. Ignoring them for further processing.");

					if (i == 0) {
						log::error(LOG_TAG,
						           "Cannot read a dataset that doesn't start with surface-water cells. Aborting.");
						return false;
					}
					break;
				}
				++i;
			}
		}

		// read positions
		std::vector<glm::dvec3> position_raw(num_nodes);
		{
			std::vector<double> pos_buffer(num_nodes);
			{
				auto pos_x = nc.getVar("Mesh2DFace_xcc");
				if (pos_x.isNull()) {
					util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2DFace_xcc'.");
					return false;
				}
				pos_x.getVar({0ull}, {num_nodes}, pos_buffer.data());
				for (size_t t = 0; t < pos_buffer.size(); ++t) position_raw[t].x = pos_buffer[t];
			}
			{
				auto pos_y = nc.getVar("Mesh2DFace_ycc");
				if (pos_y.isNull()) {
					util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2DFace_ycc'.");
					return false;
				}
				pos_y.getVar({0ull}, {num_nodes}, pos_buffer.data());
				for (size_t t = 0; t < pos_buffer.size(); ++t) position_raw[t].y = pos_buffer[t];
			}
			{
				auto pos_z = nc.getVar("Mesh2DFace_zcc");
				if (pos_z.isNull()) {
					util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2DFace_zcc'.");
					return false;
				}
				pos_z.getVar({0ull}, {num_nodes}, pos_buffer.data());
				for (size_t t = 0; t < pos_buffer.size(); ++t) position_raw[t].z = pos_buffer[t];
			}

			// make swap explicitly visible
			for (auto &p : position_raw) std::swap(p.y, p.z);
		}

		// read depths
		std::vector<float> depth_raw(num_nodes * num_timesteps);
		{
			auto wse = nc.getVar("Mesh2D_s1");
			if (wse.isNull()) {
				util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2D_s1'.");
				return false;
			}

			// could also read timestep per timestep for all variables
			for (size_t t = 0; t < num_timesteps; ++t) {
				std::vector<size_t> offset = {t, 0};
				std::vector<size_t> count = {1, num_nodes};
				wse.getVar(offset, count, depth_raw.data() + t * num_nodes);
			}

			// Do not remove the information of NODATA_value here but use it later for wse calculation
			// for (auto &d : depth_raw)
			//	if (d == -9999.0) d = 0;
		}

		// read velocities
		std::vector<glm::fvec2> velocity_raw(num_nodes * num_timesteps);
		{
			std::vector<float> velocity_buffer(num_nodes);
			{
				auto vu = nc.getVar("Mesh2D_ucx");
				if (vu.isNull()) {
					util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2D_ucx'.");
					return false;
				}

				// could also read timestep per timestep for all variables
				for (size_t t = 0; t < num_timesteps; ++t) {
					std::vector<size_t> offset = {t, 0};
					std::vector<size_t> count = {1, num_nodes};
					vu.getVar(offset, count, velocity_buffer.data());

					for (auto &u : velocity_buffer)
						if (u == -9999.0f) u = 0;

					for (size_t i = 0; i < velocity_buffer.size(); ++i)
						velocity_raw[t * num_nodes + i].x = velocity_buffer[i];
				}
			}
			{
				auto vu = nc.getVar("Mesh2D_ucy");
				if (vu.isNull()) {
					util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2D_ucy'.");
					return false;
				}

				// could also read timestep per timestep for all variables
				for (size_t t = 0; t < num_timesteps; ++t) {
					std::vector<size_t> offset = {t, 0};
					std::vector<size_t> count = {1, num_nodes};
					vu.getVar(offset, count, velocity_buffer.data());

					for (auto &u : velocity_buffer)
						if (u == -9999.0f) u = 0;

					for (size_t i = 0; i < velocity_buffer.size(); ++i)
						velocity_raw[t * num_nodes + i].y = velocity_buffer[i];
				}
			}
		}

		// construct timesteps of dataset, builds entities
		if (timesteps[0] != 0.0f) dataset->update_timestep(0, timesteps[0]);
		for (size_t t = 1; t < timesteps.size(); ++t) {
			dataset->append_timestep(timesteps[t]);
		}

		// read contours, diff = [1] - [0]
		// 4 x-positions for every corner
		std::vector<std::array<double, 4>> cell_sizes(num_nodes);
		{
			auto var = nc.getVar("Mesh2DContour_x");
			if (var.isNull()) {
				util::log::error(LOG_TAG, "Data variables do not contain 'Mesh2DContour_x'.");
				return false;
			}

			auto cxdims = var.getDims(); // [0] = num_nodes, [1] == 4
			//auto cxdimsv = std::array<size_t, 2>({cxdims[0].getSize(), cxdims[1].getSize()});
			var.getVar({0ull, 0ull}, {num_nodes, 4ull}, cell_sizes.data());
		}

		// compute corners
		std::vector<std::array<glm::dvec3, 4>> corner_positions(num_nodes);
		for (size_t t = 0; t < position_raw.size(); ++t) {
			const auto &pos = position_raw[t];
			const auto cell_size = (cell_sizes[t][1] - cell_sizes[t][0]) * 0.5;
			corner_positions[t] = {
			    pos + glm::dvec3(-cell_size, 0.0, +cell_size), // up left
			    pos + glm::dvec3(-cell_size, 0.0, -cell_size), // down left
			    pos + glm::dvec3(+cell_size, 0.0, -cell_size), // down right
			    pos + glm::dvec3(+cell_size, 0.0, +cell_size), // up right
			};
		}

		// remove nodata vertices
		// IMPROVE: maybe use multi remove-erase instead
		if (std::any_of(position_raw.begin(), position_raw.end(), [](const glm::dvec3 &p) { return p.y == -9999.0; })) {
			auto remove_count = 0ull;
			decltype(position_raw) position_raw_temp;
			decltype(depth_raw) depth_raw_temp;
			decltype(velocity_raw) velocity_raw_temp;
			decltype(cell_sizes) cell_sizes_temp;
			decltype(corner_positions) corner_positions_temp;

			for (size_t c = 0; c < position_raw.size(); ++c) {
				if (position_raw[c].y == -9999.0) {
					bool has_later_data = false;

					for (size_t ts = 0; ts < timesteps.size(); ++ts) {
						if (depth_raw[ts * num_nodes + c] != -9999.0f) {
							has_later_data = true;
							break;
						}
					}

					if (!has_later_data) {
						++remove_count;
						break;
					}
				}

				position_raw_temp.push_back(position_raw[c]);
				cell_sizes_temp.push_back(cell_sizes[c]);
				corner_positions_temp.push_back(corner_positions[c]);

				for (size_t ts = 0; ts < timesteps.size(); ++ts) {
					depth_raw_temp.push_back(depth_raw[ts * num_nodes + c]);
					velocity_raw_temp.push_back(velocity_raw[ts * num_nodes + c]);
				}
			}

			position_raw = std::move(position_raw_temp);
			depth_raw = std::move(depth_raw_temp);
			velocity_raw = std::move(velocity_raw_temp);
			cell_sizes = std::move(cell_sizes_temp);
			corner_positions = std::move(corner_positions_temp);

			num_nodes = position_raw.size();

			log::info(LOG_TAG, fmt::format("Removed {} no-data vertices.", remove_count));
		}

		// construct bounding box
		data::components::common::bounding_box_3D bbox_all;

		for (const auto &c : corner_positions) {
			for (auto i = 0; i < 4; ++i) {
				bbox_all.min = glm::min(bbox_all.min, c[i]);
				bbox_all.max = glm::max(bbox_all.max, c[i]);
			}
		}

		// extend bounds by X so objects on edges can have boxes without degeneration (X must be > diameter of objects)
		// asymetrical so that exactly centered nodes are sorted either west or east, north or south
		bbox_all.min -= glm::dvec3{1.0, 0.0, 1.0};
		bbox_all.max += glm::dvec3{2.0, 0.0, 2.0};

		struct accum {
			std::vector<glm::fvec2> velocity;
			std::vector<glm::fvec1> depth;
			std::vector<double> distance;
			glm::dvec3 position;
		};

		auto box = quadtree::Box<double>({bbox_all.min.x, bbox_all.min.z},
		                                 {bbox_all.max.x - bbox_all.min.x, bbox_all.max.z - bbox_all.min.z});

		auto get_box_accum = [&](const accum *a) {
			return quadtree::Box<double>({a->position.x, a->position.z}, 0.001);
		};

		size_t threshold = 32;
		size_t max_depth = static_cast<size_t>(std::ceil(
		    std::log(static_cast<double>(corner_positions.size() * 4) / static_cast<double>(threshold)) / (2.0 * std::log(2.0))));

		// define outside of loop so we can access last loops data for triangulation
		std::vector<accum *> ipol_data;
		ipol_data.reserve(static_cast<size_t>(static_cast<double>(num_nodes) * 1.5)); // theoretical max is 4

		// define outside as we don't want to build the Quadtree every timestep
		auto ipol_qt = quadtree::Quadtree<accum *, decltype(get_box_accum), std::equal_to<accum *>, double>(
		    threshold, max_depth + 2ull /*safety*/, box, get_box_accum);

		for (size_t ts = 0; ts < timesteps.size(); ++ts) {
			if (ts != 0) {
				for (auto i : ipol_data) {
					i->depth.clear();
					i->velocity.clear();
				}
			}

			for (auto c = 0ull; c < corner_positions.size(); ++c) {
				size_t tc_idx = ts * num_nodes + c;

				// MSL to depth, if NODATA_value keep
				glm::fvec1 depth{0.0f};

				if (depth_raw[ts * num_nodes + c] == -9999.0f) {

					depth = glm::fvec1(0.0f);
				}
				else {
					depth = glm::fvec1(depth_raw[tc_idx]) - glm::fvec1(static_cast<float>(position_raw[c].y));
				}

				auto distance = std::sqrt(util::sqr((cell_sizes[c][1] - cell_sizes[c][0])));
				auto velocity = velocity_raw[tc_idx]; // same data for all corners

				for (auto i = 0; i < 4; ++i) {
					auto elems = ipol_qt.query({{corner_positions[c][i].x, corner_positions[c][i].z}, 0.01});

					// first element, insert accum
					if (elems.empty() && ts == 0) {
						accum *ipol = new accum();

						ipol->position = corner_positions[c][i];
						ipol->velocity.push_back(velocity);
						ipol->depth.push_back(depth);
						ipol->distance.push_back(distance);

						ipol_data.push_back(std::move(ipol));
						ipol_qt.add(ipol_data.back());
					}
					// append data
					else if (elems.size() == 1) {
						auto &ipol = elems[0];

						ipol->velocity.push_back(velocity);
						ipol->depth.push_back(depth);

						if (ts == 0) ipol->distance.push_back(distance);
					}
					else if (elems.empty() && ts != 0) {
						/* NO-OP */
					}
					// should not happen
					else {
						util::log::error(LOG_TAG, "Bounding Box mismatch on interpolation.");
						return false;
					}
				}
			}
			ipol_data.shrink_to_fit();

			// store position only for first timestep
			if (ts == 0) {
				auto &positions = dataset->emplace_or_replace_component<data::triangle_dataset::position_3D>({}, 0);
				positions.reserve(ipol_data.size());

				for (const auto &i : ipol_data) {
					positions.push_back(i->position);
				}
			}

			auto &depths = dataset->emplace_or_replace_component<data::triangle_dataset::depth>({}, ts);
			depths.reserve(ipol_data.size());
			auto &velocities = dataset->emplace_or_replace_component<data::triangle_dataset::velocity_2D>({}, ts);
			velocities.reserve(ipol_data.size());

			// compute weights and store depth and velocity
			for (auto &i : ipol_data) {
				std::transform(i->distance.begin(), i->distance.end(), i->distance.begin(),
				               [](double a) { return 1.0 / a; });
				double total_distance = std::accumulate(i->distance.begin(), i->distance.end(), 0.0);

				double depth_tmp{0.0};
				glm::dvec2 velocity_tmp{0.0, 0.0};

				// distance, depth and velocity have same size
				for (size_t t = 0; t < i->distance.size(); ++t) {
					double weight = i->distance[t] / total_distance;

					depth_tmp += weight * static_cast<double>(i->depth[t].x);
					velocity_tmp += weight * glm::dvec2(i->velocity[t]);
				}

				depths.push_back(glm::fvec1(static_cast<float>(depth_tmp))); // reduce precision
				velocities.push_back(glm::fvec2(velocity_tmp));              // reduce precision
			}
		}

		for (auto i : ipol_data) {
			delete i;
		}

		auto &positions = dataset->get_component<data::triangle_dataset::position_3D>(0);

		std::vector<double> linearized_coords;
		linearized_coords.reserve(2 * positions.size());

		// better meshing of colinear and coplanar vertices by applying jitter
		const double JITTER = 0.00001;
		std::mt19937_64 mt(42u); // fix seed
		std::uniform_real_distribution<double> dt(-JITTER, JITTER);

		// as there are no overlapping (y) triangles in this type of mesh, can use 2D delaunay
		for (const auto &p : positions) {
			linearized_coords.push_back(p.x + dt(mt));
			linearized_coords.push_back(p.z + dt(mt));
		}

		// https://github.com/abellgithub/delaunator-cpp
		delaunator::Delaunator delaunator(linearized_coords);

		// removal of added convex triangles
		// construct and fill Quadtree for
		auto get_box_corner = [&](const std::array<glm::dvec3, 4> *c) {
			auto center = ((*c)[0] + (*c)[1] + (*c)[2] + (*c)[3]) / 4.0;
			auto size = ((*c)[3].x - (*c)[0].x) / 2.0;
			return quadtree::Box<double>({center.x, center.z}, size - 0.01); // 1cm safety
		};

		auto qt = quadtree::Quadtree<const std::array<glm::dvec3, 4> *, decltype(get_box_corner),
		                             std::equal_to<const std::array<glm::dvec3, 4> *>, double>(
		    threshold, max_depth + 2ull /*safety*/, box, get_box_corner);

		for (auto &i : corner_positions) {
			qt.add(&i);
		}

		auto &indices = dataset->emplace_or_replace_component<data::components::common::triangle_index>({}, 0);
		indices.reserve(delaunator.triangles.size() / 3);
		for (std::size_t t = 0; t < delaunator.triangles.size(); t += 3) {
			const auto &p0 = positions[delaunator.triangles[t]];
			const auto &p1 = positions[delaunator.triangles[t + 1]];
			const auto &p2 = positions[delaunator.triangles[t + 2]];
			auto center = (p0 + p1 + p2) / 3.0;
			auto elems = qt.query({{center.x, center.z}, 0.01});

			// found box corresponding to triangle
			if (elems.size() == 1) {
				indices.push_back(glm::ivec3({static_cast<int32_t>(delaunator.triangles[t])},
				                             {static_cast<int32_t>(delaunator.triangles[t + 1])},
				                             {static_cast<int32_t>(delaunator.triangles[t + 2])}));
			}
			else {
				// log::info(LOG_TAG, "tri not in quad");
				// may also be duplicated data for different types e.g. groundwater
			}
		}
		indices.shrink_to_fit();
	}
	catch (netCDF::exceptions::NcException &e) {
		util::log::error(LOG_TAG, e.what());
		return false;
	}

	return true;
}
} // namespace IWD::io

#endif