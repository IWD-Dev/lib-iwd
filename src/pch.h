#pragma once

// common stl
#define _USE_MATH_DEFINES
#include <cmath>
#include <limits>
#include <numeric>
#include <cstddef>
#include <string>
#include <string_view>
#include <sstream>
#include <chrono>

#include <array>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <stack>
#include <queue>
#include <deque>
#include <list>
#include <forward_list>
#include <tuple>

#include <filesystem>
#include <fstream>
#include <iostream>

#include <algorithm>
#include <functional>
#include <utility>
#include <memory>
#include <type_traits>
#include <random>
#include <stdexcept>
#include <cassert>
#include <iterator>
#include <regex>

#include <atomic>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>

// third party
#pragma warning(push, 0) // disable all warnings

#include <glm/glm.hpp>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <magic_enum/magic_enum.hpp>
#include <entt/entt.hpp>



#if PROJ_ENABLED
#include "proj.h"
#endif

#pragma warning(pop)

// if WIN is ever included
//#define WIN32_LEAN_AND_MEAN
//#define NOMINMAX

// future stuff for c++20
// ranges-v3, gsl::span