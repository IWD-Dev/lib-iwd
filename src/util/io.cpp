#include <vector>
#include <string>
#include <fstream>

#include "util/io.h"

#include "util/logger.h"

namespace IWD::util {

std::string read_file_as_text(const std::filesystem::path &path)
{
	std::ifstream file(path);

	if (!file.is_open()) {
		log::error("{util::io::read_file_as_text}", fmt::format("Could not find given path:  {0}", path.u8string()));
		return "";
	}

	file.seekg(0, std::ifstream::end);
	size_t length = file.tellg();
	std::vector<char> content;
	content.resize(length);
	file.seekg(0, std::ifstream::beg);
	file.read(content.data(), length);
	return std::string(content.data(), length);
}

std::vector<std::string> split_string(const std::string &str, char sep, size_t size_hint)
{
	std::vector<std::string> tokens;
	tokens.reserve(size_hint);
	std::size_t start = 0, end = 0;
	while ((end = str.find(sep, start)) != std::string::npos) {
		tokens.push_back(str.substr(start, end - start));
		start = end + 1;
	}
	tokens.push_back(str.substr(start));
	return tokens;
}

std::string get_right_of_delim(const std::string &str, const std::string &delim)
{
	return str.substr(str.find(delim) + delim.size());
}

std::string to_lower(const std::string &str)
{
	auto ret = str;
	std::transform(ret.begin(), ret.end(), ret.begin(),
	               [](unsigned char c) { return static_cast<unsigned char>(std::tolower(c)); });
	return ret;
}

std::string remove_inner_whitespace(std::string &str, const char whitespace)
{
	std::string::iterator new_end =
	    std::unique(str.begin(), str.end(), [=](char lhs, char rhs) { return (lhs == rhs) && (lhs == whitespace); });
	str.erase(new_end, str.end());
	return str;
}

std::string &ltrim(std::string &s, const char *t)
{
	s.erase(0, s.find_first_not_of(t));
	return s;
}

std::string &rtrim(std::string &s, const char *t)
{
	s.erase(s.find_last_not_of(t) + 1);
	return s;
}

std::string &trim(std::string &s, const char *t) { return ltrim(rtrim(s, t), t); }

} // namespace IWD::util::io
