#include "util/wgs84.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace IWD::util::wgs84 {

/////////////////////////////////////////////////////////////////////////////////

static const glm::dvec3 CART_X = {1.0, 0.0, 0.0};
static const glm::dvec3 CART_Y = {0.0, 1.0, 0.0};
static const glm::dvec3 CART_Z = {0.0, 0.0, 1.0};

static const glm::dvec3 radii = {6378137.0, 6378137.0, 6356752.3142451793};
static const glm::dvec3 radii_squared = {40680631590769.0, 40680631590769.0, 40408299984661.445};
static const glm::dvec3 one_over_radii_squared = {2.458172257647332e-14, 2.458172257647332e-14,
                                                  2.4747391015697002e-14};

/////////////////////////////////////////////////////////////////////////////////

glm::dmat4 get_transform(double latitude, double longitude, double height)
{

	double cos_latitude = std::cos(latitude);
	glm::dvec3 scratch_N =
	    glm::normalize(glm::dvec3{cos_latitude * std::cos(longitude),
	                              cos_latitude * std::sin(longitude), std::sin(latitude)});

	glm::dvec3 scratch_K = radii_squared * scratch_N;

	double gamma = std::sqrt(glm::dot(scratch_N, scratch_K));
	scratch_K /= gamma;
	scratch_N *= height;

	glm::dvec3 position = {scratch_K.x + scratch_N.x, scratch_K.y + scratch_N.y,
	                       scratch_K.z + scratch_N.z};

	glm::dvec3 normal = glm::normalize(position * one_over_radii_squared);
	glm::dvec3 tangent = glm::normalize(glm::dvec3{-position.y, position.x, 0.0});
	glm::dvec3 bitangent = glm::cross(normal, tangent);

	glm::dmat4 mat = {tangent.y,    normal.y,   -bitangent.y, position.y, tangent.z,    normal.z,
	                  -bitangent.z, position.z, tangent.x,    normal.x,   -bitangent.x, position.x,
	                  0.0,          0.0,        0.0,          1.0};

	return mat;
}

glm::dvec3 get_position(double latitude, double longitude, double height)
{
	double cos_latitude = std::cos(latitude);
	glm::dvec3 scratch_N =
	    glm::normalize(glm::dvec3{cos_latitude * std::cos(longitude),
	                              cos_latitude * std::sin(longitude), std::sin(latitude)});

	glm::dvec3 scratch_K = radii_squared * scratch_N;

	double gamma = std::sqrt(glm::dot(scratch_N, scratch_K));
	scratch_K /= gamma;
	scratch_N *= height;

	return scratch_K + scratch_N;
}

//glm::dmat4 get_modelmatrix(glm::dvec3 position)
//{
//	auto modelmatrix = heading_pitch_roll_to_fixed_frame(position, 0.0, 0.0, 0.0);
//	auto rot_x = glm::rotate(glm::dmat4(1.0), -0.5 * M_PI, CART_X);
//	auto flip = glm::rotate(glm::dmat4(1.0), M_PI, CART_X);
//	auto rot_y = glm::rotate(glm::dmat4(1.0), (-45.0 * M_PI) / 180.0, CART_Y);
//
//	auto trans = flip * rot_x * modelmatrix * rot_x * rot_y;
//	return trans;
//}
//
//glm::dmat4 heading_pitch_roll_to_fixed_frame(glm::dvec3 position, double heading, double pitch,
//                                             double roll)
//{
//	glm::dquat hpr_quaternion = quaternion_from_heading_pitch_roll(heading, pitch, roll);
//	glm::dvec3 translation{};
//	glm::dvec3 scale = {1.0, 1.0, 1.0};
//	glm::dmat4 hprMatrix =
//	    matrix4_from_translation_quaternion_scale(translation, hpr_quaternion, scale);
//	glm::dmat4 result = east_north_up_to_fixed_frame(position);
//
//	result *= hprMatrix;
//	return result;
//}
//
//// https://github.com/g-truc/glm/issues/535
//glm::dquat quaternion_from_heading_pitch_roll(double heading, double pitch, double roll)
//{
//	glm::dquat qpitch = glm::angleAxis(pitch, CART_X);
//	glm::dquat qyaw = glm::angleAxis(heading, CART_Y);
//	glm::dquat qroll = glm::angleAxis(roll, CART_Z);
//
//	return qyaw * qpitch * qroll;
//}
//
//glm::dmat4 matrix4_from_translation_quaternion_scale(glm::dvec3 translation, glm::dquat rotation,
//                                                     glm::dvec3 scale)
//{
//	glm::dmat4 translate = glm::translate(glm::dmat4(1), translation);
//	glm::dmat4 rotate = glm::toMat4(rotation);
//	glm::dmat4 scaling = glm::scale(glm::dmat4(1), scale);
//
//	return translate * rotate * scaling;
//}
//
//glm::dmat4 east_north_up_to_fixed_frame(glm::dvec3 position)
//{
//	glm::dvec3 normal = glm::normalize(position * one_over_radii_squared);
//	glm::dvec3 tangent = glm::normalize(glm::dvec3{-position.y, position.x, 0.0});
//	glm::dvec3 bitangent = glm::cross(normal, tangent);
//
//	glm::dmat4 mat = {tangent.x, bitangent.x, normal.x,  position.x,  tangent.y, bitangent.y,
//	                  normal.y,  position.y,  tangent.z, bitangent.z, normal.z,  position.z,
//	                  0.0,       0.0,         0.0,       1.0};
//
//	return mat;
//}

} // namespace wgs84