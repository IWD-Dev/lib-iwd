#include "util/algorithm.h"

namespace IWD::util {

template <> int to_numerical<int>(const std::string &str) { return std::stoi(str); }
template <> float to_numerical<float>(const std::string &str) { return std::stof(str); }
template <> double to_numerical<double>(const std::string &str) { return std::stod(str); }

} // namespace util