#include "data/triangle_dataset.h"

namespace IWD::data {

dataset::type triangle_dataset::get_type() noexcept { return dataset::type::triangle; }

size_t triangle_dataset::vertex_count()
{
	auto p = try_get_component<triangle_dataset::position_3D>(0);
	if (p) return p->size();
	return 0;
}

size_t triangle_dataset::index_count()
{
	auto i = try_get_component<components::common::triangle_index>(0);
	if (i) return i->size();
	return 0;
}

} // namespace IWD::data
