#include "data/dataset_component_storage.h"

namespace IWD::data {

size_t dataset_component_storage::append_timestep(double time)
{
	if (!timesteps.empty() && time <= timesteps.back()) {
		return std::numeric_limits<size_t>::max();
	}

	auto entity = registry.create();
	entities.push_back(entity);
	timesteps.push_back(time);

	return entities.size() - 1ull;
}

bool dataset_component_storage::update_timestep(size_t timestep, double time)
{
	if (timestep > timesteps.size()) return false;

	timesteps[timestep] = time;
	return true;
}

void dataset_component_storage::clear_all_components(size_t timestep)
{
	assert(timestep < entities.size());
	registry.remove_all(entities[timestep]);
}

void dataset_component_storage::clear() { registry.clear(); }

} // namespace IWD::data