#include "data/triangle_dataset_processor.h"

#include "io/sms_2dm_reader.h"
#include "io/mike21_reader.h"
#include "io/geotiff_reader.h"
#include "io/3di_reader.h"
#include "io/obj_writer.h"
#include "io/stl_writer.h"
#include "io/gltf_writer.h"
#include "io/3d_tiles_writer.h"
#include "io/time_parser.h"

#include "util/logger.h"

using namespace IWD::util;

const std::string LOG_TAG = "{triangle_dataset_processor}";

namespace IWD::data {

IWD::exit_code triangle_dataset_processor::process(std::shared_ptr<job_processing_options> options)
{
	// create blank dataset
	auto dataset = std::make_shared<data::triangle_dataset>();

	// !important, generates first internal container for data
	dataset->append_timestep(0);

	// read input based on given data type
	switch (options->input_type) {
	case data::dataset_input_type::sms_2dm: {
		auto sms_job = std::static_pointer_cast<data::sms_2dm_processing_options>(options);
		if (!io::sms_2dm_reader::read(dataset, sms_job)) {
			log::error(LOG_TAG, "Error while reading input files.");
			return IWD::exit_code::read_error;
		}

		// reverse_triangle_winding(dataset->get_component<components::common::triangle_index>(0));
		break;
	}
	case data::dataset_input_type::mike21: {
		auto mike_job = std::static_pointer_cast<data::mike21_processing_options>(options);
		if (!io::mike21_reader::read(dataset, mike_job)) {
			log::error(LOG_TAG, "Error while reading input files.");
			return IWD::exit_code::read_error;
		}

		// reverse_triangle_winding(dataset->get_component<components::common::triangle_index>(0));
		break;
	}
	case data::dataset_input_type::geotiff: {
		auto geotiff_job = std::static_pointer_cast<data::geotiff_processing_options>(options);
		auto res = io::geotiff_reader::read(dataset, geotiff_job, options->epsg_input);
		if (!res) {
			log::error(LOG_TAG, "Error while reading input files.");
			return IWD::exit_code::read_error;
		}

		break;
	}
	case data::dataset_input_type::_3di: {
		auto _3di_job = std::static_pointer_cast<data::_3di_processing_options>(options);
		auto res = io::_3di_reader::read(dataset, _3di_job, options->epsg_input);
		if (!res) {
			log::error(LOG_TAG, "Error while reading input files.");
			return IWD::exit_code::read_error;
		}

		reverse_triangle_winding(dataset->get_component<components::common::triangle_index>(0));
		break;
	}
	default: {
		log::error(LOG_TAG, "Invalid or not implemented input type.");
		return IWD::exit_code::read_error;
		break;
	}
	}

	log::info(LOG_TAG, fmt::format("Processing {} timesteps.", dataset->get_timesteps().size()));

	// calculate first bbox
	dataset->emplace_or_replace_component<components::common::bounding_box_3D>(
	    calculate_bbox(dataset->get_component<triangle_dataset::position_3D>(0)), 0);

	std::vector<size_t> output_timesteps;
	// construct output timesteps
	if (options->output_timesteps.empty()) {
		output_timesteps.push_back(0);
	}
	else {
		io::time_parser::time_graph time_graph;
		for (size_t t = 0; t < dataset->get_timesteps().size(); ++t)
			time_graph.push_back({t, dataset->get_timesteps()[t]});
		output_timesteps = io::time_parser::parse(options->output_timesteps, time_graph);
	}

	// remove dry cells [check code of Unity Program]
	if (options->remove_dry_cells) {
		if (!remove_dry_cells(dataset)) return IWD::exit_code::processing_error;
	}

	// center or level
	// these are mutually exclusive as they serve different purposes (e.g. geo-referenced vs. local coordinates)
	if (options->epsg_output == 0) {
		if (options->center_data) {
			center_data(dataset->get_component<triangle_dataset::position_3D>(0), true);
			dataset->emplace_or_replace_component<components::common::bounding_box_3D>(
			    calculate_bbox(dataset->get_component<triangle_dataset::position_3D>(0)), 0);
		}
		if (options->level_data) {
			level_data(dataset->get_component<triangle_dataset::position_3D>(0));
			dataset->emplace_or_replace_component<components::common::bounding_box_3D>(
			    calculate_bbox(dataset->get_component<triangle_dataset::position_3D>(0)), 0);
		}
	}

	std::vector<dataset_lod> datasets_lod;

	// simplify data
	if (options->lod_levels != 0) {
		auto [valid, lods] = simplify_mesh(dataset, options->lod_levels);
		if (!valid) return IWD::exit_code::processing_error;

		datasets_lod = std::move(lods);

		// sort lods so that coarsest is first
		std::sort(datasets_lod.begin(), datasets_lod.end(),
		          [](const dataset_lod &a, const dataset_lod &b) { return a.lod < b.lod; });
	}
	else {
		datasets_lod.push_back({{dataset}, {0}});
	}

	// split data
	if (options->tile_size != 0.0) {
		for (auto &ds_lod : datasets_lod) {

			// only 1 dataset in vector now
			auto [valid, splits] = tile_dataset(ds_lod.datasets[0], options->tile_size, options->tile_epsg);
			if (!valid) return IWD::exit_code::processing_error;

			// clear original, free memory; erase original dataset
			ds_lod.datasets[0]->clear();
			ds_lod.datasets.clear();

			ds_lod.datasets = std::move(splits);
		}
	}

	if (options->epsg_output != 0) {
#if PROJ_ENABLED
		util::log::info("{dataset_processor}", fmt::format("Projecting from EPSG epsg_in: {} to epsg_out: {}",
		                                                   options->epsg_input, options->epsg_output));

		for (auto &ds_lod : datasets_lod) {
			for (auto &ds : ds_lod.datasets) {

				auto &position_0 = ds->get_component<triangle_dataset::position_3D>(0);
				triangle_dataset::position_3D pos_copy(position_0.size());
				std::copy(position_0.begin(), position_0.end(), pos_copy.begin());

				auto &position_original = ds->emplace_or_replace_component<triangle_dataset::position_2D>({}, 0);
				position_original.reserve(pos_copy.size());
				for (const auto &p : pos_copy) {
					position_original.push_back(glm::dvec2{p.x, p.z});
				}

				// IMPROVE: Baking may take a long time. Calculate normal and displace height after coordinate transform
				// bake y coordinate: y += depth before transforming as in projected space y-up != (0,1,0)
				for (size_t ts = 0; ts < ds->get_timesteps().size(); ++ts) {
					const auto &dep = ds->get_component<triangle_dataset::depth>(ts);

					auto &pos_ts = ds->emplace_or_replace_component<triangle_dataset::position_3D>({}, ts);
					pos_ts.resize(pos_copy.size());
					std::copy(pos_copy.begin(), pos_copy.end(), pos_ts.begin());

					for (size_t t = 0ull; t < pos_copy.size(); ++t) {
						pos_ts[t].y += static_cast<double>(dep[t].x);
					}

					if (!project_CRS_to_CRS(ds, pos_ts, options->epsg_input, options->epsg_output, ts))
						return IWD::exit_code::processing_error;

					ds->emplace_or_replace_component<components::common::bounding_box_3D>(
					    calculate_bbox(ds->get_component<triangle_dataset::position_3D>(ts)), ts);
				}
			}
		}

#elif
		log::warn(LOG_TAG, "Tried calling CRS transformation in a lib-iwd build without \"proj\" support.");
#endif
	}

	for (const auto &of : options->output_formats) {

		switch (of) {
		case dataset_output_type::obj: {
			// for (const auto timestep : output_timesteps) {
			for (auto &ds_lod : datasets_lod) {
				size_t tile_idx = 0; // assuming same amount of lod / tiles

				for (auto &ds : ds_lod.datasets) {
					auto ds_opt_tmp = (*std::const_pointer_cast<job_processing_options>(options).get());
					auto ds_opt = std::make_shared<job_processing_options>(ds_opt_tmp);

					ds_opt->output_basename += "-" + std::to_string(tile_idx) + fmt::format("_lod{}", ds_lod.lod)
					    /*+ "-T" + std::to_string(ds->get_timesteps()[timestep])*/;

					auto &pos = ds->get_component<triangle_dataset::position_3D>(0);
					auto pos_centered = triangle_dataset::position_3D();
					pos_centered.resize(pos.size());
					std::copy(pos.begin(), pos.end(), pos_centered.begin());
					// for smooth visualization require float-safe positions
					center_data(pos_centered, true);

					auto dep = ds->get_component<triangle_dataset::depth>(0 /*timestep*/);
					for (size_t t = 0ull; t < pos_centered.size(); ++t) {
						pos_centered[t].y += static_cast<double>(dep[t].x);
					}

					if (!io::obj_writer::write(pos_centered, ds->get_component<components::common::triangle_index>(0),
					                           ds_opt)) {
						log::error(LOG_TAG, "Error while processing Wavefront obj.");
						return IWD::exit_code::processing_error;
					}
					++tile_idx;
				}
			}
			//} // timesteps

			break;
		}
		case dataset_output_type::stl: {

			for (auto &ds_lod : datasets_lod) {
				size_t tile_idx = 0; // assuming same amount of lod / tiles

				for (auto &ds : ds_lod.datasets) {
					auto ds_opt_tmp = (*std::const_pointer_cast<job_processing_options>(options).get());
					auto ds_opt = std::make_shared<job_processing_options>(ds_opt_tmp);

					ds_opt->output_basename += "-" + std::to_string(tile_idx) + fmt::format("_lod{}", ds_lod.lod);

					auto &pos = ds->get_component<triangle_dataset::position_3D>(0);
					auto pos_centered = triangle_dataset::position_3D();
					pos_centered.resize(pos.size());
					std::copy(pos.begin(), pos.end(), pos_centered.begin());
					// for smooth visualization require float-safe positions
					center_data(pos_centered, true);

					auto dep = ds->get_component<triangle_dataset::depth>(0 /*timestep*/);
					for (size_t t = 0ull; t < pos_centered.size(); ++t) {
						pos_centered[t].y += static_cast<double>(dep[t].x);
					}

					if (!io::stl_writer::write(pos_centered, ds->get_component<components::common::triangle_index>(0),
					                           ds_opt)) {
						log::error(LOG_TAG, "Error while processing stl.");
						return IWD::exit_code::processing_error;
					}

					++tile_idx;
				}
			}
			break;
		}
		case dataset_output_type::gltf: {
			for (const auto timestep : output_timesteps) {
				for (auto &ds_lod : datasets_lod) {
					size_t tile_idx = 0; // assuming same amount of lod / tiles

					for (auto &ds : ds_lod.datasets) {
						auto ds_opt_tmp = (*std::const_pointer_cast<job_processing_options>(options).get());
						auto ds_opt = std::make_shared<job_processing_options>(ds_opt_tmp);

						ds_opt->output_basename += "-" + std::to_string(tile_idx) + fmt::format("_lod{}", ds_lod.lod) +
						                           "-T" + std::to_string(ds->get_timesteps()[timestep]);

						// linearize data
						{
							// for smooth visualization require float-safe positions
							center_data(ds->get_component<triangle_dataset::position_2D>(0));

							// IMPROVE: may handle glTF without epsg transform in future
							if (options->epsg_output == 0) {
								log::warn(LOG_TAG, "Cannot linearize data with epsg '0' binary data!");
								break;
							}

							auto lin_data = linearize_data(ds, timestep);
							if (lin_data.empty()) {
								log::error(LOG_TAG, "An error ocurred while linearizing binary data!");
								return IWD::exit_code::processing_error;
							}

							ds->emplace_or_replace_component<components::common::linearized_binary>(lin_data, 0);
						}

						// calc min/max scalars (veloc. , height) (e.g. for glTF transfer functions)
						auto [depth_min, depth_max] =
						    calculate_minmax(ds->get_component<triangle_dataset::depth>(timestep));
						auto [velocity_min, velocity_max] =
						    calculate_minmax(ds->get_component<triangle_dataset::velocity_2D>(timestep));

						auto bbox_original = components::common::bounding_box_2D();
						bbox_original.calculate(ds->get_component<triangle_dataset::position_2D>(0));

						io::gltf_writer::processing_params params;
						params.bbox = ds->get_component<components::common::bounding_box_3D>(timestep);
						params.bbox_original = bbox_original;
						params.depth_min = depth_min;
						params.depth_max = depth_max;
						params.velocity_min = velocity_min;
						params.velocity_max = velocity_max;

						if (!io::gltf_writer::write(std::static_pointer_cast<data::dataset>(ds), ds_opt, params)) {
							log::error(LOG_TAG, "Error while processing glTF.");
							return IWD::exit_code::processing_error;
						}

						++tile_idx;
					}
				}
			}
			break;
		}
		case dataset_output_type::_3d_tiles: {
			if (!datasets_lod[0].datasets[0]->try_get_component<components::rtc_info>(0)) {
				log::warn(LOG_TAG, "Cannot use 3D Tiles without 'rtc_info' !");
				break;
			}

			std::vector<io::_3d_tiles_writer::tile_info> tile_info;

			for (const auto timestep : output_timesteps) {
				for (auto &ds_lod : datasets_lod) {
					size_t tile_idx = 0; // assuming same amount of lod / tiles

					for (auto &ds : ds_lod.datasets) {
						auto ds_opt_tmp = (*std::const_pointer_cast<job_processing_options>(options).get());
						auto ds_opt = std::make_shared<job_processing_options>(ds_opt_tmp);

						ds_opt->output_basename += "-" + std::to_string(tile_idx) + fmt::format("_lod{}", ds_lod.lod) +
						                           "-T" + std::to_string(ds->get_timesteps()[timestep]);

						// linearize data
						{
							// for smooth visualization require float-safe positions
							center_data(ds->get_component<triangle_dataset::position_2D>(0));

							// IMPROVE: may handle glTF without epsg transform in future
							if (options->epsg_output == 0) {
								log::warn(LOG_TAG, "Cannot linearize data with epsg '0' binary data!");
								break;
							}

							auto lin_data = std::move(linearize_data(ds, timestep));
							if (lin_data.empty()) {
								log::error(LOG_TAG, "An error ocurred while linearizing binary data!");
								return IWD::exit_code::processing_error;
							}

							// keep memory footprint small, overwrite timestep 0
							ds->emplace_or_replace_component<components::common::linearized_binary>(lin_data, 0);
						}

						// calc min/max scalars (veloc. , height) (e.g. for glTF transfer functions)
						auto [depth_min, depth_max] =
						    calculate_minmax(ds->get_component<triangle_dataset::depth>(timestep));
						auto [velocity_min, velocity_max] =
						    calculate_minmax(ds->get_component<triangle_dataset::velocity_2D>(timestep));

						auto bbox = ds->get_component<components::common::bounding_box_3D>(timestep);
						auto bbox_original = components::common::bounding_box_2D();
						bbox_original.calculate(ds->get_component<triangle_dataset::position_2D>(0));

						io::gltf_writer::processing_params params;

						params.bbox = bbox;
						params.bbox_original = bbox_original;
						params.depth_min = depth_min;
						params.depth_max = depth_max;
						params.velocity_min = velocity_min;
						params.velocity_max = velocity_max;

						auto b3dm_bin = std::move(io::_3d_tiles_writer::build_b3dm_header_binary());

						std::ofstream file;
						auto of_name = ds_opt->output_directory / (ds_opt->output_basename.u8string() + ".b3dm");

						if (file.open(of_name, std::ios::binary); file.is_open()) {
							file.write(reinterpret_cast<const char *>(b3dm_bin.data()), b3dm_bin.size());
							file.close();
						}
						else {
							log::error(LOG_TAG, fmt::format("Could not open file for writing: {}", of_name.u8string()));
							return IWD::exit_code::processing_error;
						}

						// enforce glb & 3d tile
						ds_opt->gltf_glb = true;
						params.is_3d_tile = true;

						// append binary gltf to .b3dm file
						if (!io::gltf_writer::write(std::static_pointer_cast<data::dataset>(ds), ds_opt, params,
						                            true)) {
							log::error(LOG_TAG, "Error while processing binary glTF.");
							return IWD::exit_code::processing_error;
						}

						// collect data for tileset.jon
						auto &lat_long = ds->get_component<components::common::lat_long>(0);
						auto bbox_lat_long = components::common::bounding_box_2D();
						bbox_lat_long.calculate(lat_long);

						io::_3d_tiles_writer::tile_info ti = {
						    bbox,       bbox_lat_long,       ds_opt->output_basename.u8string() + ".b3dm",
						    ds_lod.lod, ds_opt->epsg_output, ds_opt->epsg_input, {}};

						if (ds_lod.lod == 0) {
							tile_info.push_back(ti);
						}
						else {
							auto s_ti = std::make_shared<io::_3d_tiles_writer::tile_info>(std::move(ti));
							auto parent_tile = &tile_info[tile_idx];
							// keep going deeper
							while (parent_tile->higher_lod_tile) {
								parent_tile = parent_tile->higher_lod_tile.get();
							}
							parent_tile->higher_lod_tile = s_ti;
						}

						++tile_idx;
					}
				}

				auto &rtc = datasets_lod[0].datasets[0]->get_component<components::rtc_info>(0);

				if (!io::_3d_tiles_writer::write(
				        tile_info, rtc,
				        options->output_directory /
				            (options->output_basename.u8string() + "-T" +
				             std::to_string(datasets_lod[0].datasets[0]->get_timesteps()[timestep]) +
				             "_tileset.json"))) {
					log::error(LOG_TAG, "Error while processing 3D Tile tileset.");
					return IWD::exit_code::processing_error;
				}
			}
			break;
		}
		default: {
			log::error(LOG_TAG, "Invalid or unknown output type.");
			return IWD::exit_code::processing_error;

			break;
		}
		}
	}

	return IWD::exit_code::normal;
}

} // namespace IWD::data