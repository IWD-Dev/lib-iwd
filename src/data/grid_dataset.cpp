#include "data/grid_dataset.h"

namespace IWD::data {

dataset::type grid_dataset::get_type() noexcept { return dataset::type::grid; }

size_t grid_dataset::vertex_count()
{
	auto p = try_get_component<grid_dataset::position_3D>(0);
	if (p) return p->get_data().size();
	return 0;
}

size_t grid_dataset::index_count()
{
	auto i = try_get_component<components::common::triangle_index>(0);
	if (i) return i->size();
	return 0;
}

} // namespace IWD::data
