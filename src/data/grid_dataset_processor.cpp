#include "data/grid_dataset_processor.h"

namespace IWD::data {

IWD::exit_code grid_dataset_processor::process([[maybe_unused]] std::shared_ptr<job_processing_options> options)
{
	return IWD::exit_code::normal;
}

std::tuple<bool, std::vector<std::shared_ptr<grid_dataset>>>
grid_dataset_processor::tile_dataset(std::shared_ptr<grid_dataset> dataset, [[maybe_unused]] double tile_size,
                                     [[maybe_unused]] uint32_t tile_epsg)
{
	std::vector<decltype(dataset)> datasets;
	return std::make_tuple(true, datasets);
}

std::vector<std::byte> grid_dataset_processor::linearize_data([[maybe_unused]] std::shared_ptr<grid_dataset> dataset)
{
	return std::vector<std::byte>();
}

} // namespace IWD::data