#include "data/triangle_dataset_processor.h"

#include "quadtree/Quadtree.h"
#include "simplify/Simplify.h"

#include "util/logger.h"
#include "util/algorithm.h"
#include "util/triangle_operations.h"

using namespace IWD::util;

const std::string LOG_TAG = "{triangle_dataset_processor}";

namespace IWD::data {

// #TODO: proper casting of indices
#ifdef __linux__
#pragma GCC diagnostic ignored "-Wsign-conversion"
#endif

std::tuple<bool, std::vector<std::shared_ptr<triangle_dataset>>>
triangle_dataset_processor::tile_dataset(std::shared_ptr<triangle_dataset> dataset, double tile_size,
                                         [[maybe_unused]] uint32_t tile_epsg)
{
	log::info(LOG_TAG, "Tiling dataset ...");

#ifndef PROJ_ENABLED
	if (tile_epsg != 0u) {
		log::error(LOG_TAG, "Can't split a dataset with a sepecific \"tile_epsg\", as lib-iwd was compiled without "
		                    "\"proj\" support.");
		return {false, {}};
	}
#endif // !PROJ_ENABLED

	if (!dataset->has_component<triangle_dataset::position_3D>(0)) {
		log::error(LOG_TAG, "Dataset has no \"triangle_dataset::position_3D\" component.");
		return {false, {}};
	}
	if (!dataset->has_component<components::common::triangle_index>(0)) {
		log::error(LOG_TAG, "Dataset has no \"common::triangle_index\" component.");
		return {false, {}};
	}

	std::vector<decltype(dataset)> datasets;

	// SETUP TILES
	{
		dataset->emplace_or_replace_component<components::common::bounding_box_3D>(
		    calculate_bbox(dataset->get_component<triangle_dataset::position_3D>(0)), 0);
		auto &bbox_all = dataset->get_component<components::common::bounding_box_3D>(0);

		// calculate tile sizes and get initial bbox
		auto min_x = util::floor_to_multiple(bbox_all.min.x, tile_size);
		auto max_x = util::ceil_to_multiple(bbox_all.max.x, tile_size);
		auto min_z = util::floor_to_multiple(bbox_all.min.z, tile_size);
		auto max_z = util::ceil_to_multiple(bbox_all.max.z, tile_size);

		// auto x_dim = std::abs((max_x - min_x) / tile_size);
		// auto z_dim = std::abs((max_z - min_z) / tile_size);

		for (auto z = min_z; z < max_z; z += tile_size) {
			for (auto x = min_x; x < max_x; x += tile_size) {
				auto bbox = components::common::bounding_box_3D();
				bbox.min = glm::dvec3(x, 0.0, z);
				bbox.max = glm::dvec3(x + tile_size, 0.0, z + tile_size);

				auto ds = std::make_shared<data::triangle_dataset>();

				// ds has no timesteps & data_component_storage yet, copy from base
				for (auto ts : dataset->get_timesteps()) {
					ds->append_timestep(ts);
				}

				ds->emplace_component<components::common::bounding_box_3D>(bbox, 0);
				datasets.push_back(ds);
			}
		}
	}

	auto &positions_all = dataset->get_component<triangle_dataset::position_3D>(0);
	auto &indices_all = dataset->get_component<components::common::triangle_index>(0);

	positions_all.reserve(static_cast<size_t>(static_cast<double>(positions_all.size()) * 1.3));
	indices_all.reserve(static_cast<size_t>(static_cast<double>(indices_all.size()) * 1.3));

	// auto last_index_before_tiling = static_cast<int32_t>(positions_all.size()) - 1;
	const auto &step_count = dataset->get_timesteps().size();

	// TRIANGLE SPLITTING
	{
		components::common::triangle_index new_indices;
		new_indices.reserve(static_cast<size_t>(static_cast<double>(indices_all.size()) * 1.3)); // estimate

		for (const auto &tri : indices_all) {
			data::components::common::bounding_box_3D tr_box;
			tr_box.calculate(std::vector<components::position_t<3, double>>{positions_all[tri.x], positions_all[tri.y],
			                                                                positions_all[tri.z]});

			util::triangle::triangle_t<double> u_tri{{positions_all[tri.x], positions_all[tri.y], positions_all[tri.z]},
			                                         tri};
			auto last_index = static_cast<int32_t>(positions_all.size()) - 1;

			auto ret = util::triangle::split_triangle<double>(u_tri, tile_size, last_index);

			// no split
			if (ret.size() == 1) {
				new_indices.push_back(tri);
				continue;
			}
			else {
				auto old_last_index = static_cast<int32_t>(positions_all.size() - 1);
				std::set<int32_t> new_idx;

				for (const auto& tmp_tri : ret) {
					for (int32_t i = 0; i < 3; ++i) {
						// new index/vertex (higher id than highest before split + not added yet)
						if (tmp_tri.i[i] > old_last_index && new_idx.find(tmp_tri.i[i]) == new_idx.end()) {
							new_idx.insert(tmp_tri.i[i]);

							// keep computed position
							positions_all.push_back(tmp_tri.v[i]);

							// calculate barycentric coord for value interpolation
							auto bary_coordsd = util::triangle::calculate_barycentric_coordinates(u_tri, tmp_tri.v[i]);

							if (auto pos2d = dataset->try_get_component<triangle_dataset::position_2D>(0)) {
								auto v = util::triangle::interpolate_barycentric(bary_coordsd, (*pos2d)[tri.x],
								                                                 (*pos2d)[tri.y], (*pos2d)[tri.z]);
								pos2d->push_back(v);
							}

							if (auto lat_long = dataset->try_get_component<data::components::common::lat_long>(0)) {
								auto l = util::triangle::interpolate_barycentric(
								    bary_coordsd, (*lat_long)[tri.x], (*lat_long)[tri.y], (*lat_long)[tri.z]);
								lat_long->push_back(l);
							}

							// glm won't mix float and double for vector multiplications, narrow to fvec3
							glm::fvec3 bary_coords = bary_coordsd;

							// interpolate new values for all timesteps
							for (auto j = 0ull; j < step_count; ++j) {
								if (auto depth = dataset->try_get_component<triangle_dataset::depth>(j)) {
									auto d = util::triangle::interpolate_barycentric(bary_coords, (*depth)[tri.x],
									                                                 (*depth)[tri.y], (*depth)[tri.z]);
									depth->push_back(d);
								}
								if (auto height = dataset->try_get_component<triangle_dataset::height>(j)) {
									auto h = util::triangle::interpolate_barycentric(
									    bary_coords, (*height)[tri.x], (*height)[tri.y], (*height)[tri.z]);
									height->push_back(h);
								}
								if (auto velocity = dataset->try_get_component<triangle_dataset::velocity_2D>(j)) {
									auto v = util::triangle::interpolate_barycentric(
									    bary_coords, (*velocity)[tri.x], (*velocity)[tri.y], (*velocity)[tri.z]);
									velocity->push_back(v);
								}
							}
						}
					}
					new_indices.push_back(tmp_tri.i);
				}
			}
		}
		positions_all.shrink_to_fit();
		new_indices.shrink_to_fit();
		indices_all = std::move(new_indices);
	}

	// sort triangles into tiles, building index
	for (auto &ds : datasets) {
		auto &bbox_curr = ds->get_component<components::common::bounding_box_3D>(0);

		auto &indices = ds->emplace_component<components::common::triangle_index>({}, 0);
		auto &new_to_old = ds->emplace_component(components::common::index_map{}, 0);

		components::common::index_map old_to_new; // somewhat like a bimap - but with double memory usage
		int32_t new_idx = 0;

		for (const auto &tri : indices_all) {
			components::position_t<3, double> center =
			    (positions_all[tri[0]] + positions_all[tri[1]] + positions_all[tri[2]]) / 3.0;

			// found corresponding triangle
			if (bbox_curr.contains_2d(center)) {
				components::index_t<3> tmp_idx;

				for (auto i = 0; i < 3; ++i) {
					auto id = old_to_new.find(tri[i]);

					if (id == old_to_new.end()) {
						new_to_old.insert({new_idx, tri[i]});
						old_to_new.insert({tri[i], new_idx});

						tmp_idx[i] = new_idx;
						++new_idx; // be explicit
					}
					else {
						tmp_idx[i] = id->second;
					}
				}
				indices.push_back(tmp_idx);
			}
		}
	}

#ifdef IWD_DEBUG
	// validate all triangles are mapped into tiles
	{
		size_t amt = 0;
		for (auto &ds : datasets) {
			auto &indices = ds->get_component<components::common::triangle_index>(0);
			amt += indices.size();
		}
		assert(amt == indices_all.size());
		if (!(amt == indices_all.size())) {
			log::warn(LOG_TAG, "Not all triangles could be mapped into tiles.");
		}
	}
#endif

	datasets.erase(
		// g++ gets confused on const auto &ds, so explicit
	    std::remove_if(datasets.begin(), datasets.end(), 
			[](const std::shared_ptr<triangle_dataset> &ds) { return ds->get_component<components::common::triangle_index>(0).empty();}),
		datasets.end());

	for (auto &ds : datasets) {
		ds->copy_component_index_mapped<triangle_dataset::position_3D>(
		    dataset, ds->get_component<components::common::index_map>(0), 0);

		ds->emplace_or_replace_component<components::common::bounding_box_3D>(
		    calculate_bbox(ds->get_component<triangle_dataset::position_3D>(0)), 0);

		if (dataset->has_component<triangle_dataset::position_2D>(0)) {
			ds->copy_component_index_mapped<triangle_dataset::position_2D>(
			    dataset, ds->get_component<components::common::index_map>(0), 0);
		}

		if (dataset->has_component<components::common::lat_long>(0)) {
			ds->copy_component_index_mapped<components::common::lat_long>(
			    dataset, ds->get_component<components::common::index_map>(0), 0);
		}

		if (dataset->has_component<components::rtc_info>(0)) {
			ds->emplace_or_replace_component<components::rtc_info>(dataset->get_component<components::rtc_info>(0), 0);
		}

		for (auto i = 0ull; i < step_count; ++i) {
			// TODO: think about copying all data manually --> ds->copy_all_components(dataset);
			if (dataset->has_component<triangle_dataset::depth>(i)) {
				ds->copy_component_index_mapped<triangle_dataset::depth>(
				    dataset, ds->get_component<components::common::index_map>(0), i);
				// if (intermediate_cleaning) dataset->clear_component<triangle_dataset::depth>(i);
			}

			if (dataset->has_component<triangle_dataset::height>(i)) {
				ds->copy_component_index_mapped<triangle_dataset::height>(
				    dataset, ds->get_component<components::common::index_map>(0), i);
				// if (intermediate_cleaning) dataset->clear_component<triangle_dataset::height>(i);
			}

			if (dataset->has_component<triangle_dataset::velocity_2D>(i)) {
				ds->copy_component_index_mapped<triangle_dataset::velocity_2D>(
				    dataset, ds->get_component<components::common::index_map>(0), i);
				// if (intermediate_cleaning) dataset->clear_component<triangle_dataset::velocity_2D>(i);
			}
		}
	}

	return std::make_tuple(true, datasets);
}

bool triangle_dataset_processor::remove_dry_cells(std::shared_ptr<triangle_dataset> dataset, double dry_threshold)
{
	if (!dataset->has_component<triangle_dataset::position_3D>(0)) {
		log::error(LOG_TAG, "Dataset has no \"triangle_dataset::position_3D\" component.");
		return false;
	}
	if (!dataset->has_component<components::common::triangle_index>(0)) {
		log::error(LOG_TAG, "Dataset has no \"common::triangle_index\" component.");
		return false;
	}

	log::info(LOG_TAG, "Removing dry cells ...");

	auto &positions = dataset->get_component<triangle_dataset::position_3D>(0);
	auto &indices = dataset->get_component<components::common::triangle_index>(0);

	enum cell_state { initial, removed, used };

	struct triangle;

	struct vertex {
		std::vector<uint32_t> triangles;
		uint32_t id;
		uint32_t id_old;
		cell_state state;
		bool is_dry;
	};

	// store offsets into vector
	struct triangle {
		uint32_t v0;
		uint32_t v1;
		uint32_t v2;
	};

	std::vector<vertex> tmp_vertex(positions.size());
	std::vector<triangle> tmp_triangle(indices.size());

	auto is_dry = [&](const uint32_t id) -> bool {
		for (auto t = 0ull; t < dataset->get_timesteps().size(); ++t) {
			if (!dataset->has_component<triangle_dataset::depth>(t)) {
				log::warn(LOG_TAG,
				          fmt::format("Dataset has no \"triangle_dataset::depth\" component at timestep {0}.", t));
			}
			else { // can return after first "wet" state was found
				if (static_cast<double>(dataset->get_component<triangle_dataset::depth>(t)[id].x) > dry_threshold) return false;
			}
		}
		return true;
	};

	// build lookup-structures
	for (auto i = 0u; i < positions.size(); ++i) {
		tmp_vertex[i] = {{}, i, i, cell_state::initial, is_dry(i)};
	}
	for (auto i = 0u; i < indices.size(); ++i) {
		const auto &tri = indices[i];
		tmp_triangle[i] = {static_cast<uint32_t>(tri.x), static_cast<uint32_t>(tri.y), static_cast<uint32_t>(tri.z)};
		tmp_vertex[tri.x].triangles.push_back(i);
		tmp_vertex[tri.y].triangles.push_back(i);
		tmp_vertex[tri.z].triangles.push_back(i);
	}

	// mark vertices to remove
	for (const auto &tri : indices) {
		auto &v0 = tmp_vertex[tri.x];
		auto &v1 = tmp_vertex[tri.y];
		auto &v2 = tmp_vertex[tri.z];

		if (v0.is_dry && v1.is_dry && v2.is_dry) {
			if (v0.state != cell_state::used) v0.state = cell_state::removed;
			if (v1.state != cell_state::used) v1.state = cell_state::removed;
			if (v2.state != cell_state::used) v2.state = cell_state::removed;
		}
		else {
			v0.state = cell_state::used;
			v1.state = cell_state::used;
			v2.state = cell_state::used;
		}
	}

	// calculate new ids
	int offset = 0;

	for (auto &v : tmp_vertex) {
		if (v.state == cell_state::removed) {
			++offset;
			//v.id = -1; // CHECK IF REMOVABLE
		}
		else {
			v.id -= offset;
		}
	}

	{
		auto copy_data = [&](auto &container) {
			std::remove_reference_t<decltype(container)> new_container;
			new_container.reserve(container.size());

			for (auto v : tmp_vertex) {
				if (v.state != cell_state::removed) {
					new_container.push_back(container[v.id_old]);
				}
			}

			new_container.shrink_to_fit();
			container = std::move(new_container);
		};

		// IMPROVE: Type-List
		copy_data(positions);
		copy_data(dataset->get_component<triangle_dataset::depth>(0));
		copy_data(dataset->get_component<triangle_dataset::velocity_2D>(0));
	}

	{
		components::common::triangle_index new_indices;
		new_indices.reserve(tmp_triangle.size());

		for (const auto &t : tmp_triangle) {
			if (!(tmp_vertex[t.v0].is_dry && tmp_vertex[t.v1].is_dry &&
			      tmp_vertex[t.v2].is_dry)) { // at least one wet cell
				new_indices.push_back(glm::ivec3{tmp_vertex[t.v0].id, tmp_vertex[t.v1].id, tmp_vertex[t.v2].id});
			}
		}
		new_indices.shrink_to_fit();
		indices = std::move(new_indices);
	}

	return true;
}

std::tuple<bool, std::vector<triangle_dataset_processor::dataset_lod>>
triangle_dataset_processor::simplify_mesh(std::shared_ptr<triangle_dataset> dataset, uint32_t lod_levels)
{
	if (!dataset->has_component<triangle_dataset::position_3D>(0)) {
		log::error(LOG_TAG, "Dataset does not have component <triangle_dataset::position_3D(0)");
		return {false, {}};
	}
	if (!dataset->has_component<components::common::triangle_index>(0)) {
		log::error(LOG_TAG, "Dataset does not have component components::common::triangle_index>(0)");
		return {false, {}};
	}

	log::info(LOG_TAG, "Simplifying data.");

	std::vector<triangle_dataset_processor::dataset_lod> ds_lods;
	auto &positions = dataset->get_component<triangle_dataset::position_3D>(0);
	auto &indices = dataset->get_component<components::common::triangle_index>(0);
	double target_tri_count = static_cast<double>(indices.size());

	// precalculate quadtree
	data::components::common::bounding_box_3D bbox;
	bbox.calculate(positions);
	// extend bounds by X so objects on edges can have boxes without degeneration
	// asymetrical so that exactly centered nodes are sorted either west or east, north or south
	bbox.min -= glm::dvec3{1.0, 0.0, 1.0};
	bbox.max += glm::dvec3{2.0, 0.0, 2.0};

	auto box = quadtree::Box<double>({bbox.min.x, bbox.min.z}, {bbox.max.x - bbox.min.x, bbox.max.z - bbox.min.z});

	struct triangle_box {
		const components::index_t<3> *t;
		quadtree::Box<double> box;
	};

	auto get_box = [&](const triangle_box *t) { return t->box; };

	// storable amount of points := (threshold * 4^depth)
	// for thresh = fix32 -> depth = ceil(log(x) / (2 * log(2)))
	int32_t threshold = 32;
	int32_t max_depth = static_cast<int32_t>(
	    std::ceil(std::log(static_cast<double>(Simplify::vertices.size()) / 
			static_cast<double>(threshold)) / (2.0 * std::log(2.0))));

	auto qt = quadtree::Quadtree<const triangle_box *, decltype(get_box), std::equal_to<const triangle_box *>, double>(
	    threshold, max_depth + 2 /*safety*/, box, get_box);

	// MUST NOT ALTER scatter UNTIL REFERENCES ARE NOT NEEDED ANYMORE AFTER THIS,
	// otherwise references may be invalidated
	std::vector<triangle_box> qtri_box;
	qtri_box.reserve(indices.size());

	for (const auto &i : indices) {
		glm::dvec3 min{std::numeric_limits<double>::max()}, max{-std::numeric_limits<double>::max()};
		for (int j = 0; j < 3; ++j) {
			min = glm::min(min, positions[i[j]]);
			max = glm::max(max, positions[i[j]]);
		}

		auto qbox = quadtree::Box<double>(min.x, min.z, std::abs(max.x - min.x), std::abs(min.z - max.z));
		qtri_box.push_back({&i, qbox});
	}

	for (auto &tb : qtri_box) {
		qt.add(&tb);
	}

	for (auto lv = 0u; lv < lod_levels; ++lv) {
		// keep original lod as highest
		if (lv == 0) {
			ds_lods.push_back({{dataset}, (lod_levels - 1) - lv}); // highest lod_level first
			target_tri_count *= 0.5;
		}
		else {
			// construct data
			std::vector<Simplify::Vertex> s_vert;
			std::vector<Simplify::Triangle> s_tri;
			s_vert.reserve(positions.size());
			s_tri.reserve(indices.size());

			for (size_t i = 0; i < positions.size(); ++i) {
				Simplify::Vertex v;
				v.p = Simplify::vec3f{positions[i].x, positions[i].y, positions[i].z};
				v.id = static_cast<int>(i);
				s_vert.push_back(v);
			}
			for (size_t i = 0; i < indices.size(); ++i) {
				Simplify::Triangle t;
				t.v[0] = indices[i].x;
				t.v[1] = indices[i].y;
				t.v[2] = indices[i].z;
				s_tri.push_back(t);
			}

			Simplify::move_set_data(std::move(s_vert), std::move(s_tri));

			// run simplification
			Simplify::simplify_mesh(static_cast<int>(target_tri_count), 7.0, false);

			// construct reduced dataset with attributes
			auto simplified_ds = std::make_shared<triangle_dataset>();

			for (auto ts : dataset->get_timesteps()) {
				simplified_ds->append_timestep(ts);
			}

			// can directly use reduced mesh
			auto &simple_positions = simplified_ds->emplace_or_replace_component<triangle_dataset::position_3D>({}, 0);
			simple_positions.reserve(Simplify::vertices.size());
			for (const auto &v : Simplify::vertices) {
				simple_positions.push_back(glm::dvec3(v.p.x, v.p.y, v.p.z));
			}

			// can directly use reduced mesh
			auto &simple_indices =
			    simplified_ds->emplace_or_replace_component<components::common::triangle_index>({}, 0);
			simple_indices.reserve(Simplify::triangles.size());
			for (const auto &t : Simplify::triangles) {
				simple_indices.push_back(glm::ivec3(t.v[0], t.v[1], t.v[2]));
			}

			// can copy original bounding box as border is preserved
			simplified_ds->emplace_or_replace_component<components::common::bounding_box_3D>(
			    dataset->get_component<components::common::bounding_box_3D>(0), 0);

			// cleanup temporary memory
			Simplify::clear_data();

			// use quadtree to interpolate attributes
			bool extrapolated = false;
			for (auto ts = 0ull; ts < dataset->get_timesteps().size(); ++ts) {

				auto &og_depth = dataset->get_component<triangle_dataset::depth>(ts);
				auto &og_veloc = dataset->get_component<triangle_dataset::velocity_2D>(ts);

				auto &simple_depth = simplified_ds->emplace_or_replace_component<triangle_dataset::depth>({}, ts);
				simple_depth.reserve(simple_positions.size());

				auto &simple_veloc = simplified_ds->emplace_or_replace_component<triangle_dataset::velocity_2D>({}, ts);
				simple_veloc.reserve(simple_positions.size());

				for (const auto &p : simple_positions) {
					glm::dvec3 bary_coordsd{};
					glm::fvec3 bary_coordsf{};
					components::index_t<3> *tri = nullptr;

					auto elems = qt.query({{p.x, p.z}, 0.01});

					if (elems.size() == 0) {
						log::error(LOG_TAG, "LoD Generation: Couldn't find matching triangle.");
						return {false, ds_lods};
					}
					else {
						for (auto &e : elems) {
							util::triangle::triangle_t<double> u_tri{
							    {positions[e->t->x], positions[e->t->y], positions[e->t->z]}, *(e->t)};
							bary_coordsd = util::triangle::calculate_barycentric_coordinates(u_tri, p);
							bary_coordsf = bary_coordsd; // reduce precision, required for template function types

							if ((bary_coordsd.x >= 0 && bary_coordsd.x <= 1.0) &&
							    (bary_coordsd.y >= 0 && bary_coordsd.y <= 1.0) &&
							    (bary_coordsd.z >= 0 && bary_coordsd.z <= 1.0)) {

								tri = const_cast<components::index_t<3> *>(e->t);
								break;
							}
							else {
								extrapolated = true;
								tri = const_cast<components::index_t<3> *>(e->t); // for extrapolation
							}
						}
					}

					simple_depth.push_back(util::triangle::interpolate_barycentric(bary_coordsf, og_depth[tri->x],
					                                                               og_depth[tri->y], og_depth[tri->z]));
					simple_veloc.push_back(util::triangle::interpolate_barycentric(bary_coordsf, og_veloc[tri->x],
					                                                               og_veloc[tri->y], og_veloc[tri->z]));
				}
			}
			if (extrapolated) {
				log::warn(LOG_TAG, "LoD Generation: Extrapolated some values instead of interpolating. This may happen "
				                   "in models with inner holes.");
			}

			ds_lods.push_back({{simplified_ds}, (lod_levels - 1) - lv});
			target_tri_count *= 0.5;
		}
	}
	return {true, ds_lods};
}

// TODO: this is no general binary, but specific for glTF. Move to glTF
std::vector<std::byte> triangle_dataset_processor::linearize_data(std::shared_ptr<triangle_dataset> dataset,
                                                                  size_t timestep)
{
	/*auto b1 = dataset->has_component<triangle_dataset::position_3D>(0);
	auto b2 = dataset->has_component<components::common::triangle_index>(0);
	auto b3 =dataset->has_component<triangle_dataset::depth>(timestep);
	auto b4 =dataset->has_component<triangle_dataset::velocity_2D>(timestep);
	auto b5 =dataset->has_component<triangle_dataset::position_2D>(0);*/

	// IMPROVE: could rely on the underlying all_of<T>
	if (!(dataset->has_component<triangle_dataset::position_3D>(0) &&
	      dataset->has_component<components::common::triangle_index>(0) &&
	      dataset->has_component<triangle_dataset::depth>(timestep) &&
	      dataset->has_component<triangle_dataset::velocity_2D>(timestep) &&
	      dataset->has_component<triangle_dataset::position_2D>(0))) {

		log::error(LOG_TAG, "Dataset has not all required components to linearize.");
		return {};
	}

	auto &pos = dataset->get_component<triangle_dataset::position_3D>(timestep);
	auto &poso = dataset->get_component<triangle_dataset::position_2D>(0);

	auto &depth = dataset->get_component<triangle_dataset::depth>(timestep);
	auto &velocity = dataset->get_component<triangle_dataset::velocity_2D>(timestep);
	auto &indices = dataset->get_component<components::common::triangle_index>(0);

	// must transform double into float for GPU
	// binary size according to float, not double! (dvec -> fvec)
	size_t pos_size = pos.size() * sizeof(glm::fvec3);
	size_t poso_size = poso.size() * sizeof(glm::fvec2);
	size_t depth_size = depth.size() * sizeof(glm::fvec1);
	size_t velocity_size = velocity.size() * sizeof(glm::fvec2);
	size_t indices_size = indices.size() * sizeof(glm::tvec3<int32_t>);

	size_t offset = 0ull;
	std::vector<std::byte> bytes;
	bytes.resize(pos_size + poso_size + depth_size + velocity_size /*+ batchid_size*/ + indices_size);

	// only for vertex attributes
	data::components::common::linearized_binary_offsets lin_offs;

	// position
	{
		std::vector<glm::fvec3> fpos;
		fpos.reserve(pos.size());
		std::transform(std::begin(pos), std::end(pos), std::back_inserter(fpos), [&](const auto &val) { return val; });

		memcpy(bytes.data(), fpos.data(), pos_size);

		lin_offs.push_back({pos_size, offset});
		offset += pos_size;
	}

	// position original
	{
		std::vector<glm::fvec2> fposo;
		fposo.reserve(poso.size());

		std::transform(std::begin(poso), std::end(poso), std::back_inserter(fposo),
		               [&](const auto &val) { return val; });

		memcpy(bytes.data() + offset, fposo.data(), poso_size);

		lin_offs.push_back({poso_size, offset});
		offset += poso_size;
	}

	// depth
	{
		memcpy(bytes.data() + offset, depth.data(), depth_size);

		lin_offs.push_back({depth_size, offset});
		offset += depth_size;
	}

	// velocity
	{
		memcpy(bytes.data() + offset, velocity.data(), velocity_size);

		lin_offs.push_back({velocity_size, offset});
		offset += velocity_size;
	}

	// index
	{
		memcpy(bytes.data() + offset, indices.data(), indices_size);

		offset += indices_size;
	}

	dataset->emplace_or_replace_component<data::components::common::linearized_binary_offsets>(lin_offs, 0);

	return bytes;
}

// #TODO: proper casting of indices
#ifdef __linux__
#pragma GCC diagnostic warning "-Wsign-conversion"
#endif

} // namespace IWD::data