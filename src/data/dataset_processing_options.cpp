#include "data/dataset_processing_options.h"

namespace IWD::data {

void job_processing_options::copy_from_base(std::shared_ptr<dataset_processing_options_global> options) noexcept
{
	dataset_processing_options_global::operator=(*options);
}

sms_2dm_processing_options::sms_2dm_processing_options(std::shared_ptr<job_processing_options> options) noexcept
{
	copy_from_base(options);
}
void sms_2dm_processing_options::copy_from_base(std::shared_ptr<job_processing_options> options) noexcept
{
	job_processing_options::operator=(*options);
	dataset_processing_options_global::operator=(*options);
}

mike21_processing_options::mike21_processing_options(std::shared_ptr<job_processing_options> options) noexcept
{
	copy_from_base(options);
}

void mike21_processing_options::copy_from_base(std::shared_ptr<job_processing_options> options) noexcept
{
	job_processing_options::operator=(*options);
	dataset_processing_options_global::operator=(*options);
}

geotiff_processing_options::geotiff_processing_options(std::shared_ptr<job_processing_options> options) noexcept
{
	copy_from_base(options);
}

void geotiff_processing_options::copy_from_base(std::shared_ptr<job_processing_options> options) noexcept
{
	job_processing_options::operator=(*options);
	dataset_processing_options_global::operator=(*options);
}

_3di_processing_options::_3di_processing_options(std::shared_ptr<job_processing_options> options) noexcept
{
	copy_from_base(options);
}

void _3di_processing_options::copy_from_base(std::shared_ptr<job_processing_options> options) noexcept
{
	job_processing_options::operator=(*options);
	dataset_processing_options_global::operator=(*options);
}

} // namespace IWD::data