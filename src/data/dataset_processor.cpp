#include "data/dataset_processor.h"
#include "util/logger.h"

#include <filesystem>

const std::string LOG_TAG = "{dataset_processor}";

namespace IWD::data {

dataset_processor::dataset_processor()
{
#if PROJ_ENABLED

	context = proj_context_create();

	util::log::info(LOG_TAG, fmt::format("Searching proj.db in {}", std::filesystem::current_path().string()));

	// https://proj.org/resource_files.html
	if (!proj_context_set_database_path(context, "./share/proj/data/proj.db", nullptr, nullptr))
		util::log::error(LOG_TAG, "Couldn't read proj database.");
#endif
}

dataset_processor::~dataset_processor()
{
	for (auto p : projections) {
		proj_destroy(p.second);
	}

	if (context) proj_context_destroy(context);
}

} // namespace IWD::data