#include <cxxopts/cxxopts.hpp>

#include "definitions.h"
#include "data/grid_dataset_processor.h"
#include "data/triangle_dataset_processor.h"

#include "io/config_reader.h"

#include "util/logger.h"
#include "util/algorithm.h"
#include "util/auto_enum.h"

using namespace IWD;
using namespace IWD::util;

const std::string LOG_TAG = "{convert-cli}";

void cmd_options_init(cxxopts::Options &options)
{
	options.add_options()("h,help", "Print usage") /* fix formatter */
	    ("c,config", "Input config file. Is mandatory.", cxxopts::value<std::string>());
}

decltype(auto) cmd_options_parse_and_validate(cxxopts::Options &options, int argc, const char *const argv[])
{
	cxxopts::ParseResult result;
	IWD::data::job_processing_options proc;

	try {
		result = options.parse(argc, argv);
	}
	catch (cxxopts::OptionParseException e) {
		log::error(LOG_TAG, fmt::format("Unrecognized parameters given: {0}", e.what()));
		std::exit(IWD::exit_code::invalid_arguments);
	}

	if (result.count("help")) {
		std::cout << options.help() << std::endl;
		std::exit(IWD::exit_code::normal);
	}

	if (!result.count("config")) {
		log::error(LOG_TAG, "No configuration file provided.");
		std::exit(IWD::exit_code::invalid_arguments);
	}

	return result;
}

int main(int argc, char *argv[])
{
	log::setup();
	log::plain(fmt::format("IWD Convert {0} - compiled: {1} {2}", IWD::version::to_string(), __DATE__, __TIME__));

	if (argc > 1) {
		std::string args_concat = "";
		for (int i = 1; i < argc; ++i) {
			args_concat += std::string(argv[i]) + " ";
		}
		log::plain(fmt::format("Arguments: {0}", args_concat));
		log::endl();
	}

	cxxopts::Options options("IWD Convert", "Institute of Hydraulic Engineering and Technical Hydromechanics - Toolkit "
	                                        "Library - Conversion Command Line Interface");
	cmd_options_init(options);

	auto processing_options = cmd_options_parse_and_validate(options, argc, argv);
	auto config = io::config_reader::read({processing_options["config"].as<std::string>()});

	if (config.status != IWD::exit_code::normal) std::exit(config.status);

	auto exit_code = IWD::exit_code::normal;

	for (const auto &job : config.jobs) {
		if (std::any_of(data::triangle_types.begin(), data::triangle_types.end(),
		                [&](const data::dataset_input_type &t) { return job->input_type == t; })) {

			auto tri_processor = std::make_unique<IWD::data::triangle_dataset_processor>();

			if (auto ret = tri_processor->process(job); ret != IWD::exit_code::normal) {
				log::warn(LOG_TAG, "Error while processing job. Continuing with the next job.");
				exit_code = ret;
			}
		}
		else if (std::any_of(data::grid_types.begin(), data::grid_types.end(),
		                     [&](const data::dataset_input_type &t) { return job->input_type == t; })) {

			auto grid_processor = std::make_unique<IWD::data::grid_dataset_processor>();

			if (auto ret = grid_processor->process(job); ret != IWD::exit_code::normal) {
				log::warn(LOG_TAG, "Error while processing job. Continuing with the next job.");
				exit_code = ret;
			}
		}
	}

	log::info(LOG_TAG, "Processing of all datasets done.");
	return IWD::exit_code::normal;
}
